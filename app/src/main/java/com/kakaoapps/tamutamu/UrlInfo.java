package com.kakaoapps.tamutamu;

/**
 * Created by John on 2016-01-29.
 */
public class UrlInfo {


    private static final boolean SSL = false;

    public static final String URL_HEADER = getHttp();

    private static final String getHttp() {
        if(SSL) return "https://";
        else return "http://";
    }

    public static final String DOMAIN = "tamu.kakaoapps.co.kr/";
//    public static final String DOMAIN = "192.168.1.112/";

    public static final String API_HEADER = DOMAIN + "api/";

    public static final String MEMBER = "member/";
    public static final String LOGIN = MEMBER + "member_login.php";//로그인
    public static final String UNREAD_MSG_COUNT = MEMBER + "unread_msg_count.php"; //읽지 않은 쪽지갯수 얻기
    public static final String EMAIL_CHECK = MEMBER + "email_chk.php";//이메일 체크
    public static final String NICK_CHECK = MEMBER + "nick_chk.php";//닉네임 체크
    public static final String AREA_LIST = MEMBER + "area_list.php";//시, 구 목록
    public static final String SIGN_UP = MEMBER + "member_in.php";//회원가입
    public static final String REPORT = MEMBER + "memberSingo.php";//신고하기
    public static final String PAYMENT_REQUEST = MEMBER + "myRequest.php";//결제요청
    public static final String PAYMENT_ORDER = MEMBER + "myOrder.php";//결제요청
    public static final String GET_PROFILE = MEMBER + "Profilepage.php";//리뷰 목록
    public static final String PROFILE_MODIFY = MEMBER + "memberEdit.php";//프로필수정
    public static final String PROFILE_MODIFY_PORTFOLIO = MEMBER + "member_portfolio.php";//I Can 글쓰기
    public static final String DELETE_PROFILE = MEMBER + "self_leave.php";//프로필삭제
    public static final String FIND_ID = MEMBER + "find_id.php";//아이디 찾기
    public static final String FIND_PW = MEMBER + "find_pw.php";//비밀번호 찾기
    public static final String GET_PROVISION = MEMBER + "getAgreement.php";//이용약관
    public static final String GET_CODE = MEMBER + "sms.php";//sms 인증

    public static final String PRODUCT = "product/";

    public static final String CAN_WRITE = PRODUCT + "ican_in.php";//I Can 글쓰기
    public static final String CAN_LIST = PRODUCT + "icanList.php";//I Can 목록
    public static final String ICAN_DETAIL = PRODUCT + "icanDetail.php";
    public static final String NEED_WRITE = PRODUCT + "ineed_in.php";//I Need 글쓰기
    public static final String NEED_LIST = PRODUCT + "ineedList.php";//I Need 목록
    public static final String INEED_DETAIL = PRODUCT + "ineedDetail.php";
    public static final String REPLY_WRITE = PRODUCT + "registComment.php";//댓글 쓰기
    public static final String REVIEW_WRITE = PRODUCT + "registReview.php";//리뷰 쓰기
    public static final String REQUEST = PRODUCT + "Request.php";//제안하기
    public static final String REPLY_LIST = PRODUCT + "commentList.php";//댓글 목록
    public static final String NOTICE_LIST = PRODUCT + "getNoticeList.php";//공지사항
    public static final String FAQ_LIST = PRODUCT + "getFaqList.php"; //자주하는 질문
    public static final String REGIST_FAQ = PRODUCT + "registQna.php"; //건의하기
    public static final String REGIST_LIST = PRODUCT + "getQnaList.php"; //건의목록

    public static final String PRODUCT_DELETE = PRODUCT + "GoodsDel.php";//Can, Need 글지우긴
    public static final String GET_EVENT = PRODUCT + "getPopup.php"; //이벤트
    public static final String REPLY_DELETE = PRODUCT + "CommentDel.php";//댓글 삭제
    public static final String GET_BANK = PRODUCT + "bankinfo.php"; // 은행정보얻기
    public static final String ACT_BANK = PRODUCT + "bank.act.php"; // 거래진행

    public static final String ACT_BANK_REFUND = PRODUCT + "bank_refund.act.php"; // 환급거래진행 // added by RGJ 2017.01.18
    public static final String CAN_USER_WORK_INFO = PRODUCT + "user_work_info.php"; // 리용자 개인 작업정보 // added by RGJ 2017.01.18
    public static final String SET_BANK = PRODUCT + "bankinfo.php"; // 은행정보설정 // added by RGJ 2017.01.18

    public static final String CAN_DETAIL = PRODUCT + "can_detail.php";
    public static final String CAN_REGIST_LIST = PRODUCT + "can_regi_list.php"; // I Can 진행상황 목록얻기
    public static final String CAN_ACTION = PRODUCT + "can_action.php"; // (봉사자)can과제에 대한 진행상태등록 // added by RGJ 2017.02.03
    public static final String CAN_REGIST = PRODUCT + "can_regist.php"; // (주문자)can과제에 대한 진행상태등록 // added by RGJ 2017.02.03
    public static final String CAN_MY_REGIST_LIST = PRODUCT + "can_my_list.php"; // 나의 can작업상황 모두 얻기 // added by RGJ 2017.02.03


    public static final String NEED_DETAIL = PRODUCT + "need_detail.php";
    public static final String NEED_REGIST_LIST = PRODUCT + "need_regi_list.php"; // I Need 한 과제에 대한 진행상황 목록얻기
    public static final String NEED_ACTION = PRODUCT + "need_action.php";
    public static final String NEED_REGIST = PRODUCT + "need_regist.php";
    public static final String NEED_MY_REGIST_LIST = PRODUCT + "need_my_list.php"; // I Need 모든 과제에 대한 진행상황 목록얻기

    public static final String REGIST_SINGO = PRODUCT + "registSingo.php"; // 신고시 처리페지
    public static final String REGIST_WISH = PRODUCT + "registWish.php";


    public static final String REGIST_MEMO = PRODUCT + "registMemo.php"; //쪽지 등록(보내기) 페지
    public static final String GET_MEMO = PRODUCT + "getMemo.php"; // 쪽지 내용 현시

    public static final String GET_POINT_MALL = PRODUCT + "pointmall.php"; // 포인트몰
    public static final String GET_POINT_HISTORY = PRODUCT + "getorderList.php"; // 포인트결제이력얻기
    public static final String GET_POINT_CHARGE_HISTORY = PRODUCT + "point_charge_history.php"; // 포인트충전이력얻기 added by RGJ 2017.01.16
    public static final String GET_POINT_USE_HISTORY = PRODUCT + "point_use_history.php"; // 포인트사용이력얻기 added by RGJ 2017.01.16
    public static final String GET_POINT_REFUND_HISTORY = PRODUCT + "point_refund_history.php"; // 포인트환급이력얻기 added by RGJ 2017.01.16

    public static final String GET_CHATROOM_LIST = PRODUCT + "ChatRoom.php"; //쪽지함

    public static final String PG_HEADER = DOMAIN + "pg/";
    public static final String PAYMENT = "pg.php";
    public static final String GALAXIA_PAYMENT = MEMBER + "pay.php";
    public static final String TERMINATE_TRANSACTION = MEMBER + "terminate_transaction.php";

    public static final String SELLER_RANK = PRODUCT + "ranking_seller.php";
    public static final String BUYER_RANK = PRODUCT + "ranking_buyer.php";

    public static final String GET_LIKE_LIST = PRODUCT + "getWish.php";

    public static final String GET_POINT = MEMBER + "getPoint.php"; // 내포인트얻기

    public static final String SET_ALARM = MEMBER + "setAlarmSetting.php";
    public static final String GET_ALARM = MEMBER + "getAlarm.php";

}
