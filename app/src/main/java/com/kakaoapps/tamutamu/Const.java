package com.kakaoapps.tamutamu;


import android.os.Environment;

import java.text.DecimalFormat;

/**
 * Created by John on 2015-12-09.
 */
public class Const {
    public static final boolean IS_TEST = true;

    //지역 최대 설정
    public static final int LOCAITON_LIMIT = 3;

    public static final DecimalFormat MONEY_DF = new DecimalFormat("#,##0");

    public static final String SAVE_FILE_DIR = Environment.getExternalStorageDirectory().getAbsolutePath() + "/YOM/";



 }
 