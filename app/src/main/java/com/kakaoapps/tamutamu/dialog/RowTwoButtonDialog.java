package com.kakaoapps.tamutamu.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;

/**
 * Created by john on 2016-02-03.
 */
public class RowTwoButtonDialog extends Dialog implements View.OnClickListener{



    public interface ChoiceListner{
        void firstClick();
        void secondClick();
    }

    private ChoiceListner mListner;
    private String title, bt1, bt2;

    public RowTwoButtonDialog(Context context, String title, String bt1, String bt2, ChoiceListner listner){
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        mListner = listner;
        this.title = title;
        this.bt1 = bt1;
        this.bt2 = bt2;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);
        setContentView(R.layout.dialog_row_two_button);

        ImageView closeIv = (ImageView)findViewById(R.id.close_iv);
        closeIv.setOnClickListener(this);
        TextView titleTv = (TextView)findViewById(R.id.tv);
        titleTv.setText(title);
        Button firstBt = (Button)findViewById(R.id.first_bt);
        Button secondBt = (Button)findViewById(R.id.second_bt);
        firstBt.setText(bt1);
        secondBt.setText(bt2);
        firstBt.setOnClickListener(this);
        secondBt.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.first_bt:
                mListner.firstClick();

                break;
            case R.id.second_bt:
                mListner.secondClick();
                break;

            case R.id.close_iv:

                break;
        }

        dismiss();

    }
}
