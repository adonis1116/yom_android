package com.kakaoapps.tamutamu.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.kakaoapps.tamutamu.R;

/**
 * Created by john on 2016-02-03.
 */
public class SortDialog extends Dialog implements View.OnClickListener{

    public interface SortListner{
        void closeLocation();
        void newRegister();
        void rowPrice();
        void selectCategory();
        void openMapView(); //added by Adonis 2017.07.04
    }

    private SortListner mListner;

    public SortDialog(Context context, SortListner listner){
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        mListner = listner;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);
        setContentView(R.layout.dialog_sort);

        ImageView closeIv = (ImageView)findViewById(R.id.close_iv);
        closeIv.setOnClickListener(this);

        Button locBt = (Button)findViewById(R.id.loc_bt);
        Button newBt = (Button)findViewById(R.id.new_bt);
        Button rowBt = (Button)findViewById(R.id.row_bt);
        Button cateBt = (Button)findViewById(R.id.cate_bt);
        Button map_bt = (Button)findViewById(R.id.map_bt);
        locBt.setOnClickListener(this);
        newBt.setOnClickListener(this);
        rowBt.setOnClickListener(this);
        cateBt.setOnClickListener(this);
        map_bt.setOnClickListener(this);//added by Adonis 2017.07.04
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.loc_bt:
                mListner.closeLocation();

                break;
            case R.id.new_bt:
                mListner.newRegister();
                break;

            case R.id.row_bt:
                mListner.rowPrice();
                break;

            case R.id.cate_bt:
                mListner.selectCategory();
                break;

            case R.id.close_iv:

                break;
            //added by Adonis 2017.07.04
            case R.id.map_bt:
                mListner.openMapView();
                break;
        }

        dismiss();

    }
}
