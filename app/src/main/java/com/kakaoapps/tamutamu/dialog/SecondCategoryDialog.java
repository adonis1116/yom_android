package com.kakaoapps.tamutamu.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.adapter.SecondCategoryAdapter;

import java.util.List;

/**
 * Created by john on 2016-02-01.
 */
public class SecondCategoryDialog extends Dialog{

    public interface SecondCategoryListner{
        void onSelectCategory(int position, String cateStr);
    }

    private SecondCategoryListner mListner;
    private int mCategory;



    public SecondCategoryDialog(Context context, int category, SecondCategoryListner listner) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        mCategory = category;
        mListner = listner;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);
        setContentView(R.layout.dialog_second_category);

        ListView lv = (ListView)findViewById(R.id.lv);


        int resId = getContext().getResources().getIdentifier("cate" + mCategory, "array", getContext().getPackageName());
        final String cates[] = getContext().getResources().getStringArray(resId);
        SecondCategoryAdapter adapter = new SecondCategoryAdapter(getContext(), cates);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mListner.onSelectCategory(position, cates[position]);
                dismiss();
            }
        });

    }

}
