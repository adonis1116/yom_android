package com.kakaoapps.tamutamu.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.activity.PointManagerActivity;
import com.kakaoapps.tamutamu.activity.WebViewActivity;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.manager.PreferencesManager;
import com.kakaoapps.tamutamu.model.Bank;
import com.loopj.android.http.RequestParams;

/**
 * Created by john on 2016-02-01.
 */
public class BackbookDialog extends Dialog implements View.OnClickListener {
    private int pointId;
    BackbookDialogListener mListener;
    public BackbookDialog(Context context, int pointId, BackbookDialogListener listener) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.pointId = pointId;
        mListener = listener;
    }

    public interface BackbookDialogListener{
        void gotoATM(int pointId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);
        setContentView(R.layout.dialog_bankbook);

        Button confirmBt = (Button)findViewById(R.id.confirm_bt);
        confirmBt.setOnClickListener(this);

        Button cancle = (Button)findViewById(R.id.cancel_bt);
        cancle.setOnClickListener(this);
        getBankNum();
    }

    private void getBankNum(){
        DialogView.progressDialogShow(getContext());
        RequestParams params = new RequestParams(); // added by RGJ 2017.01.31
        // params.put("user_no", new PreferencesManager(this.getContext()).getUser().getUserNo()); // added by RGJ 2017.01.31
        params.put("user_no", "0"); // added by RGJ 2017.01.31
       // params.put("user_id", new PreferencesManager(this.getContext()).getUser().getUserId()); // added by RGJ 2017.01.31
        new HttpsManager(getContext()).getBankInfo(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                Bank item = (Bank)result;
                TextView bankTv = (TextView)findViewById(R.id.bank_tv);
                bankTv.setText(String.format("%1s %2s", item.getName(), item.getNumber()));
                TextView ownerTv = (TextView)findViewById(R.id.owner_tv);
                ownerTv.setText(String.format("예금주:%s", item.getOwner()));
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                Toast.makeText(getContext(), errorStr, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void actBank(){
        RequestParams params = new RequestParams();
        params.put("gidx", pointId);
        params.put("user_no", new PreferencesManager(getContext()).getUser().getUserNo());
        new HttpsManager(getContext()).actBank(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                Toast.makeText(getContext(), (String)result, Toast.LENGTH_SHORT).show();
                dismiss();
            }

            @Override
            public void onFailure(String errorStr) {
                Toast.makeText(getContext(), errorStr, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirm_bt:
                mListener.gotoATM(pointId);
//                actBank();
                break;
            case R.id.cancel_bt:
                dismiss();
                break;
        }



    }
}
