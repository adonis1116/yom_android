package com.kakaoapps.tamutamu.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;

/**
 * Created by john on 2016-02-01.
 */
public class OneButtonDialog extends Dialog implements View.OnClickListener {

    public interface ChoiceListner{
        void confirm();
    }

    private ChoiceListner mListner;
    private String title, content, bt1;


    public OneButtonDialog(Context context, String title, String content, String bt1, ChoiceListner listner) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);

        this.title = title;
        this.content = content;
        this.bt1 = bt1;
        mListner = listner;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);
        setContentView(R.layout.dialog_one_button);

        TextView titleTv = (TextView)findViewById(R.id.title_tv);
        TextView contentTv = (TextView)findViewById(R.id.content_tv);

        titleTv.setText(title);
        contentTv.setText(Html.fromHtml(content));

        Button confirmBt = (Button)findViewById(R.id.confirm_bt);
        confirmBt.setText(bt1);
        confirmBt.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirm_bt:
                mListner.confirm();
                break;
        }

        dismiss();

    }
}
