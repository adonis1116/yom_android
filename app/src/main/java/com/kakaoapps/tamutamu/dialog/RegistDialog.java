package com.kakaoapps.tamutamu.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kakaoapps.tamutamu.R;

/**
 * Created by john on 2016-02-01.
 */
public class RegistDialog extends Dialog implements View.OnClickListener{
    public interface ChoiceListner{
        void confirm(String content, String price);
        void cancle();
    }

    private ChoiceListner mListner;
    private EditText priceEt, et;

    /**
     *
     * @param context context
     * @param listner 콜백
     */

    public RegistDialog(Context context, ChoiceListner listner) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);

        mListner = listner;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);
        setContentView(R.layout.dialog_regist);


        Button confirmBt = (Button)findViewById(R.id.confirm_bt);
        Button cancleBt = (Button)findViewById(R.id.cancel_bt);

        confirmBt.setOnClickListener(this);
        cancleBt.setOnClickListener(this);
        priceEt = (EditText)findViewById(R.id.price_et);
        et = (EditText)findViewById(R.id.et);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirm_bt:
                if(et.getText().toString().isEmpty()){
                    Toast.makeText(getContext(), "내용을 입력해주세요", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(priceEt.getText().toString().isEmpty()){
                    Toast.makeText(getContext(), "금액을 입력해주세요", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(priceEt.getText().toString().trim().length() > 15){
                    Toast.makeText(getContext(), "금액은 15자리수를 넘을수 없습니다.", Toast.LENGTH_SHORT).show();
                    return;
                }
                mListner.confirm(et.getText().toString(), priceEt.getText().toString());
                dismiss();
                break;
            case R.id.cancel_bt:
                mListner.cancle();
                dismiss();
                break;
        }



    }

}
