package com.kakaoapps.tamutamu.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.kakao.kakaolink.AppActionBuilder;
import com.kakao.kakaolink.AppActionInfoBuilder;
import com.kakao.kakaolink.KakaoLink;
import com.kakao.kakaolink.KakaoTalkLinkMessageBuilder;
import com.kakao.util.KakaoParameterException;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.manager.PreferencesManager;
import com.kakaoapps.tamutamu.model.Can;
import com.kakaoapps.tamutamu.model.Need;
import com.kakaoapps.tamutamu.utils.DebugUtils;

/**
 * Created by developer on 2016-06-28.
 */
public class SNSShareDialog extends Dialog implements View.OnClickListener{
    private final String TAG = getClass().getSimpleName();
    private final String KAKAOTALK_PACKAGENAME = "com.kakao.talk";

    private CallbackManager callbackManager;
    private String title, content, imgPath, shareLink;

    private Activity mActivity;
    private int idx, type;

    public SNSShareDialog(Context context, Activity activity, Can item) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.imgPath = item.getImgList().get(0);
        this.title = item.getSubject();
        this.content = item.getContent();
        shareLink = item.getShareLink();
        mActivity = activity;
        type = 1;
        idx = item.getId();
    }


    public SNSShareDialog(Context context, Activity activity, Need item) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.title = item.getSubject();
        this.content = item.getContent();
        shareLink = item.getShareLink();
        mActivity = activity;
        type = 2;
        idx = item.getId();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);
        setContentView(R.layout.dialog_share);


        findViewById(R.id.kakao_iv).setOnClickListener(this);
        findViewById(R.id.facebook_iv).setOnClickListener(this);
        findViewById(R.id.close_iv).setOnClickListener(this);

        callbackManager = CallbackManager.Factory.create();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.kakao_iv: shareKakao(); break;
            case R.id.facebook_iv: shareFacebook(); break;
            case R.id.close_iv: dismiss(); break;
        }
    }

    private void shareKakao(){
        if(!checkAppInstall(KAKAOTALK_PACKAGENAME)){
            Toast.makeText(getContext(), "카카오톡이 설치되어있지 않습니다.", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            KakaoLink kakaoLink = KakaoLink.getKakaoLink(getContext());
            KakaoTalkLinkMessageBuilder builder = kakaoLink.createKakaoTalkLinkMessageBuilder();
            PreferencesManager  pm = new PreferencesManager(getContext());
            builder.addText("[YOM]" + title + "\n" + content);
            builder.addAppButton("앱으로 이동",
                    new AppActionBuilder()
                            .addActionInfo(AppActionInfoBuilder.createAndroidActionInfoBuilder()
                                    //.setExecuteParam("yom://go_detail?type=2&idx=23")
                                    .setExecuteParam("type=" + type + "&idx=" + idx)
                                    .setMarketParam("referrer=yom")
                                    .build())
                            //.setUrl("https://play.google.com/store/apps/details?id=com.kakaoapps.tamutamu")
                            .setUrl(shareLink)
                            .build());
            kakaoLink.sendMessage(builder, getContext());

        } catch (KakaoParameterException e) {
            e.printStackTrace();
        }
    }

    private void shareFacebook(){


        ShareDialog dialog = new ShareDialog(mActivity);
        dialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                dismiss();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                DebugUtils.setLog(TAG, "error : " + error.toString());
            }

        });

        if(dialog.canShow(ShareLinkContent.class)){

            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle("[YOM]" + title)
                    .setImageUrl(Uri.parse(imgPath))
                    .setContentDescription(content)
                    .setContentUrl(Uri.parse(shareLink))
                    .build();


            dialog.show(linkContent);
        }

    }


    private boolean checkAppInstall(String packageName){

        try {
            PackageManager packageManager = getContext().getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(packageName, PackageManager.GET_META_DATA);
            ApplicationInfo applicationInfo = packageInfo.applicationInfo;
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

}
