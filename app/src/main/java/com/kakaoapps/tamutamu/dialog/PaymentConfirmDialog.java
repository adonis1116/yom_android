package com.kakaoapps.tamutamu.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;

/**
 * Created by john on 2016-02-01.
 */
public class PaymentConfirmDialog extends Dialog implements View.OnClickListener{
    public interface ChoiceListner{
        void confirm();
        void cancle();
    }

    private ChoiceListner mListner;

    public PaymentConfirmDialog(Context context, ChoiceListner listner) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);

        mListner = listner;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);
        setContentView(R.layout.dialog_payment_confirm);


        Button confirmBt = (Button)findViewById(R.id.confirm_bt);
        Button cancleBt = (Button)findViewById(R.id.cancel_bt);

        confirmBt.setOnClickListener(this);
        cancleBt.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirm_bt:
                mListner.confirm();

                break;
            case R.id.cancel_bt:
                mListner.cancle();
                break;
        }

        dismiss();

    }

}
