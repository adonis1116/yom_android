package com.kakaoapps.tamutamu.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;

/**
 * Created by john on 2016-02-01.
 */
public class TwoButtonDialog extends Dialog implements View.OnClickListener{
    public interface ChoiceListner{
        void confirm();
        void cancle();
    }

    private ChoiceListner mListner;
    private String title, content, bt1, bt2;

    /**
     *
     * @param context context
     * @param title 제목
     * @param content 내용
     * @param bt1 1버튼 텍스트
     * @param bt2 2버튼 텍스트
     * @param listner 콜백
     */

    public TwoButtonDialog(Context context, String title, String content, String bt1, String bt2, ChoiceListner listner) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);

        this.title = title;
        this.content = content;
        this.bt1 = bt1;
        this.bt2 = bt2;
        mListner = listner;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);
        setContentView(R.layout.dialog_two_button);

        TextView titleTv = (TextView)findViewById(R.id.title_tv);
        TextView contentTv = (TextView)findViewById(R.id.content_tv);

        titleTv.setText(title);
        contentTv.setText(Html.fromHtml(content));

        Button confirmBt = (Button)findViewById(R.id.confirm_bt);
        Button cancleBt = (Button)findViewById(R.id.cancel_bt);

        confirmBt.setOnClickListener(this);
        cancleBt.setOnClickListener(this);

        confirmBt.setText(bt1);
        cancleBt.setText(bt2);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirm_bt:
                mListner.confirm();

                break;
            case R.id.cancel_bt:
                mListner.cancle();
                break;
        }

        dismiss();

    }

}
