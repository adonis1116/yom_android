package com.kakaoapps.tamutamu.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.activity.PointManagerActivity;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.manager.PreferencesManager;
import com.kakaoapps.tamutamu.model.Process;
import com.kakaoapps.tamutamu.utils.DebugUtils;
import com.loopj.android.http.RequestParams;

/**
 * Created by developer on 2016-06-29.
 */
public class PaymentDialog extends Dialog implements View.OnClickListener{
    private final String TAG = getClass().getSimpleName();

    private String usePoint;
    private boolean isPayment = false;
    private Process mProcess;
    private int type;

    private PaymetListner mListner;

    public interface PaymetListner{
        public void onSuccess();
    }


    public PaymentDialog(Context context, String usePoint, int type, Process process, PaymetListner listner) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.usePoint = usePoint;
        this.type = type;
        mProcess = process;
        mListner = listner;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);
        setContentView(R.layout.dialog_payment);

        Long savePointLong = Long.parseLong(new PreferencesManager(getContext()).getUser().getPoint());
        Long userPointLong = Long.parseLong(usePoint);

        isPayment = savePointLong >= userPointLong;


        TextView usePointTv = (TextView)findViewById(R.id.use_point_tv);
        usePointTv.setText("해당 컨텐츠는 " + Long.toString(userPointLong) + "P 입니다.");

        TextView contentTv = (TextView)findViewById(R.id.content_tv);
        contentTv.setText(isPayment ? "결제하시겠습니까?" : "포인트가 부족합니다\n충전하시겠습니까?");

        TextView savePointTv = (TextView)findViewById(R.id.save_point_tv);

        savePointTv.setText("보유중인 포인트 : " + Long.toString(savePointLong) + "P 입니다.");

        Button confirmBt = (Button) findViewById(R.id.confirm_bt);
        Button cancleBt = (Button)findViewById(R.id.cancel_bt);
        confirmBt.setOnClickListener(this);
        cancleBt.setOnClickListener(this);


        getMyPoint();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirm_bt:
                if(isPayment){
                    pointPayment();
                }else{
                    Intent intent = new Intent(getContext(), PointManagerActivity.class);
                    getContext().startActivity(intent);
                    dismiss();
                }
                break;
            case R.id.cancel_bt:
                dismiss();
                break;
        }
    }

    private void pointPayment(){
        DialogView.progressDialogShow(getContext());
        RequestParams params = new RequestParams();
        params.put("idx", mProcess.getId());
        params.put("type", mProcess.getType());
        params.put("user_no", new PreferencesManager(getContext()).getUser().getUserNo());

        HttpsManager hm = new HttpsManager(getContext());
        if(type == 1){
            hm.canAction(params, httpListner);
        }else{
            hm.needAction(params, httpListner);
        }
    }

    private HttpsManager.HttpListner httpListner = new HttpsManager.HttpListner() {
        @Override
        public void onSuccess(Object result) {
            DialogView.progressDialogClose();
            PreferencesManager pm = new PreferencesManager(getContext());

            Long userPointLong = Long.parseLong(pm.getUser().getPoint());
            Long usePointLong = Long.parseLong(usePoint);
            pm.setUserPoint(Long.toString(userPointLong - usePointLong));
            dismiss();
            mListner.onSuccess();


        }

        @Override
        public void onFailure(String errorStr) {
            DialogView.progressDialogClose();
            Toast.makeText(getContext(), errorStr, Toast.LENGTH_SHORT).show();
        }
    };


    private void getMyPoint(){
        DebugUtils.setLog(TAG, "getMyPoint called!!!");
        RequestParams params = new RequestParams();
        params.put("user_no", new PreferencesManager(getContext()).getUser().getUserNo());
        new HttpsManager(getContext()).getMyPoint(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();

            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                Toast.makeText(getContext(), errorStr, Toast.LENGTH_SHORT).show();
            }
        });
    }

}
