package com.kakaoapps.tamutamu.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kakaoapps.tamutamu.Const;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.activity.CanDetailActivity;
import com.kakaoapps.tamutamu.activity.CommonActivity;
import com.kakaoapps.tamutamu.activity.MapViewActivity;
import com.kakaoapps.tamutamu.activity.MypageActivity;
import com.kakaoapps.tamutamu.activity.OtherPageActivity;
import com.kakaoapps.tamutamu.activity.ProgressActivity;
import com.kakaoapps.tamutamu.adapter.MypageCanAdapter;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.manager.PreferencesManager;
import com.kakaoapps.tamutamu.model.Can;
import com.kakaoapps.tamutamu.model.Process;
import com.loopj.android.http.RequestParams;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.w3c.dom.Text;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Panther on 1/14/2017.
 */

public class CanDetailUserInfoDialog extends Dialog implements View.OnClickListener{

    protected PreferencesManager pm;

    private Can mCanUser;
    private int mUserNum;
    private String mUserId;
    private View mainView; // main view
    private CircularImageView photo_iv; // 사용자 이미지
    // private ImageView user_iv; // 사용자 이미지
    private TextView nickname_tv; // 닉네임
    private TextView worknum_tv; // 작업완료개수
    private TextView avgprice_tv; //평균의뢰가격
    private Button profile_bt; // 프로필보기 버튼 added by Adonis 2017.07.04
    public CanDetailUserInfoDialog(Context context, Can canUser) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        mCanUser = canUser;
    }

    // added by Adonis 2017.07.04
    @Override
    public void onClick(View v) {
        /*Intent intent = null;
        switch (v.getId()) {
            case R.id.profile_bt:
                if( pm.getUser().getUserNo() == mCanUser.getUserNo() ){
                    intent = new Intent(this.getContext(), MypageActivity.class);
                }else{
                    intent = new Intent(this.getContext(), OtherPageActivity.class);
                    intent.putExtra("write_id", mCanUser.getUserNo());
                }

                this.getContext().startActivity(intent);
                break;
            default:*/
                this.dismiss();
        /*}*/
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);
        setContentView(R.layout.dialog_can_detail_userinfo);

        init();
    }

    private void init(){
        mainView = findViewById(R.id.userinfo_layout);
        mainView.setOnClickListener(this);

        photo_iv = (CircularImageView)findViewById(R.id.pic_iv); // 사용자이미지
        nickname_tv = (TextView)findViewById(R.id.nick_name_tv); // 닉네임
        worknum_tv = (TextView)findViewById(R.id.work_num_tv); // 작업완료개수
        avgprice_tv = (TextView)findViewById(R.id.avg_price_tv); // 평균의뢰가격
        profile_bt = (Button)findViewById(R.id.profile_bt);
        profile_bt.setOnClickListener(this);
        getUserInfo(); //정보얻기
    }

    private void getUserInfo()
    {
        // 그림설정
        photo_iv.setOnClickListener(this);
        photo_iv.setBorderWidth(0);
        if(mCanUser.getUserImage() != null && mCanUser.getUserImage().length() > 0) Glide.with(this.getContext())
                .load(mCanUser.getUserImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(photo_iv);

        nickname_tv.setText(mCanUser.getNickName()); // 닉네임 설정

        // 작업정보설정
        setUserWorkInfo();
    }

    // 리용자작업정보얻기
    private void setUserWorkInfo()
    {
        DialogView.progressDialogShow(getContext());
        RequestParams params = new RequestParams();
        params.put("user_no", mCanUser.getUserNo());
        params.put("user_id", mCanUser.getUserId());

        new HttpsManager(getContext()).getUserWorkInfo(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();

                double[] proList = (double[])result;
                worknum_tv.setText(NumberFormat.getNumberInstance(Locale.US).format(proList[0]) + "건");
                avgprice_tv.setText(NumberFormat.getNumberInstance(Locale.US).format(proList[1] ) + "원");
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                Toast.makeText(getContext(), errorStr, Toast.LENGTH_SHORT).show();
            }
        });

    }

    //작업완료개수 얻기
    private void getWorkNum()
    {
        //  if( new CommonActivity().networkCheck() == false) return; // network check

        DialogView.progressDialogShow(getContext());
        RequestParams params = new RequestParams();
        params.put("user_no", mCanUser.getUserNo());
        new HttpsManager(getContext()).canMyRegistList(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                ArrayList<Process> proList = (ArrayList<Process>)result;
                int work_cnt = 0;
                for(int i=0; i<proList.size();i++)
                {
                    if(proList.get(i).getType() == 5) work_cnt++;
                }
                worknum_tv.setText(NumberFormat.getNumberInstance(Locale.US).format(work_cnt) + "건");
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                Toast.makeText(getContext(), errorStr, Toast.LENGTH_SHORT).show();
            }
        });


    }
    // 평균가격 얻기
    private void getAveragePrice()
    {
      //  if( new CommonActivity().networkCheck() == false) return; // network check

        RequestParams params = new RequestParams();
        params.put("orderby", "regdt");
        params.put("desc", "desc");
        params.put("user_no", mCanUser.getUserNo());
        params.put("start", 1);
        params.put("pgNum", 10000000);
        params.put("ismy", 1);
        DialogView.progressDialogShow(getContext());
        new HttpsManager(getContext()).getCanList(params, new HttpsManager.HttpListner() {

            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                ArrayList<Can> canList = (ArrayList<Can>) result;
                long avg_price=0;
                for(int i=0; i<canList.size(); i++){
                    avg_price += Long.parseLong(canList.get(i).getPrice());
                }
//                avg_price = 10;
                avg_price /= canList.size();
                avgprice_tv.setText(Const.MONEY_DF.format(Long.valueOf(avg_price)) + "포인트");
//                avgprice_tv.setText(NumberFormat.getNumberInstance(Locale.US).format(avg_price) + "포인트"); // 평균의뢰가격설정
              }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                Toast.makeText(getContext(), errorStr, Toast.LENGTH_SHORT).show();
            }
        });
    }

}
