package com.kakaoapps.tamutamu.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.manager.PreferencesManager;
import com.loopj.android.http.RequestParams;

/**
 * Created by john on 2016-02-01.
 */
public class SetPushDialog extends Dialog implements View.OnClickListener {

    private CheckBox pushCb, memoCb, smsCb;
    private PreferencesManager pm;


    public SetPushDialog(Context context) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);
        setContentView(R.layout.dialog_setpush);

        pm = new PreferencesManager(getContext());

        pushCb = (CheckBox)findViewById(R.id.push_cb);
        pushCb.setChecked(pm.isPush());

        memoCb = (CheckBox)findViewById(R.id.recive_cb);
        memoCb.setChecked(pm.isMemoPush());

        smsCb = (CheckBox)findViewById(R.id.sms_cb);
        smsCb.setChecked(pm.isSendSMS());

        Button confirmBt = (Button)findViewById(R.id.confirm_bt);
        confirmBt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirm_bt:
                setAlram();
                break;
        }

        dismiss();
    }


    private void setAlram(){
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        params.put("is_alarm_on", pushCb.isChecked() ? "Y" : "N");
        params.put("is_alarm_on2", memoCb.isChecked() ? "Y" : "N");
        params.put("is_alarm_on3", smsCb.isChecked() ? "Y" : "N");
        new HttpsManager(getContext()).setAlarm(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                pm.setIsPush(pushCb.isChecked());
                pm.setIsMemoPush(memoCb.isChecked());
                pm.setIsSendSMS(smsCb.isChecked());
                dismiss();
            }

            @Override
            public void onFailure(String errorStr) {
                Toast.makeText(getContext(), errorStr, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
