package com.kakaoapps.tamutamu.dialog;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;

public class DialogView {

    public static ProgressDialog mDialog;

    /**
     * 프로그래스 다이얼로그 Show
     */
    public static void progressDialogShow(Context ctx) {
        try {
            if (mDialog != null) {
                progressDialogClose();
            }
            mDialog = new ProgressDialog(ctx);
            mDialog.setIndeterminate(false);
            mDialog.setMessage("잠시만 기다려주세요");
            mDialog.setCancelable(true);
            mDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void progressDialogClose() {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }

    public static void oneButtonDialogShow(Context context, String title,
                                           String message, String btn1, final Handler handler, final int msg1) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(null);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(btn1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (handler != null) {
                    handler.sendEmptyMessage(msg1);
                }
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();
    }

    public static void oneButtonDialogShow(Context context, int icon,
                                           String title, String message, String btn1, final Handler handler,
                                           final int msg1) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(icon);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(btn1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (handler != null) {
                    handler.sendEmptyMessage(msg1);
                }
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();

    }

    public static void oneButtonDialogShow(Context ctx, int icon, String title,
                                           String content, String btn1, final Handler handler, final int msg1,
                                           final Object msg2) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
        dialog.setIcon(icon);
        dialog.setTitle(title);
        dialog.setMessage(content);
        dialog.setCancelable(false);
        dialog.setPositiveButton(btn1, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (handler != null) {
                    if (msg2 == null) {
                        handler.sendEmptyMessage(msg1);
                    } else {
                        Message message = Message.obtain();
                        message.what = msg1;
                        message.obj = msg2;
                        handler.sendMessage(message);
                    }
                }
                dialog.dismiss();
            }
        });
        dialog.create();
        dialog.show();
    }

    public static void twoButtonDialogShow(Context context, int icon,
                                           String title, String message, String btn1, String btn2,
                                           final Handler handler, final int msg1, final int msg2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(icon);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(btn1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (handler != null) {
                    handler.sendEmptyMessage(msg1);
                }
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(btn2, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (handler != null) {
                    handler.sendEmptyMessage(msg2);
                }
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();

    }

    public static void twoButtonDialogShow(Context context, int icon,
                                           String title, String message, String btn1, String btn2,
                                           final Handler handler, final int msg1, final int msg2, boolean flag) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(icon);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(flag);
        builder.setPositiveButton(btn1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (handler != null) {
                    handler.sendEmptyMessage(msg1);
                }
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(btn2, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (handler != null) {
                    handler.sendEmptyMessage(msg2);
                }
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();

    }

    public static void twoButtonDialogShow(Context ctx, int icon, String title,
                                           String content, String btn1, String btn2, final Handler handler,
                                           final int msg1, final int msg2, final Object object) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
        dialog.setIcon(icon);
        dialog.setTitle(title);
        dialog.setMessage(content);
        dialog.setCancelable(false);
        dialog.setPositiveButton(btn1, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (handler != null) {

                    Message message = Message.obtain();
                    message.what = msg1;
                    message.obj = object;
                    handler.sendMessage(message);

                }
                dialog.dismiss();
            }
        });
        dialog.setNegativeButton(btn2, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (handler != null) {
                    handler.sendEmptyMessage(msg2);
                }
                dialog.dismiss();
            }
        });

        dialog.create();
        dialog.show();
    }

    public static void threeButtonDialogShow(Context context, int icon,
                                             String title, String message, String btn1, String btn2,
                                             String btn3, final Handler handler, final int msg1, final int msg2,
                                             final int msg3) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(icon);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(btn1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (handler != null) {
                    handler.sendEmptyMessage(msg1);
                }
                dialog.dismiss();
            }
        });
        builder.setNeutralButton(btn2, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (handler != null) {
                    handler.sendEmptyMessage(msg2);
                }
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(btn3, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (handler != null) {
                    handler.sendEmptyMessage(msg3);
                }
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();

    }

}
