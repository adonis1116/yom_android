package com.kakaoapps.tamutamu.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;

/**
 * Created by john on 2016-02-03.
 */
public class RowThreeButtonDialog extends Dialog implements View.OnClickListener{



    public interface ChoiceListner{
        void firstClick();
        void secondClick();
        void thirdClick();
    }

    private ChoiceListner mListner;
    private String title, bt1, bt2, bt3;

    public RowThreeButtonDialog(Context context, String title, String bt1, String bt2, String bt3, ChoiceListner listner){
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        mListner = listner;
        this.title = title;
        this.bt1 = bt1;
        this.bt2 = bt2;
        this.bt3 = bt3;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);
        setContentView(R.layout.dialog_row_three_button);

        ImageView closeIv = (ImageView)findViewById(R.id.close_iv);
        closeIv.setOnClickListener(this);
        TextView titleTv = (TextView)findViewById(R.id.tv);
        titleTv.setText(title);
        Button firstBt = (Button)findViewById(R.id.first_bt);
        Button secondBt = (Button)findViewById(R.id.second_bt);
        Button thirdBt = (Button)findViewById(R.id.third_bt);
        firstBt.setText(bt1);
        secondBt.setText(bt2);
        thirdBt.setText(bt3);
        firstBt.setOnClickListener(this);
        secondBt.setOnClickListener(this);
        thirdBt.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.first_bt:
                mListner.firstClick();

                break;
            case R.id.second_bt:
                mListner.secondClick();
                break;

            case R.id.third_bt:
                mListner.thirdClick();
                break;

            case R.id.close_iv:

                break;
        }

        dismiss();

    }
}
