package com.kakaoapps.tamutamu.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.angads25.filepicker.controller.DialogSelectionListener;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.manager.SelectPhotoManager;

import java.io.File;

/**
 * Created by john on 2016-02-01.
 */
public class BuyConfirmAskDialogue extends Dialog implements View.OnClickListener{
    public interface ChoiceListner{
        void confirm(File file);
        void cancel();
    }
    FilePickerDialog dialog;
    public ChoiceListner mListner;
    private EditText fileNameEt;
    public Context mContext;
    public File mFile;
    /**
     *
     * @param context context
     * @param listner 콜백
     */

    public BuyConfirmAskDialogue(Context context, ChoiceListner listner) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        mListner = listner;
        mContext = context;
        DialogProperties properties = new DialogProperties();
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.selection_type = DialogConfigs.FILE_SELECT;
        properties.root = new File(DialogConfigs.DEFAULT_DIR);
        properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
        properties.offset = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = null;
        dialog = new FilePickerDialog(context,properties);
        dialog.setTitle("첨부할 파일을 선택하세요.");
        dialog.setDialogSelectionListener(new DialogSelectionListener() {
            @Override
            public void onSelectedFilePaths(String[] files) {
                for(String path:files) {
                    mFile =  new File(path);
                    fileNameEt.setText( mFile.getName() );
                    break;
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);
        setContentView(R.layout.dialog_askconfirm);


        Button confirmBt = (Button)findViewById(R.id.confirm_bt);
        Button cancleBt = (Button)findViewById(R.id.cancel_bt);
        Button addFileBt = (Button)findViewById(R.id.btn_addfile);

        confirmBt.setOnClickListener(this);
        cancleBt.setOnClickListener(this);
        addFileBt.setOnClickListener(this);
        fileNameEt = (EditText)findViewById(R.id.et_filename);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirm_bt:
                mListner.confirm(mFile);
                dismiss();
                break;
            case R.id.cancel_bt:
                mListner.cancel();
                dismiss();
                break;
            case R.id.btn_addfile:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int permissionCheck = ContextCompat.checkSelfPermission(mContext,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                        dialog.show();
                    } else {
                        ActivityCompat.requestPermissions( (Activity) mContext ,
                                new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,
                                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,}, SelectPhotoManager.MY_CAMERA_REQUEST_CODE);
                    }
                    return;
                } else {
                    dialog.show();
                }
                break;
        }
    }

}
