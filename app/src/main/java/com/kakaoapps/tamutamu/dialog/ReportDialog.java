package com.kakaoapps.tamutamu.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;

/**
 * Created by john on 2016-02-01.
 */
public class ReportDialog extends Dialog implements View.OnClickListener{
    public interface ChoiceListner{
        void confirm(String content);
        void cancle();
    }

    private ChoiceListner mListner;
    private String content;
    private EditText et;

    /**
     *
     * @param context context
     * @param content 내용
     * @param listner 콜백
     */

    public ReportDialog(Context context, String content, ChoiceListner listner) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);

        this.content = content;
        mListner = listner;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);
        setContentView(R.layout.dialog_report);


        TextView contentTv = (TextView)findViewById(R.id.content_tv);
        contentTv.setText(Html.fromHtml(content));

        Button confirmBt = (Button)findViewById(R.id.confirm_bt);
        Button cancleBt = (Button)findViewById(R.id.cancel_bt);

        confirmBt.setOnClickListener(this);
        cancleBt.setOnClickListener(this);

        et = (EditText)findViewById(R.id.et);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirm_bt:
                mListner.confirm(et.getText().toString());
                break;
            case R.id.cancel_bt:
                mListner.cancle();
                break;
        }

        dismiss();

    }

}
