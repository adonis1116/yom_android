package com.kakaoapps.tamutamu.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.utils.DebugUtils;

/**
 * Created by john on 2016-02-04.
 */
public class CategoryDialog extends Dialog implements View.OnTouchListener{
    private final String TAG = getClass().getSimpleName();

    private final int CATEGORY_MAX = 10;

    private Category categorys [] = new Category[CATEGORY_MAX];
    private int position = 0;

    class Category{
        FrameLayout fl;
        ImageView iv;
    }

    public interface CategoryListner{
        void selectCategory(int category);
    }

    private CategoryListner mListner;

    public CategoryDialog(Context context, CategoryListner listner){
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        mListner = listner;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);
        setContentView(R.layout.dialog_category);


        for(int i = 1; i<=CATEGORY_MAX; i++){
            int resId = getContext().getResources().getIdentifier("category_fl" + i, "id", getContext().getPackageName());
            Category category = new Category();
            category.fl = (FrameLayout)findViewById(resId);
            category.fl.setOnTouchListener(this);
            category.fl.setBackgroundResource(R.drawable.weak_gray_round_box);
            resId = getContext().getResources().getIdentifier("category_iv" + i, "id", getContext().getPackageName());
            category.iv = (ImageView)findViewById(resId);
            resId = getContext().getResources().getIdentifier("category_" + i + "_off", "drawable", getContext().getPackageName());
            category.iv.setImageResource(resId);
            categorys[i-1] = category;
        }

        ImageView closeIv = (ImageView)findViewById(R.id.close_iv);
        closeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {

        DebugUtils.setLog(TAG, "action : " + event.getAction());
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                for(int i = 1; i<=CATEGORY_MAX; i++){
                    Category category = categorys[i-1];
                    if(v == category.fl){
                        category.fl.setBackgroundResource(R.drawable.purple_round_box);
                        int resId = getContext().getResources().getIdentifier("category_" + i + "_on", "drawable", getContext().getPackageName());
                        category.iv.setImageResource(resId);
                        position = i;
                    }
                }
                break;

            case MotionEvent.ACTION_UP:
                mListner.selectCategory(position);
                dismiss();
                break;
        }

        return true;
    }

}
