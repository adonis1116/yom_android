package com.kakaoapps.tamutamu.utils;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by John on 2015-12-09.
 */


public class RecycleUtils {
    /**
     * View MEMORY 해제
     *
     * @param root
     *            해제하고자 하는 View
     */
    public static void recursiveRecycle(View root) {
        if (root == null) {
            return;
        }
        root.setBackgroundDrawable(null);
        if (root instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) root;
            int count = group.getChildCount();
            for (int i = 0; i < count; i++) {
                recursiveRecycle(group.getChildAt(i));
            }
            if (!(root instanceof AdapterView)) {
                group.removeAllViews();
            }

        }
        if (root instanceof ImageView) {
            ((ImageView) root).setImageDrawable(null);
        }
        root = null;
        return;
    }

    /**
     * Adapter 객체에서 사용되는 View 해제
     *
     * @param recycleList
     *            View를 참조하는 WeakReference 리스트 객체
     */
    public static void recursiveRecycle(List<WeakReference<View>> recycleList) {
        for (WeakReference<View> ref : recycleList) {
            recursiveRecycle(ref.get());
        }
    }
}