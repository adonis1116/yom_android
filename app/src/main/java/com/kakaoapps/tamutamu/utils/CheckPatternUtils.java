package com.kakaoapps.tamutamu.utils;

import java.util.regex.Pattern;

/**
 * Created by John on 2015-12-30.
 */
public class CheckPatternUtils {

    public enum PatternType{
        EMAIL,
        KOREAN
    }

    public boolean checkPattern(String value, PatternType type){
        String str = "";
        switch (type){
            case EMAIL:
                str = "^[_a-zA-Z0-9-\\.]+@[\\.a-zA-Z0-9-]+\\.[a-zA-Z]+$";
                break;
            case KOREAN:
                str = "^[ㄱ-ㅣ가-힣]*$";
                break;

        }

        Pattern pattern = Pattern.compile(str);
        boolean isMatch = pattern.matcher(value).matches();
        return isMatch;
    }
}
