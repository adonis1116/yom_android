package com.kakaoapps.tamutamu.utils;


import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.kakaoapps.tamutamu.Const;

public class DebugUtils {
	public static void setLog(final String TAG, final String MSG) {
		if(Const.IS_TEST) Log.e("TamuTamu " + TAG, MSG);
	}

	public static void setToast(final Context CONTEXT, final String MSG) {
		if(Const.IS_TEST) Toast.makeText(CONTEXT, MSG, Toast.LENGTH_LONG).show();
	}
}