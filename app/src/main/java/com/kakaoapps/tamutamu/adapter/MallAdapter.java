package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.model.Mall;

import java.util.ArrayList;

/**
 * Created by developer on 2016-07-06.
 */
public class MallAdapter extends CommonBaseAdapter {
    private Context mContext;
    private ArrayList<Mall> mList;
    private LayoutInflater inflater;

    public MallAdapter(Context context, ArrayList<Mall> list) {
        mContext = context;
        mList = list;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Mall getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        Mall item = getItem(position);
        if (v == null) v = inflater.inflate(R.layout.item_mall, null);

        ImageView iv = ViewHolder.get(v, R.id.iv);
        Glide.with(mContext)
                .load(item.img)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .into(iv);

        return v;
    }
}
