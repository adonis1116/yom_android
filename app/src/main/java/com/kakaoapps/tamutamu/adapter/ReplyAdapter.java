package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.UrlInfo;
import com.kakaoapps.tamutamu.activity.CanDetailActivity;
import com.kakaoapps.tamutamu.activity.OtherPageActivity;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.manager.PreferencesManager;
import com.kakaoapps.tamutamu.model.Can;
import com.kakaoapps.tamutamu.model.Reply;
import com.kakaoapps.tamutamu.model.User;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;

/**
 * Created by john on 2016-02-04.
 */
public class ReplyAdapter extends CommonBaseAdapter {

    private final String TAG = getClass().getSimpleName();
    private Context mContext;
    private ArrayList<Reply> mList;
    private LayoutInflater inflater;
    private User loginUser;


    public ReplyAdapter(Context context, ArrayList<Reply> list){
        mContext = context;
        mList = list;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        PreferencesManager pm = new PreferencesManager(mContext);
        loginUser = pm.getUser();
    }


    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View v, ViewGroup parent) {
        if(v == null)v = inflater.inflate(R.layout.item_reply, null);
        final Reply item = (Reply)getItem(position);

        ImageView picIv = ViewHolder.get(v, R.id.pic_iv);
        ImageView closeIv = ViewHolder.get(v, R.id.close_iv);
        TextView nickTv = ViewHolder.get(v, R.id.nick_tv);
        TextView contentTv = ViewHolder.get(v, R.id.content_tv);

        if(item.getUserPic() != null){
            if (!item.getUserPic().contains("http")) {
                item.setUserPic(UrlInfo.URL_HEADER + UrlInfo.API_HEADER + "data/profile/" + item.getUserPic());
            }
            Glide.with(mContext)
                    .load(item.getUserPic())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(picIv);
        }
        nickTv.setText(item.getUserNick());
        contentTv.setText(item.getContent());

        picIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, OtherPageActivity.class);
                intent.putExtra("write_id", item.getUserNo());
                mContext.startActivity(intent);
            }
        });


        if(loginUser.getUserNo() == item.getUserNo()){
            closeIv.setVisibility(View.VISIBLE);
            closeIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteReply(item.getId(), position);
                }
            });
        }else{
            closeIv.setVisibility(View.GONE);
        }
        return v;
    }

    private void deleteReply(int id, final int position){
        DialogView.progressDialogShow(mContext);
        RequestParams params = new RequestParams();
        params.put("idx", id);

        new HttpsManager(mContext).deleteReply(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                mList.remove(position);
                notifyDataSetChanged();
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                Toast.makeText(mContext, errorStr, Toast.LENGTH_LONG).show();
            }
        });
    }


}
