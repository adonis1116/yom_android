package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.model.Tag;

import java.util.ArrayList;

/**
 * Created by john on 2016-02-12.
 */
public class CloseTagAdapter extends CommonBaseAdapter {

    private final String TAG = getClass().getSimpleName();
    private Context mContext;
    private ArrayList<Tag> mList;
    private LayoutInflater inflater;

    public CloseTagAdapter(Context context, ArrayList<Tag> list){
        mContext = context;
        mList = list;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Tag getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View v, ViewGroup parent) {
        if(v == null)v = inflater.inflate(R.layout.item_closetag, null);
        TextView tagTv = ViewHolder.get(v, R.id.tag_tv);
        tagTv.setText("#" + getItem(position).tag);
        ImageView closeIv = ViewHolder.get(v, R.id.close_iv);
        closeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mList.remove(position);
                notifyDataSetChanged();
            }
        });
        return v;
    }
}
