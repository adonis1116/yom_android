package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.model.PointHistory;
import com.kakaoapps.tamutamu.model.Rank;

import java.util.ArrayList;

/**
 * Created by developer on 2016-06-30.
 */
public class RankAdapter extends CommonBaseAdapter {

    private Context mContext;
    private ArrayList<Rank> mList;
    private LayoutInflater inflater;

    public RankAdapter(Context context, ArrayList<Rank> list){
        mContext = context;
        mList = list;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Rank getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        if(v == null) v = inflater.inflate(R.layout.item_rank, null);
        LinearLayout ll = ViewHolder.get(v, R.id.ll);
        if(position % 2 == 0) ll.setBackgroundColor(Color.parseColor("#FFFFFF"));
        else ll.setBackgroundColor(Color.parseColor("#FBFBFB"));

        Rank item = getItem(position);

        TextView rankTv = ViewHolder.get(v, R.id.rank_tv);
        TextView nickTv = ViewHolder.get(v, R.id.nick_tv);
        TextView pointTv = ViewHolder.get(v, R.id.point_tv);

        rankTv.setText(item.rank + "위");
        nickTv.setText(item.nick);
        pointTv.setText(item.point + "포인트");

        if(!item.myRank.equals("null") && item.rank == Integer.parseInt(item.myRank)){
            ll.setBackgroundColor(mContext.getResources().getColor(R.color.purple));
            rankTv.setTextColor(Color.WHITE);
            nickTv.setTextColor(Color.WHITE);
            pointTv.setTextColor(Color.WHITE);
        }

        return v;
    }
}
