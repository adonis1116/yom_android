package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.UrlInfo;
import com.kakaoapps.tamutamu.model.Review;
import com.kakaoapps.tamutamu.utils.DebugUtils;

import java.util.ArrayList;

/**
 * Created by john on 2016-02-04.
 */
public class ReviewAdapter extends CommonBaseAdapter {

    private final String TAG = getClass().getSimpleName();
    private Context mContext;
    private ArrayList<Review> mList;
    private LayoutInflater inflater;

    private final int STAR_MAX = 5;
    private LayoutParams params;

    public ReviewAdapter(Context context, ArrayList<Review> list){
        mContext = context;
        mList = list;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    }


    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        if(v == null)  v = inflater.inflate(R.layout.item_review, null);
        Review item = (Review)getItem(position);
        LinearLayout starLl = ViewHolder.get(v, R.id.star_ll);
        starLl.removeAllViews();
        for(int i=0; i<STAR_MAX; i++){

            ImageView starIv = new ImageView(mContext);
            starIv.setLayoutParams(params);

            if(item.getPoint() / 2 > i)starIv.setImageResource(R.drawable.check_star_small);
            else starIv.setImageResource(R.drawable.uncheck_star_small);

            starLl.addView(starIv);
        }

        ImageView picIv = ViewHolder.get(v, R.id.pic_iv);
        TextView nickTv = ViewHolder.get(v, R.id.nick_tv);
        TextView dateTv = ViewHolder.get(v, R.id.date_tv);
        TextView contentTv = ViewHolder.get(v, R.id.content_tv);
        if(item.getWriterPic() != null && item.getWriterPic().trim().length() > 0){
            if (!item.getWriterPic().contains("http")) {
                item.setWriterPic(UrlInfo.URL_HEADER + UrlInfo.API_HEADER + "data/profile/" + item.getWriterPic());
            }
            Glide.with(mContext).load(item.getWriterPic())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(picIv);
        }
        nickTv.setText(item.getWriterNick());
        dateTv.setText(item.getRegdt());
        contentTv.setText(item.getContent());

        return v;
    }


}
