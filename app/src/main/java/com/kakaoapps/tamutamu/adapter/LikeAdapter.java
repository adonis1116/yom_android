package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.UrlInfo;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.manager.PreferencesManager;
import com.kakaoapps.tamutamu.model.Like;
import com.kakaoapps.tamutamu.model.Mall;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;

/**
 * Created by developer on 2016-07-06.
 */
public class LikeAdapter extends CommonBaseAdapter {
    private Context mContext;
    private ArrayList<Like> mList;
    private LayoutInflater inflater;

    public LikeAdapter(Context context, ArrayList<Like> list) {
        mContext = context;
        mList = list;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Like getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View v, ViewGroup parent) {
        final Like item = getItem(position);
        if (v == null) v = inflater.inflate(R.layout.item_like, null);

        TextView nickTv = ViewHolder.get(v, R.id.nick_tv);
        nickTv.setText(item.nick);

        TextView priceTv = ViewHolder.get(v, R.id.price_tv);
        priceTv.setText("" + item.price);

        // modified by Adonis 2017.07.02
        TextView contentTv = ViewHolder.get(v, R.id.content_tv);
        contentTv.setText(item.content);

        // added by Adonis 2017.07.02
        TextView subjectTv = ViewHolder.get(v, R.id.subject_tv);
        subjectTv.setText(item.subject);

        TextView regdtTv = ViewHolder.get(v, R.id.regdt_tv);
        regdtTv.setText(item.regdt.substring(0,10));
        //

        ImageView iv = ViewHolder.get(v, R.id.iv);


        if(item.pic != null && item.pic.length() > 0){
            if ( !item.pic.contains("http")){
                item.pic = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + "data/profile/" + item.pic;
            }

            Glide.with(mContext)
                    .load(item.pic)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .thumbnail(0.1f)
                    .into(iv);
        }else{
            Glide.with(mContext)
                    .load(R.drawable.no_image)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .thumbnail(0.1f)
                    .into(iv);
        }

/*        Glide.with(mContext)
                .load(item.pic)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .into(iv);*/

        ImageView likeIv = ViewHolder.get(v, R.id.like_iv);
        likeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLike(item, position);
            }
        });

        return v;
    }


    private void setLike(Like item, final int position){
        RequestParams params = new RequestParams();
        params.put("user_no", new PreferencesManager(mContext).getUser().getUserNo());
        params.put("gidx", item.gidx);
        params.put("status", 0);

        new HttpsManager(mContext).registWish(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                mList.remove(position);
                notifyDataSetChanged();
            }

            @Override
            public void onFailure(String errorStr) {
                Toast.makeText(mContext, errorStr, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
