package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.kakaoapps.tamutamu.manager.PreferencesManager;
import com.kakaoapps.tamutamu.model.Process;

import java.util.ArrayList;

/**
 * Created by john on 2016-03-03.
 */
public class NeedProgressPagerAdapter extends CommonPagerAdapter{
    private final String TAG = getClass().getSimpleName();

    //public static final String PROGRESS_MENUS[] = {"전체", "수락대기", "수락요청", "결제요청", "결제대기", "결제완료" , "작업완료", "승인하기", "승인대기", "완료"};
    public static final String PROGRESS_MENUS[] = {"전체", "결제대기", "결제완료" , "구매확정", "완료"}; // modified by Adonis 2017.07.08
//    public static final String PROGRESS_MENUS[] = {"전체", "수락요청", "결제요청", "결제완료" , "작업완료", "승인하기"}; //commented by Adonis 2017.07.08
    private Context mContext;
    private ArrayList<Process> mList;
    public NeedProgressPagerAdapter(Context context, ArrayList<Process> list){
            mContext = context;
            mList = list;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return PROGRESS_MENUS[position];
        }

        @Override
        public int getCount() {
            return PROGRESS_MENUS.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object){
            return view == ((View)object);
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public void destroyItem(ViewGroup container, final int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ListView lv =  new ListView(mContext);
            ArrayList<Process> list =  getList(position);
            NeedProgressListAdapter adapter = new NeedProgressListAdapter(mContext, list);
            lv.setAdapter(adapter);
            ((ViewPager) container).addView(lv, 0);
            addConvertView(lv);
        return lv;
    }

    private ArrayList<Process> getList(int position){
        int userNo = new PreferencesManager(mContext).getUser().getUserNo();
        if(position == 0){ //전체
            return mList;
        }else{
            ArrayList<Process> result = new ArrayList<Process>();
            for(Process item : mList){
                switch (position){
                    case 1: // 수락요청
                        if(item.getType() == 2){
                            result.add(item);
                        }
                        break;
                    case 2:// 결제요청
                        if(item.getType() == 3){
                            result.add(item);
                        }
                        break;
                    case 3://결제완료
                        if(item.getType() == 4){
                            result.add(item);
                        }
                        break;
                    case 4://작업완료
                        if(item.getType() == 5){
                            result.add(item);
                        }
                        break;
/*                    case 5://승인하기
                        if(item.getType() == 4 && item.getUserNo() == userNo){ //changed by RGJ
                            result.add(item);
                        }
                        break;*/ //commented by Adonis 2017.07.08
                }

            }
            return result;
        }
    }

    /*private ArrayList<Process> getList(int position){
        int userNo = new PreferencesManager(mContext).getUser().getUserNo();
        if(position == 0){ //전체
            return mList;
        }else{
            ArrayList<Process> result = new ArrayList<Process>();
            for(Process item : mList){
                switch (position){
                    case 1: // 수락대기
                        if(item.getType() == 1 && item.getUserNo() == userNo){
                            result.add(item);
                        }
                        break;
                    case 2: // 수락요청
                        if(item.getType() == 1 && item.getUserNo() != userNo){
                            result.add(item);
                        }
                        break;
                    case 3:// 결제요청
                        if(item.getType() == 2 && item.getUserNo() != userNo){
                            result.add(item);
                        }
                        break;
                    case 4://결제대기
                        if(item.getType() == 2 && item.getUserNo() == userNo){
                            result.add(item);
                        }
                        break;
                    case 5://결제완료
                        if(item.getType() == 3 && item.getUserNo() != userNo){
                            result.add(item);
                        }
                        break;
                    case 6://작업완료
                        if(item.getType() == 3 && item.getUserNo() == userNo){
                            result.add(item);
                        }
                        break;
                    case 7://승인하기
                        if(item.getType() == 4 && item.getUserNo() != userNo){
                            result.add(item);
                        }
                        break;
                    case 8://승인대기
                        if(item.getType() == 4 && item.getUserNo() == userNo){
                            result.add(item);
                        }
                        break;
                    case 9://완료
                        if(item.getType() == 5){
                            result.add(item);
                        }
                        break;
                }

            }
            return result;
        }
    }*/
}
