package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.model.PointChargeHistory;
import com.kakaoapps.tamutamu.model.PointHistory;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Panther on 1/16/2017.
 */

public class PointChargeHistoryAdapter  extends CommonBaseAdapter{
    private Context mContext;
    private ArrayList<PointChargeHistory> mList; //환급처리이력목록
    private LayoutInflater inflater;

    public PointChargeHistoryAdapter(Context context, ArrayList<PointChargeHistory> list){
        mContext = context;
        mList = list;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public PointChargeHistory getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) { return mList.get(position).id; }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        // 아이템 뷰 얻기
        if(v == null) v = inflater.inflate(R.layout.item_point_history, null);
        LinearLayout ll = ViewHolder.get(v, R.id.ll);

        // 배경색설정
        if(position % 2 == 0) ll.setBackgroundColor(Color.parseColor("#FFFFFF"));
        else ll.setBackgroundColor(Color.parseColor("#FBFBFB"));

        // 선택된 아이템얻기
        PointChargeHistory item = getItem(position);

        // 등록날자 얻기
        TextView dateTv = ViewHolder.get(v, R.id.date_tv);
        dateTv.setText(item.regdt);

        String point = NumberFormat.getNumberInstance(Locale.US).format(item.point); // 포인트값
        String viewMoney = NumberFormat.getNumberInstance(Locale.US).format(item.point); // 돈값

        // 내용 얻기 (제목)
        TextView contentTv = ViewHolder.get(v, R.id.content_tv);
        contentTv.setText(point + "P " + "구매");
        //contentTv.setText(String.format("[%1s]%3s",item.category, item.title));

        if(item.status == 1) { //요청상태
            contentTv.setText(point + "P " + "구매요청");
        }else if(item.status == 0){
            contentTv.setText(point + "P " + "구매완료");
        }

        // 현재 포인트 얻기
        TextView pointTv = ViewHolder.get(v, R.id.point_tv);
        pointTv.setText(viewMoney + "원");
        return v;
    }
}
