package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.model.PointHistory;
import com.kakaoapps.tamutamu.model.PointRefundHistory;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Panther on 1/14/2017.
 */

public class PointRefundHistoryAdapter extends CommonBaseAdapter {
    private Context mContext;
    private ArrayList<PointRefundHistory> mList; //환급처리이력목록
    private LayoutInflater inflater;

    public PointRefundHistoryAdapter(Context context, ArrayList<PointRefundHistory> list){
        mContext = context;
        mList = list;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public PointRefundHistory getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) { return mList.get(position).id; }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        // 아이템 뷰 얻기
        if(v == null) v = inflater.inflate(R.layout.item_point_refund_history, null);
        LinearLayout ll = ViewHolder.get(v, R.id.ll);

        // 배경색설정
        if(position % 2 == 0) ll.setBackgroundColor(Color.parseColor("#FFFFFF"));
        else ll.setBackgroundColor(Color.parseColor("#FBFBFB"));

        // 선택된 아이템얻기
        PointRefundHistory item = getItem(position);

        // 등록날자 얻기
        TextView dateTv = ViewHolder.get(v, R.id.date_tv);
        dateTv.setText(item.regdt);

        String point = NumberFormat.getNumberInstance(Locale.US).format(item.point); // 포인트양
        String viewMoney = NumberFormat.getNumberInstance(Locale.US).format(item.point); // 실제돈양

        // 내용 얻기 (제목)
        TextView contentTv = ViewHolder.get(v, R.id.content_tv);
        contentTv.setText(point + "P " + "환급신청");

        //카테고리 얻기 (처리중, 환급완료)
        TextView categoryTv = ViewHolder.get(v, R.id.category_tv);
        categoryTv.setText(item.category);
        if(item.status == 1) { // 처리중이면
            categoryTv.setBackgroundResource(R.drawable.border_style);
            categoryTv.setTextColor(mContext.getResources().getColor(R.color.purple));
            categoryTv.setText("처리 중");
        }else if(item.status == 0) { // 환급완료
            categoryTv.setBackgroundResource(R.drawable.rank_stroke_box);
            categoryTv.setTextColor(mContext.getResources().getColor(R.color.gray));
            categoryTv.setText("환급완료");
        }
        // 현재 포인트 얻기
        TextView pointTv = ViewHolder.get(v, R.id.point_tv);
        pointTv.setText(viewMoney + "원");
        return v;
    }
}
