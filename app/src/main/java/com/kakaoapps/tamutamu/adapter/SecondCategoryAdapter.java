package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;

import java.util.ArrayList;

/**
 * Created by john on 2016-02-24.
 */
public class SecondCategoryAdapter extends CommonBaseAdapter {

    private final String TAG = getClass().getSimpleName();
    private Context mContext;
    private String categorys[];
    private LayoutInflater inflater;

    public SecondCategoryAdapter(Context context, String categorys[]){
        mContext = context;
        this.categorys = categorys;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return categorys.length;
    }

    @Override
    public String getItem(int position) {
        return categorys[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        if(v == null)v = inflater.inflate(R.layout.item_second_category, null);
        TextView tv = ViewHolder.get(v, R.id.tv);
        tv.setText(getItem(position));
        return v;
    }
}
