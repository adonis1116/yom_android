package com.kakaoapps.tamutamu.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;

import com.kakaoapps.tamutamu.utils.RecycleUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by John on 2015-12-11.
 */
public class CommonPagerAdapter extends PagerAdapter {

    protected List<WeakReference<View>> weakRefList = new ArrayList<WeakReference<View>>();;

    public void recycle(){
        if(weakRefList != null){
            RecycleUtils.recursiveRecycle(weakRefList);
        }
    }

    protected void addConvertView(View convertView){
        weakRefList.add(new WeakReference<View>(convertView));
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return false;
    }
}
