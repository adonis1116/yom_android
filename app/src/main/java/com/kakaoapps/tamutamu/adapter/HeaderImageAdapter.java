package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kakaoapps.tamutamu.R;

import java.util.ArrayList;

/**
 * Created by john on 2016-02-05.
 */
public class HeaderImageAdapter extends CommonPagerAdapter{

    private final String TAG = getClass().getSimpleName();
    private Context mContext;
    private ArrayList<String> mList;

    public HeaderImageAdapter(Context context, ArrayList<String> list){
        mContext = context;

        // added by Adonis 2017.07.03
        for(String item : list){
            if(item.isEmpty() || item.equals("")) list.remove(item);
        }
        //

        mList = list;
    }



    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object){
        return view == ((View)object);
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public void destroyItem(ViewGroup container, final int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView iv =  new ImageView(mContext);
        iv.setScaleType(ImageView.ScaleType.FIT_XY);
        Glide.with(mContext)
                .load(mList.get(position))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .into(iv);

        ((ViewPager) container).addView(iv, 0);
        addConvertView(iv);
        return iv;
    }
}
