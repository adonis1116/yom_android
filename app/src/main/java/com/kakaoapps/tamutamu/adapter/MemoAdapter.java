package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.UrlInfo;
import com.kakaoapps.tamutamu.model.Memo;
import com.kakaoapps.tamutamu.model.PointHistory;
import com.kakaoapps.tamutamu.utils.DebugUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import jp.wasabeef.glide.transformations.CropCircleTransformation;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * Created by developer on 2016-06-30.
 */
public class MemoAdapter extends CommonBaseAdapter {
    private final String TAG = getClass().getSimpleName();
    private Context mContext;
    private ArrayList<Memo> mList;
    private LayoutInflater inflater;

    public MemoAdapter(Context context, ArrayList<Memo> list){
        mContext = context;
        mList = list;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Memo getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        if(v == null) v = inflater.inflate(R.layout.item_memo, null);

        LinearLayout ll = ViewHolder.get(v, R.id.ll);
        if(position % 2 == 0) ll.setBackgroundColor(Color.parseColor("#FFFFFF"));
        else ll.setBackgroundColor(Color.parseColor("#FBFBFB"));

        Memo item = getItem(position);

        ImageView iv = ViewHolder.get(v, R.id.iv);

        if(item.partnerImg != null && item.partnerImg.trim().length() > 0){
            if (!item.partnerImg.contains("http")) {
                item.partnerImg = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + "data/profile/" + item.partnerImg;
            }
            Glide.with(mContext)
                    .load(item.partnerImg)
                    .bitmapTransform(new CropCircleTransformation(mContext))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(iv);
        }


        TextView nickTv = ViewHolder.get(v, R.id.nick_tv);
        nickTv.setText(item.partnerNick);

        TextView msgTv = ViewHolder.get(v, R.id.msg_tv);
        msgTv.setText(item.latestMSG);

        TextView dateTv = ViewHolder.get(v, R.id.date_tv);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        try {
            Date date = sdf.parse(item.regdt);
            long nowTime = System.currentTimeMillis();
            long beforeTime = (nowTime -  date.getTime()) / 1000;
            DebugUtils.setLog(TAG, "systedate : " + sdf.format(nowTime));
            DebugUtils.setLog(TAG, "system : " + nowTime);
            DebugUtils.setLog(TAG, "date : " + date.getTime());

            String str = "";
            if(beforeTime < 60){
                str = beforeTime + "초전";
            }else if(beforeTime < 60 * 60){
                str = (beforeTime/60) + "분전";
            }else if(beforeTime < 60 * 60 * 24 ) {
                str = (beforeTime / (60 * 60)) + "시간전";
            }else if(beforeTime < 60 * 60 * 24 * 30){
                str = (beforeTime / (60 * 60 * 24)) + "일전";
            }else if(beforeTime < 60 * 60 * 24 * 30 * 365){
                str = (beforeTime / (60 * 60 * 24 * 30)) + "달전";
            }else{
                str = (beforeTime / (60 * 60 * 24 * 30 * 365)) + "년전";
            }

            dateTv.setText(str);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return v;
    }
}
