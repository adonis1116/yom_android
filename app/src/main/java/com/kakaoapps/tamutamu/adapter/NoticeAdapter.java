package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.model.Notice;

import java.util.ArrayList;

/**
 * Created by john on 2016-02-11.
 */
public class NoticeAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private ArrayList<Notice> mList;
    private LayoutInflater inflater;

    public NoticeAdapter(Context context, ArrayList<Notice> list){
        mContext = context;
        mList = list;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getGroupCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mList.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View v = convertView;
        if(v == null) v = this.inflater.inflate(R.layout.item_notice_group, null);
        TextView numTv = ViewHolder.get(v, R.id.num_tv);
        TextView subjectTv = ViewHolder.get(v, R.id.subject_tv);
        subjectTv.setText(mList.get(groupPosition).getSubject());
        numTv.setText("" + (mList.size() - groupPosition));
        ImageView arrowIv = ViewHolder.get(v, R.id.arrow_iv);
        if (isExpanded)arrowIv.setImageResource(R.drawable.arr1);
        else arrowIv.setImageResource(R.drawable.arr2);
        return v;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mList.get(groupPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }


    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View v = convertView;
        if(v == null) v = this.inflater.inflate(R.layout.item_notice_child, null);
        TextView tv = ViewHolder.get(v, R.id.tv);
        tv.setText(mList.get(groupPosition).getContent());
        return v;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }
}
