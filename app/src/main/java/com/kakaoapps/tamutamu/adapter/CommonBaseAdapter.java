package com.kakaoapps.tamutamu.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;


import com.kakaoapps.tamutamu.utils.RecycleUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class CommonBaseAdapter extends BaseAdapter {
    
    protected List<WeakReference<View>> weakRefList = new ArrayList<WeakReference<View>>();;

    public void recycle(){
        if(weakRefList != null){
            RecycleUtils.recursiveRecycle(weakRefList);
        }
    }
    
    protected void addConvertView(View convertView){
        weakRefList.add(new WeakReference<View>(convertView));
    }
    
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }

}
