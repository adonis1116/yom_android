package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.model.Can;
import com.kakaoapps.tamutamu.model.Tag;

import java.util.ArrayList;

/**
 * Created by john on 2016-02-12.
 */
public class TagAdapter extends CommonBaseAdapter {

    private final String TAG = getClass().getSimpleName();
    private Context mContext;
    private ArrayList<Tag> mList;
    private LayoutInflater inflater;
    private int mColor;

    public TagAdapter(Context context, ArrayList<Tag> list){
        mContext = context;
        mList = list;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mColor = Color.BLACK;
    }

    public TagAdapter(Context context, ArrayList<Tag> list, int color){
        mContext = context;
        mList = list;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mColor = Color.WHITE;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Tag getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        if(v == null)v = inflater.inflate(R.layout.item_tag, null);
        TextView tagTv = ViewHolder.get(v, R.id.tag_tv);
        tagTv.setText("#" + getItem(position).tag);
        tagTv.setTextColor(mColor);
        if(mColor == Color.WHITE)tagTv.setBackgroundResource(R.drawable.white_stroke);
        return v;
    }
}
