package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.model.Area;
import com.kakaoapps.tamutamu.utils.DebugUtils;

import java.util.ArrayList;

/**
 * Created by john on 2016-02-03.
 */
public class AreaAdapter extends CommonBaseAdapter{
    private final String TAG = getClass().getSimpleName();
    private Context mContext;
    private ArrayList<Area> mList;
    private LayoutInflater inflater;

    private final int SHOW_ITEM_MAX = 3;

    public interface AreaListner{
        void select(Area area);
    }

    private AreaListner mListner;

    public AreaAdapter(Context context, ArrayList<Area> list, AreaListner listner){
        mContext = context;
        mList = list;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mListner = listner;
    }

    @Override
    public int getCount() {
        int count = mList.size() / SHOW_ITEM_MAX;
        if(mList.size() <= SHOW_ITEM_MAX) count = 1;
        else if(mList.size() % SHOW_ITEM_MAX > 0)++count;
        return count;
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        if(v == null)v = inflater.inflate(R.layout.item_area, null);
        DebugUtils.setLog(TAG, "position : " + position);

        final ArrayList<Area> list = getList(position);

        for(int i=0; i< list.size(); i++){
            int resId = mContext.getResources().getIdentifier("tv" + (i + 1), "id", mContext.getPackageName());
            TextView tv = ViewHolder.get(v, resId);

            final Area item = list.get(i);
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListner.select(item);
                }
            });
            tv.setText(item.getName());
        }

        return v;
    }

    private ArrayList<Area> getList(int position){
        ArrayList<Area> list = new ArrayList<Area>();
        int start = position * SHOW_ITEM_MAX;
        int end = start + SHOW_ITEM_MAX;
        if(getCount() - 1 == position)end = start + mList.size() % SHOW_ITEM_MAX;


        int i = start;
        while (i < end){
            list.add(mList.get(i));
            ++i;
        }

        return list;
    }
}
