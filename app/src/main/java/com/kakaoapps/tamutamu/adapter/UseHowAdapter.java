package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.kakaoapps.tamutamu.model.Process;

import java.util.ArrayList;

/**
 * Created by developer on 2016-07-01.
 */
public class UseHowAdapter extends CommonPagerAdapter {

    private final String TAG = getClass().getSimpleName();
    private final int PAGE_MAX = 4;

    private Context mContext;
    public UseHowAdapter(Context context){
        mContext = context;
    }

    @Override
    public int getCount() {
        return PAGE_MAX;
    }

    @Override
    public boolean isViewFromObject(View view, Object object){
        return view == ((View)object);
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public void destroyItem(ViewGroup container, final int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView iv =  new ImageView(mContext);
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        iv.setLayoutParams(params);
        iv.setScaleType(ImageView.ScaleType.FIT_XY);
        int resId = mContext.getResources().getIdentifier("manual0" + (position + 1), "drawable", mContext.getPackageName());
        iv.setImageResource(resId);
        ((ViewPager) container).addView(iv, 0);
        addConvertView(iv);
        return iv;
    }
}
