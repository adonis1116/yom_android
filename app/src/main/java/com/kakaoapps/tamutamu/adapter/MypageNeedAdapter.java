package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.UrlInfo;
import com.kakaoapps.tamutamu.activity.NeedDetailActivity;
import com.kakaoapps.tamutamu.model.Need;

import java.util.ArrayList;

/**
 * Created by john on 2016-02-04.
 */
public class MypageNeedAdapter extends CommonBaseAdapter{
    private final String TAG = getClass().getSimpleName();
    private Context mContext;
    private ArrayList<Need> mList;
    private LayoutInflater inflater;

    public MypageNeedAdapter(Context context, ArrayList<Need> list){
        mContext = context;
        mList = list;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        if(v == null)v = inflater.inflate(R.layout.item_mypage_need, null);
        final Need item = (Need)getItem(position);
        LinearLayout ll = ViewHolder.get(v, R.id.ll);
        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, NeedDetailActivity.class);
                intent.putExtra("need", item);
                mContext.startActivity(intent);
            }
        });
        TextView nickTv = ViewHolder.get(v, R.id.nick_tv);
        TextView subjectTv = ViewHolder.get(v, R.id.subject_tv);
        TextView priceTv = ViewHolder.get(v, R.id.price_tv);
        TextView dateTv = ViewHolder.get(v, R.id.date_tv); // added by Adonis 2017.6.30
        nickTv.setText(item.getNickName());
        subjectTv.setText(item.getSubject());
        priceTv.setText(item.getPrice());
        dateTv.setText(item.getRegdt().substring(0,10)); // added by Adonis 2017.6.30
        TextView wantTv = ViewHolder.get(v, R.id.want_tv);
        //TODO: 수정횟수 추가할거
        String wantStr = String.format(mContext.getString(R.string.want_date_want_update_count), item.getTerm(), 4);
        wantTv.setText(Html.fromHtml(wantStr));
        ImageView picIv = ViewHolder.get(v,R.id.pic_iv);
        if(item.getUserPic() != null){
            if (!item.getUserPic().contains("http")) {
                item.setUserPic(UrlInfo.URL_HEADER + UrlInfo.API_HEADER + "data/profile/" + item.getUserPic());
            }
            Glide.with(mContext)
                    .load(item.getUserPic() )
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(picIv);
        }
        return v;
    }
}
