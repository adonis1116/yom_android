package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.model.PointHistory;
import com.kakaoapps.tamutamu.model.PointUseHistory;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Panther on 1/15/2017.
 */

public class PointUseHistoryAdapter extends CommonBaseAdapter {
    private Context mContext;
    private ArrayList<PointUseHistory> mList; //환급처리이력목록
    private LayoutInflater inflater;

    public PointUseHistoryAdapter(Context context, ArrayList<PointUseHistory> list){
        mContext = context;
        mList = list;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public PointUseHistory getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) { return mList.get(position).id; }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        // 아이템 뷰 얻기
        if(v == null) v = inflater.inflate(R.layout.item_point_use_history, null);
        LinearLayout ll = ViewHolder.get(v, R.id.ll);

        // 배경색설정
        if(position % 2 == 0) ll.setBackgroundColor(Color.parseColor("#FFFFFF"));
        else ll.setBackgroundColor(Color.parseColor("#F8F8F8"));

        // 선택된 아이템얻기
        PointUseHistory item = getItem(position);

        // 등록날자 얻기
        TextView dateTv = ViewHolder.get(v, R.id.date_tv);
        dateTv.setText(item.regdt);

        // 닉네임 얻기
        TextView nickTv = ViewHolder.get(v, R.id.nick_name_tv);
        nickTv.setText(item.trader_name);

        //String point = Integer.toString(item.point);
        String point = NumberFormat.getNumberInstance(Locale.US).format(item.point);
        String viewMoney = NumberFormat.getNumberInstance(Locale.US).format(item.point);

        // 내용 얻기
        if (item.type==1){ // 구매
            point = "+" + point;
            viewMoney = "+" + viewMoney;
        }else if (item.type==2) // 판매
        {
            point = "-" + point;
            viewMoney = "-" + viewMoney;
        }

        TextView contentTv = ViewHolder.get(v, R.id.content_tv);
        contentTv.setText(point + "P " + "구매");

        // 현재 포인트 얻기
        TextView pointTv = ViewHolder.get(v, R.id.point_tv);
        pointTv.setText(viewMoney + "원");
        return v;
    }
}
