/*
package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kakaoapps.tamutamu.Const;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.utils.DebugUtils;

*/
/**
 * Created by john on 2016-02-24.
 *//*

public class MenuAdapter extends CommonBaseAdapter {

    private final String TAG = getClass().getSimpleName();
    private boolean selectFlags[];

    private Context mContext;
    private LayoutInflater inflater;

    public MenuAdapter(Context context){
        mContext = context;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        selectFlags = new boolean[Const.PROGRESS_MENUS.length];
        selectFlags[0] = true;
    }

    @Override
    public int getCount() {
        return Const.PROGRESS_MENUS.length;
    }

    @Override
    public String getItem(int position) {
        return Const.PROGRESS_MENUS[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        if(v == null)v = inflater.inflate(R.layout.item_menu, null);
        TextView tv = ViewHolder.get(v, R.id.tv);
        tv.setText(getItem(position));
        final View line = ViewHolder.get(v, R.id.line);
        if(selectFlags[position])line.setVisibility(View.VISIBLE);
        else line.setVisibility(View.INVISIBLE);
        return v;
    }

    public void selectMenu(int position){
        for(int i=0; i<selectFlags.length; i++){
            if(i == position)selectFlags[i] = true;
            else selectFlags[i] = false;
        }
        notifyDataSetChanged();
    }
}
*/
