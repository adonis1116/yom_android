package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kakaoapps.tamutamu.Const;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.model.PointHistory;
import com.kakaoapps.tamutamu.model.Question;

import java.util.ArrayList;

/**
 * Created by developer on 2016-06-30.
 */
public class PointHistoryAdapter extends CommonBaseAdapter{
    private Context mContext;
    private ArrayList<PointHistory> mList;
    private LayoutInflater inflater;

    public PointHistoryAdapter(Context context, ArrayList<PointHistory> list){
        mContext = context;
        mList = list;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public PointHistory getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        if(v == null) v = inflater.inflate(R.layout.item_point_history, null);
        LinearLayout ll = ViewHolder.get(v, R.id.ll);
        if(position % 2 == 0) ll.setBackgroundColor(Color.parseColor("#FFFFFF"));
        else ll.setBackgroundColor(Color.parseColor("#FBFBFB"));

        PointHistory item = getItem(position);

        TextView dateTv = ViewHolder.get(v, R.id.date_tv);
        dateTv.setText(item.regdt);
        TextView contentTv = ViewHolder.get(v, R.id.content_tv);
        //contentTv.setText(item.goods);
        contentTv.setText(String.format("[%1s]%3s",item.category, item.title));
        TextView pointTv = ViewHolder.get(v, R.id.point_tv);
        pointTv.setText(item.point + "P");
        return v;
    }
}
