package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kakaoapps.tamutamu.Const;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.activity.CanDetailActivity;
import com.kakaoapps.tamutamu.model.Can;
import com.kakaoapps.tamutamu.widget.HorizontalListView;

import java.util.ArrayList;

/**
 * Created by john on 2016-02-04.
 */
public class MypageCanAdapter extends CommonBaseAdapter {

    private final String TAG = getClass().getSimpleName();
    private Context mContext;
    private ArrayList<Can> mList;
    private LayoutInflater inflater;

    public MypageCanAdapter(Context context, ArrayList<Can> list){
        mContext = context;
        mList = list;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        if(v == null)v = inflater.inflate(R.layout.item_mypage_can, null);

        final Can item = (Can)getItem(position);

        LinearLayout ll = ViewHolder.get(v, R.id.ll);
        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, CanDetailActivity.class);
                intent.putExtra("can", item);
                mContext.startActivity(intent);
            }
        });
        TextView nickTv = ViewHolder.get(v, R.id.nick_tv);
        TextView priceTv = ViewHolder.get(v, R.id.price_tv);
        TextView subjectTv = ViewHolder.get(v, R.id.subject_tv);
        TextView dateTv = ViewHolder.get(v, R.id.date_tv); // added by Adonis 2017.6.30

        ImageView iv = ViewHolder.get(v, R.id.iv);

        HorizontalListView hlv = ViewHolder.get(v, R.id.hlv);

        TagAdapter adapter = new TagAdapter(mContext, item.getTagList(), Color.WHITE);
        hlv.setAdapter(adapter);

        subjectTv.setText(item.getSubject());
        nickTv.setText(item.getNickName());
        priceTv.setText(Const.MONEY_DF.format(Long.valueOf(item.getPrice())));
        dateTv.setText(item.getRegdt().substring(0,10)); // added by Adonis 2017.6.30

        if(item.getImgList().size() > 0){ Glide.with(mContext)
                .load(item.getImgList().get(0))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(iv);
        }else{ // added by RGJ 2017.02.17
            Glide.with(mContext)
                .load(R.drawable.no_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(iv);
        }
        return v;
    }
}
