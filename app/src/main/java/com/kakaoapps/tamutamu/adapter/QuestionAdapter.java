package com.kakaoapps.tamutamu.adapter;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kakaoapps.tamutamu.Const;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.activity.MypageActivity;
import com.kakaoapps.tamutamu.activity.OtherPageActivity;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.manager.PreferencesManager;
import com.kakaoapps.tamutamu.model.Question;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * Created by developer on 2016-06-28.
 */
public class QuestionAdapter extends CommonBaseAdapter {

    private Context mContext;
    private ArrayList<Question> mList;
    private LayoutInflater inflater;
    private PreferencesManager pm;

    public QuestionAdapter(Context context, ArrayList<Question> list){
        mContext = context;
        mList = list;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        pm = new PreferencesManager(mContext);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Question getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {

        final Question item = getItem(position);
        if(item.sendId.equals(pm.getUser().getUserId())){
            v = inflater.inflate(R.layout.item_question_send, null);

            if(item.content.trim().length() > 0){
                TextView contentTv = ViewHolder.get(v, R.id.content_tv);
                contentTv.setVisibility(View.VISIBLE);
                contentTv.setText(item.content);
            }


        }else{
            v = inflater.inflate(R.layout.item_question_receive, null);

            ImageView picIv = ViewHolder.get(v, R.id.pic_iv);
            picIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(mContext, OtherPageActivity.class);
                    intent.putExtra("receive_id", item.sendId);
                    mContext.startActivity(intent);
                }
            });



            if(item.receivePath != null && item.receivePath.trim().length() > 0){
                Glide.with(mContext)
                        .load(item.receivePath)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(picIv);
            }



            TextView nickTv = ViewHolder.get(v, R.id.nick_tv);
            nickTv.setText(item.sendId);

            if(item.content.trim().length() > 0){
                TextView contentTv = ViewHolder.get(v, R.id.content_tv);
                contentTv.setVisibility(View.VISIBLE);
                contentTv.setText(item.content);
            }


        }

        if(item.file.trim().length() > 0){
            if(item.fileType == 1){
                ImageView iv = ViewHolder.get(v, R.id.iv);
                iv.setVisibility(View.VISIBLE);
                Glide.with(mContext)
                        .load(item.file)
                        .bitmapTransform(new RoundedCornersTransformation(mContext, 15, 0, RoundedCornersTransformation.CornerType.ALL))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(iv);
            }else{
                LinearLayout fileLl = ViewHolder.get(v, R.id.file_ll);
                fileLl.setVisibility(View.VISIBLE);
                TextView fileNameTv = ViewHolder.get(v, R.id.file_name_tv);
                TextView fileSizeTv = ViewHolder.get(v, R.id.file_size_tv);
                TextView fileActiveTv = ViewHolder.get(v, R.id.file_active_tv);


                final File file = new File(Const.SAVE_FILE_DIR + item.fileName);

                if(file.exists()) fileActiveTv.setText("열기");

                else fileActiveTv.setText("다운로드");


                fileActiveTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(file.exists()){
                            try{
                                Uri uri = Uri.fromFile(file);
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.addCategory(Intent.CATEGORY_DEFAULT);

                                if (file.toString().contains(".doc") || file.toString().contains(".docx")) {
                                    // Word document
                                    intent.setDataAndType(uri, "application/msword");
                                } else if(file.toString().contains(".pdf")) {
                                    // PDF file
                                    intent.setDataAndType(uri, "application/pdf");
                                } else if(file.toString().contains(".ppt") || file.toString().contains(".pptx")) {
                                    // Powerpoint file
                                    intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
                                } else if(file.toString().contains(".xls") || file.toString().contains(".xlsx")) {
                                    // Excel file
                                    intent.setDataAndType(uri, "application/vnd.ms-excel");
                                } else if(file.toString().contains(".zip") || file.toString().contains(".rar")) {
                                    // WAV audio file
                                    intent.setDataAndType(uri, "application/zip");
                                } else if(file.toString().contains(".rtf")) {
                                    // RTF file
                                    intent.setDataAndType(uri, "application/rtf");
                                } else if(file.toString().contains(".wav") || file.toString().contains(".mp3")) {
                                    // WAV audio file
                                    intent.setDataAndType(uri, "audio/x-wav");
                                } else if(file.toString().contains(".gif")) {
                                    // GIF file
                                    intent.setDataAndType(uri, "image/gif");
                                } else if(file.toString().contains(".jpg") || file.toString().contains(".jpeg") || file.toString().contains(".png")) {
                                    // JPG file
                                    intent.setDataAndType(uri, "image/jpeg");
                                } else if(file.toString().contains(".txt")) {
                                    // Text file
                                    intent.setDataAndType(uri, "text/plain");
                                } else if(file.toString().contains(".3gp") || file.toString().contains(".mpg") || file.toString().contains(".mpeg") || file.toString().contains(".mpe") || file.toString().contains(".mp4") || file.toString().contains(".avi")) {
                                    // Video files
                                    intent.setDataAndType(uri, "video/*");
                                } else if (file.toString().contains("hwp")) {
                                    intent.setDataAndType(uri, "application/hansofthwp");
                                } else {
                                    intent.setDataAndType(uri, "*/*");
                                }

                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                mContext.getApplicationContext().startActivity(intent);
                            }
                            catch(ActivityNotFoundException e)
                            {
                                Toast.makeText(mContext, item.fileName + "을 확인할 수 있는 앱이 없습니다.", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            fileDownload(item.file, item.fileName);
                        }

                    }
                });

                fileNameTv.setText(item.fileName);
                fileSizeTv.setText(item.fileSize + "|" + item.regdate);
            }

        }


        return v;
    }

    public void setList(ArrayList<Question> list){
        mList = list;
    }

    private static String getExtension(String fileStr) {
        return fileStr.substring(fileStr.lastIndexOf(".") + 1, fileStr.length());
    }

    private void fileDownload(String url, String fileName){
        DialogView.progressDialogShow(mContext);
        new HttpsManager(mContext).download(url, fileName, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                Toast.makeText(mContext, Const.SAVE_FILE_DIR + (String)result + " 에 저장하였습니다.", Toast.LENGTH_SHORT).show();
                DialogView.progressDialogClose();
                notifyDataSetChanged();
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
            }
        });
    }
}
