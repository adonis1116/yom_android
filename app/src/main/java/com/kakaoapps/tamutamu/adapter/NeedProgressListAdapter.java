package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kakaoapps.tamutamu.Const;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.activity.OtherPageActivity;
import com.kakaoapps.tamutamu.activity.ReviewWriteActivity;
import com.kakaoapps.tamutamu.dialog.BuyConfirmAskDialogue;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.dialog.PaymentDialog;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.manager.PreferencesManager;
import com.kakaoapps.tamutamu.model.Process;

import com.loopj.android.http.RequestParams;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * Created by john on 2016-02-24.
 */
public class NeedProgressListAdapter extends CommonBaseAdapter {

    private final String TAG = getClass().getSimpleName();
    private Context mContext;
    private ArrayList<Process> mList;
    private LayoutInflater inflater;

    public NeedProgressListAdapter(Context context, ArrayList<Process> list){
        mContext = context;
        mList = list;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Process getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View v, ViewGroup parent) {
        if(v == null)v = inflater.inflate(R.layout.item_progress, null);
        final Process item = getItem(position);
        final int type = item.getType();
        ImageView iv = ViewHolder.get(v, R.id.iv);
        TextView contentTv = ViewHolder.get(v, R.id.content_tv);
        TextView nickTv = ViewHolder.get(v, R.id.nick_tv);
        TextView priceTv = ViewHolder.get(v, R.id.price_tv);

        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, OtherPageActivity.class);
                intent.putExtra("write_id", item.getUserNo());
                mContext.startActivity(intent);
            }
        });

        Glide.with(mContext)
                .load(item.getPic())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(iv);

        priceTv.setText(Const.MONEY_DF.format(Long.valueOf(item.getPrice())));
        nickTv.setText(item.getUserNick());
        contentTv.setText(item.getContent());

        Button bt1 = ViewHolder.get(v, R.id.bt1);
        bt1.setText(getButtonText(item));
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            if(new PreferencesManager(mContext).getUser().getUserNo() != item.getUserNo()){
                switch (type){
                    case 2:
                        new PaymentDialog(mContext, item.getPrice(), 2, item, new PaymentDialog.PaymetListner() {
                            @Override
                            public void onSuccess() {
                                item.setType(item.getType() + 1);
                                mList.set(position, item);
                                notifyDataSetChanged();
                            }
                        }).show();
                        break;
                    case 1:
                        action(position, item.getId(), type);
                        break;
                    case 4://commented by Adonis 2017.07.08
                        final Intent intent = new Intent(mContext, ReviewWriteActivity.class);
                        intent.putExtra("user_id", item.getUserNo());
                        if(intent != null) mContext.startActivity(intent);
                        action(position, item.getId(), type);
                        break;
                }
            }else{
                switch (type){
                    case 3:
                        new BuyConfirmAskDialogue(mContext, new BuyConfirmAskDialogue.ChoiceListner() {
                            @Override
                            public void confirm(File file) {
                                if (file == null){
                                    action(position, item.getId(), type);
                                }
                                else{
                                    send(file, position, type);
                                }
                            }
                            @Override
                            public void cancel() {

                            }
                        }).show();

                        break;

                }
            }
            }
        });

        if(new PreferencesManager(mContext).getUser().getUserNo() != item.getUserNo()){
            switch (type){
                case 5:
                case 3:
                case 100:
                    bt1.setBackgroundResource(R.drawable.gray_round_box);
                    break;
                case 1:
                case 2:
                case 4:
                    bt1.setBackgroundResource(R.drawable.purple_round_box);
                    break;
            }
        }else{
            switch (type){
                case 1:
                case 2:
                case 4:
                case 5:
                case 100:
                    bt1.setBackgroundResource(R.drawable.gray_round_box);
                    break;
                case 3:
                    bt1.setBackgroundResource(R.drawable.purple_round_box);
                    break;
            }
        }

        Button bt2 = ViewHolder.get(v, R.id.bt2);
        if(type == 2 && new PreferencesManager(mContext).getUser().getUserNo() != item.getUserNo()){

            bt2.setVisibility(View.VISIBLE);
            bt2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    action(position, item.getId(), 100);
                }
            });
        }else{
            bt2.setVisibility(View.GONE);
        }

        Button bt_downfile = ViewHolder.get(v, R.id.bt_downfile);
        if (!item.getUpfile().isEmpty()){
            if(type == 4 && new PreferencesManager(mContext).getUser().getUserNo() != item.getUserNo()){
                bt_downfile.setVisibility(View.VISIBLE);
                bt_downfile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DialogView.progressDialogShow(mContext);
                        new HttpsManager(mContext).download(item.getUpfile(), item.getUpreal(), new HttpsManager.HttpListner() {
                            @Override
                            public void onSuccess(Object result) {
                                Toast.makeText(mContext, Const.SAVE_FILE_DIR + (String)result + " 에 저장하였습니다.", Toast.LENGTH_SHORT).show();
                                DialogView.progressDialogClose();
                                notifyDataSetChanged();
                            }

                            @Override
                            public void onFailure(String errorStr) {
                                DialogView.progressDialogClose();
                            }
                        });
                    }
                });
            }
        }
        else{
            bt_downfile.setVisibility(View.GONE);
        }

        return v;
    }


    private void action(final int position, int id, final int type){
        DialogView.progressDialogShow(mContext);
        RequestParams params = new RequestParams();
        params.put("idx", id);
        params.put("type", type);
        params.put("user_no", new PreferencesManager(mContext).getUser().getUserNo());

        new HttpsManager(mContext).needAction(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                Process item = mList.get(position);
                if (type < 100) {
                    item.setType(type + 1);
                } else {
                    item.setType(type);
                }

                mList.set(position, item);
                notifyDataSetChanged();
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                Toast.makeText(mContext, errorStr, Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void send(final File file , final int position, final int type){
        DialogView.progressDialogShow(mContext);
        RequestParams params = new RequestParams();
        PreferencesManager pm = new PreferencesManager(mContext);
        params.put("user_no", pm.getUser().getUserNo());
        params.put("user_id", pm.getUser().getUserId());
        params.put("g_user_no", mList.get(position).getUserNo());
        params.put("buy_confirm", 1); // 구매확정요청인가 0: 아니 1: 예
        params.put("g_type", 2); // 해줄게욤인가? 1: 예 2: 해주세욤
        params.put("idx",  mList.get(position).getId());
        if(file != null){
            try {
                params.put("upfile", file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        new HttpsManager(mContext).registMemo(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                action(position, mList.get(position).getId(), type);
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                Toast.makeText(mContext, "첨부파일을 보낼수가 없네요. 쪽지를 이용해 시도해보세요.", Toast.LENGTH_LONG);
            }
        });

    }

    private String getButtonText(Process item){
        String str = "";
        if(new PreferencesManager(mContext).getUser().getUserNo() != item.getUserNo()){
            switch (item.getType()){
                case 1: str = "수락하기"; break;
                case 2: str = "결제하기"; break;
                case 3: str = "결제완료"; break;
                case 4: str = "구매확정하기"; break;
                case 5: str = "완료됨"; break;
                case 100: str = "거절됨"; break;
            }
        }else{
            switch (item.getType()){
                case 1: str = "수락대기"; break;
                case 2: str = "결제대기중"; break;
                case 3: str = "구매확정요청"; break;
                case 4: str = "완료대기중"; break;
                case 5: str = "완료됨"; break;
                case 100: str = "거절됨"; break;
            }
        }

        return str;
    }
}
