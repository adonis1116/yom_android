package com.kakaoapps.tamutamu.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kakaoapps.tamutamu.Const;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.UrlInfo;
import com.kakaoapps.tamutamu.activity.NeedDetailActivity;
import com.kakaoapps.tamutamu.model.Need;
import com.kakaoapps.tamutamu.widget.HorizontalListView;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

/**
 * Created by john on 2016-02-04.
 */
public class NeedAdapter extends CommonBaseAdapter{
    private final String TAG = getClass().getSimpleName();
    private Context mContext;
    private ArrayList<Need> mList;
    private LayoutInflater inflater;

    private NeedListner mListner;

    public interface NeedListner{
        void setCategory(int category);
        void setTagString(String strTag);
    }

    public NeedAdapter(Context context, ArrayList<Need> list, NeedListner listner){
        mContext = context;
        mList = list;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mListner = listner;
    }


    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        if(v == null)v = inflater.inflate(R.layout.item_need, null);
        final Need item = (Need)getItem(position);

        LinearLayout ll = ViewHolder.get(v, R.id.ll);
        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, NeedDetailActivity.class);
                intent.putExtra("need", item);
                mContext.startActivity(intent);
            }
        });
        TextView nickTv = ViewHolder.get(v, R.id.nick_tv);
        TextView priceTv = ViewHolder.get(v, R.id.price_tv);
        TextView subjectTv = ViewHolder.get(v, R.id.subject_tv);
        TextView hopeTv = ViewHolder.get(v, R.id.hope_date_tv);
        TextView dateTv = ViewHolder.get(v, R.id.date_tv);

        CircularImageView iv = ViewHolder.get(v, R.id.iv);

        HorizontalListView hlv = ViewHolder.get(v, R.id.hlv);
        hlv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                int category = item.getTagList().get(position).cate;
/*
                String strTag = "";
                for (int i = 0; i < item.getTagList().size(); i++){
                    if ( i == 0 ) strTag = item.getTagList().get(i).tag;
                    else strTag += item.getTagList().get(i).tag;
                }
*/
//                mListner.setCategory(category);
                mListner.setTagString(item.getTagList().get(position).tag);
            }
        });

        TagAdapter adapter = new TagAdapter(mContext, item.getTagList());
        hlv.setAdapter(adapter);

        subjectTv.setText(item.getSubject());
        nickTv.setText(item.getNickName());

/*
        String itemPriceStr = item.getPrice();
        for (int i = item.getPrice().length() - 3; i > 0; i=i-3){
            if (i > 0)
        }
*/
        priceTv.setText(Const.MONEY_DF.format(Long.valueOf(item.getPrice())));
        hopeTv.setText(item.getTerm() + "일");
        dateTv.setText(item.getRegdt().substring(0,10)); // added by Adonis 2017.6.30
        if(item.getUserPic() != null && item.getUserPic().length() > 0){
            if (!item.getUserPic().contains("http")) {
                item.setUserPic(UrlInfo.URL_HEADER + UrlInfo.API_HEADER + "data/profile/" + item.getUserPic());
            }
            Glide.with(mContext)
                    .load(item.getUserPic())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(iv);
        }
        else{ // added by RGJ 2017.02.17
            Glide.with(mContext)
                    .load(R.drawable.no_image)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(iv);
        }
        return v;
    }
}
