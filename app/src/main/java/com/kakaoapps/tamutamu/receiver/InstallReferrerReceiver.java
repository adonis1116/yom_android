package com.kakaoapps.tamutamu.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.kakaoapps.tamutamu.activity.SplashActivity;

/**
 * Created by developer on 2016-07-01.
 */
public class InstallReferrerReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String referrer = intent.getStringExtra("referrer");
        if( referrer == null || referrer.length() == 0) {
            return;
        }

        context.startActivity(new Intent(context, SplashActivity.class));
    }
}
