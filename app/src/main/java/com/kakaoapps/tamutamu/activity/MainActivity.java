package com.kakaoapps.tamutamu.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.kakaoapps.tamutamu.adapter.CanAdapter;
import com.kakaoapps.tamutamu.adapter.NeedAdapter;
import com.kakaoapps.tamutamu.dialog.CategoryDialog;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.dialog.SortDialog;
import com.kakaoapps.tamutamu.manager.ActivityManager;
import com.kakaoapps.tamutamu.manager.AppManager;
import com.kakaoapps.tamutamu.manager.DataBaseManager;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.model.Can;
import com.kakaoapps.tamutamu.model.Event;
import com.kakaoapps.tamutamu.model.Need;
import com.kakaoapps.tamutamu.model.UnreadMsgCount;
import com.kakaoapps.tamutamu.model.User;
import com.kakaoapps.tamutamu.utils.DebugUtils;
import com.kakaoapps.tamutamu.widget.FloatingActionButton;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kakaoapps.tamutamu.R;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class MainActivity extends CommonActivity implements View.OnClickListener{

    private final String TAG = getClass().getSimpleName();
    private final int GO_MYPAGE = 113;
    private final int APP_FINISH = 0;
    private final long FINISH_TIME = 3 * 1000; //앱 종료 시간

    private boolean isFinsh = false;
    private ListView lv;

    private CanAdapter canAdapter;
    private NeedAdapter needAdapter;

    private ArrayList<Can> canList = new ArrayList<Can>();
    private ArrayList<Need> needList = new ArrayList<Need>();
    private JSONArray canNearList = new JSONArray();// added by Adonis  2017.07.06
    private JSONArray needNearList = new JSONArray();// added by Adonis  2017.07.06
    private TextView canTv, needTv;

    static private boolean isCan = true; // changed by RGJ 2017.02.17

    private String desc = "desc";
    private String orderBy = "regdt";
    private int category = 0;
    private String strTag = "";
    private EditText searchEt;

    private ArrayList<Event> eventList;
    private DataBaseManager db;

    private int page = 1;
    private int ROW_LIMIT = 10;

    private LinearLayout searchLl;
    private TextView searchTv, searchTotalTv;

    Boolean gotoMap = false;
    private View headerView;
    private boolean isJustLogin = true;
    private boolean isUnreadMsg = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActivityManager.actList.add(this);
        db = new DataBaseManager(this);
        eventList = new ArrayList<Event>();
        isJustLogin = true;
        fetchUserInfo();
        init();
        getEventList();
    }

    private void init(){

       /* InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow()*/
        setTitlebar();
        titlebar.logoIv.setVisibility(View.VISIBLE);
        titlebar.settingIv.setVisibility(View.VISIBLE);
        titlebar.settingIv.setOnClickListener(this);
        titlebar.processBt.setVisibility(View.VISIBLE);
        titlebar.btMemo.setVisibility(View.VISIBLE);
//        titlebar.rlNoti.setVisibility(View.VISIBLE);
        titlebar.processBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ProgressActivity.class));
            }
        });
        titlebar.btMemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, MemoBoxActivity.class));
            }
        });


        lv = (ListView)findViewById(R.id.lv);
        lv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int size = 0;
                boolean isLast = false;
                if(isCan){
                    size = canList.size();
                    if(size > 0)isLast = page * ROW_LIMIT < canList.get(0).getTotal();
                }else{
                    size = needList.size();
                    if(size > 0)isLast = page * ROW_LIMIT < needList.get(0).getTotal();
                }

                if (size > 0 && lv.getLastVisiblePosition() >= totalItemCount - 1 && isLast) {
                    if(DialogView.mDialog == null){
                        ++page;
                        if(isCan)getCanList();
                        else getNeedList();
                    }
                }
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = null;
                if (isCan) intent = new Intent(MainActivity.this, CanWriteActivity.class);
                else intent = new Intent(MainActivity.this, NeedWriteActivity.class);
                startActivity(intent);
            }
        });
        if(headerView == null)setHeader();
    }

    private void setHeader(){
        headerView = View.inflate(this, R.layout.header_main, null);
        // Added by Adonis 2017.7.1

        Button sortBt = (Button)headerView.findViewById(R.id.sort_bt);
        sortBt.setOnClickListener(this);

        Button cateBt = (Button)headerView.findViewById(R.id.cate_bt);
        cateBt.setOnClickListener(this);

        searchEt = (EditText)headerView.findViewById(R.id.search_et);
        ImageView searchIv = (ImageView)headerView.findViewById(R.id.search_iv);
        searchIv.setOnClickListener(this);

        ImageView searchCancelIv = (ImageView)headerView.findViewById(R.id.searchcancel_iv);
        searchCancelIv.setOnClickListener(this);

        //

        canTv = (TextView)headerView.findViewById(R.id.can_tv);
        needTv = (TextView)headerView.findViewById(R.id.need_tv);

        canTv.setOnClickListener(this);
        needTv.setOnClickListener(this);

        searchLl = (LinearLayout)headerView.findViewById(R.id.search_ll);
        searchTv = (TextView)headerView.findViewById(R.id.search_tv);
        searchTotalTv = (TextView)headerView.findViewById(R.id.search_total_tv);

        ImageView gotoHelp = (ImageView)headerView.findViewById(R.id.gotoHelp);
        gotoHelp.setOnClickListener(this);

        lv.addHeaderView(headerView);
    }

    @Override
    protected void onResume() {
        RequestParams params = new RequestParams();
        params.put("user_id", pm.getUser().getUserId());

        new HttpsManager(this).getUnreadMsgCount(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                UnreadMsgCount umc = ((UnreadMsgCount) result);
                if (umc.getUnreadNotiCount() > 0)
                    titlebar.rlNoti.setVisibility(View.VISIBLE);
                else
                    titlebar.rlNoti.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(String errorStr) {
                setToast(errorStr);
            }
        });

        page = 1;
        if(isCan) { // changed by RGJ 20170217
            canTv.setTextColor(getResources().getColor(R.color.purple));
            canTv.setBackgroundColor(Color.WHITE);
            needTv.setTextColor(getResources().getColor(R.color.gray));
            needTv.setBackgroundResource(R.drawable.gradient_round);
            page = 1;
            getCanList();
        }
        else{ // changed by RGJ 20170217
            canTv.setTextColor(getResources().getColor(R.color.gray));
            canTv.setBackgroundResource(R.drawable.gradient_round);
            needTv.setTextColor(getResources().getColor(R.color.purple));
            needTv.setBackgroundColor(Color.WHITE);
            page = 1;
            getNeedList();
        }
        gotoMap = false;
        super.onResume();
    }

    private void getEventList(){
        if(!networkCheck())return;

        DialogView.progressDialogShow(this);
        new HttpsManager(this).getEvent(new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                checkEvent((ArrayList<Event>)result);
            }

            @Override
            public void onFailure(String errorStr) {

            }
        });
    }

    private void checkEvent(ArrayList<Event> list){
        DebugUtils.setLog(TAG, "size : " + list.size());
        for(Event value : list){
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("id", value.getId());
            Event event = db.getEvent(map);
            if(event != null){
                String closeDateStr  = event.getCloseDate();
                if(closeDateStr != null && closeDateStr.length() > 0){
                    Date date = new Date();
                    SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                    String today = format.format(date);

                    if(!closeDateStr.equals(today)){
                        eventList.add(value);
                    }

                }else{
                    eventList.add(value);
                }

            }else{
                db.insertEvent(value);
                eventList.add(value);
            }
        }

        for(Event value : eventList){
            Intent intent = new Intent(this, EventActivity.class);
            intent.putExtra("event", value);
            startActivity(intent);
        }

    }

    private void fetchUserInfo(){
        RequestParams params = new RequestParams();
        params.put("user_type", pm.getUser().getUserType());
        params.put("user_id", pm.getUser().getUserId());
        if (pm.getUser().getUserType() == 1)
        {
            params.put("user_pass", pm.getUser().getPassword());
        }
        params.put("device_type", 1);
        params.put("device_f_id", AppManager.getDeviceSerialNumber());
        params.put("device_push_token", pm.getPushToken());

        DebugUtils.setLog(TAG, AppManager.getDeviceSerialNumber());

        new HttpsManager(this).login(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                pm.setIsLogin(true);
                User newUser = ((User) result);
                newUser.setPassword(pm.getUser().getPassword());
                pm.setUser(newUser);
                if (!isJustLogin) startActivity(new Intent(MainActivity.this, MypageActivity.class));
            }

            @Override
            public void onFailure(String errorStr) {
                setToast(errorStr);
            }
        });
    }

    private void getCanList(){
        if(!networkCheck()) return;
        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("orderby", orderBy);
        params.put("desc", desc);
        params.put("start", page);
        params.put("pgNum", ROW_LIMIT);
        params.put("user_no", pm.getUser().getUserNo());

        if(category > 0) params.put("category", category);
        else params.put("search", searchEt.getText().toString());
        if ( !strTag.isEmpty() ) params.put("tagstring", strTag);
        new HttpsManager(this).getCanList(params, new HttpsManager.HttpListner() {

            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                HashMap<String, Object> map = (HashMap<String, Object>) result;

                if (page == 1) {
                    canList = (ArrayList<Can>) map.get("list");
                    canAdapter = new CanAdapter(MainActivity.this, canList, new CanAdapter.CanListner() {
                        @Override
                        public void setCategory(int category) {
                            MainActivity.this.category = category;
                            page = 1;
                            getCanList();
                        }

                        @Override
                        public void setTagString(String strTag) {
                            MainActivity.this.strTag = strTag;
                            page = 1;
                            getCanList();
                        }

                    });
                    lv.setAdapter(canAdapter);
                    setSeachResult();
                } else {
                    canList.addAll((ArrayList<Can>) map.get("list"));
                    canAdapter.notifyDataSetChanged();
                }

                canNearList = ((JSONArray) map.get("near_users_array"));

                if(gotoMap){
                    Intent intent = new Intent(MainActivity.this, MapViewActivity.class);
                    intent.putExtra( "near", canNearList.toString() );
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }

    private void getNeedList(){
        if(!networkCheck()) return;
        RequestParams params = new RequestParams();
        params.put("orderby", orderBy);
        params.put("desc", desc);
        params.put("search", searchEt.getText().toString());
        params.put("start", page);
        params.put("pgNum", ROW_LIMIT);
        params.put("user_no", pm.getUser().getUserNo());

        if(category > 0)params.put("category", category);
        else if(searchEt.getText().toString().trim().length() > 0)params.put("search", searchEt.getText().toString());
        if ( !strTag.isEmpty() ) params.put("tagstring", strTag);
        DialogView.progressDialogShow(this);

        new HttpsManager(this).getNeedList(params, new HttpsManager.HttpListner() {

            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();

                HashMap<String, Object> map = (HashMap<String, Object>) result;
                if (page == 1) {
                    needList = (ArrayList<Need>) map.get("list");
                    needAdapter = new NeedAdapter(MainActivity.this, needList, new NeedAdapter.NeedListner() {
                        @Override
                        public void setCategory(int category) {
                            MainActivity.this.category = category;
                            page = 1;
                            getNeedList();
                        }

                        @Override
                        public void setTagString(String strTag) {
                            MainActivity.this.strTag = strTag;
                            page = 1;
                            getNeedList();
                        }
                    });
                    lv.setAdapter(needAdapter);
                    setSeachResult();
                } else {
                    needList.addAll((ArrayList<Need>) map.get("list"));
                    needAdapter.notifyDataSetChanged();
                }
                needNearList = ((JSONArray) map.get("near_users_array"));

                if(gotoMap){
                    Intent intent = new Intent(MainActivity.this, MapViewActivity.class);
                    intent.putExtra( "near", needNearList.toString() );
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }

    private void setSeachResult(){
        if(searchEt.getText().toString().trim().length() > 0){
            searchLl.setVisibility(View.VISIBLE);

            if(isCan){
                if(canList.size() == 0) searchTotalTv.setText("(0)");
                else searchTotalTv.setText("(" + canList.get(0).getTotal() + ")");
            }else{
                if(needList.size() == 0) searchTotalTv.setText("(0)");
                else searchTotalTv.setText("(" + needList.get(0).getTotal() + ")");
            }
            searchTv.setText("\"" + searchEt.getText().toString() + "\"");

            // Modified by Adonis 2017.7.1
            ImageView searchIv = (ImageView)findViewById(R.id.search_iv);
            searchIv.setVisibility(View.GONE);

            ImageView searchCancelIv = (ImageView)findViewById(R.id.searchcancel_iv);
            searchCancelIv.setVisibility(View.VISIBLE);
            //

        }else{
            searchLl.setVisibility(View.GONE);
        }

        searchEt.setText("");
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.sort_bt:
                searchEt.setText("");
                new SortDialog(this, sortListner).show();
                break;
            case R.id.can_tv:
                setTab(R.id.can_tv);
                break;
            case R.id.need_tv:
                setTab(R.id.need_tv);
                break;
            case R.id.setting_iv:
                isJustLogin = false;
                fetchUserInfo();
                break;
            case R.id.search_iv:
                if(searchEt.getText().toString().trim().length() > 0){
                    page = 1;
                    if(isCan)getCanList();
                    else getNeedList();

                }else setToast("검색어를 입력해주세요");

                break;
            // Added by Adonis 2017.7.1
            case R.id.searchcancel_iv:
                page = 1;
                if(isCan) getCanList();
                else getNeedList();

                // Modified by Adonis 2017.7.1
                ImageView searchIv = (ImageView)findViewById(R.id.search_iv);
                searchIv.setVisibility(View.VISIBLE);

                ImageView searchCancelIv = (ImageView)findViewById(R.id.searchcancel_iv);
                searchCancelIv.setVisibility(View.GONE);

                break;
            case R.id.cate_bt:
                new CategoryDialog(MainActivity.this, categoryListner).show();
                break;
            //added by Adonis 2017.07.14
            case R.id.gotoHelp:
                Intent intent = new Intent(this, UseGuideActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void setTab(int resId){

        // Modified by Adonis 2017.7.1
        ImageView searchIv = (ImageView)findViewById(R.id.search_iv);
        searchIv.setVisibility(View.VISIBLE);

        ImageView searchCancelIv = (ImageView)findViewById(R.id.searchcancel_iv);
        searchCancelIv.setVisibility(View.GONE);
        //

        orderBy = "regdt";
        desc = "desc";
        searchEt.setText("");
        strTag = "";
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(searchEt.getWindowToken(), 0);

        category = 0;

        if(R.id.can_tv == resId){
            if(isCan)return;
            canTv.setTextColor(getResources().getColor(R.color.purple));
            canTv.setBackgroundColor(Color.WHITE);
            needTv.setTextColor(getResources().getColor(R.color.gray));
            needTv.setBackgroundResource(R.drawable.gradient_round);
            isCan = true;
            page = 1;
            getCanList();
        }else{
            if(!isCan)return;
            canTv.setTextColor(getResources().getColor(R.color.gray));
            canTv.setBackgroundResource(R.drawable.gradient_round);
            needTv.setTextColor(getResources().getColor(R.color.purple));
            needTv.setBackgroundColor(Color.WHITE);
            isCan = false;
            page = 1;
            getNeedList();
        }

    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == GO_MYPAGE){
            if(RESULT_OK == resultCode){
                startActivity(new Intent(this, LoginActivity.class));
                finish();
            }
        }
    }*/

    SortDialog.SortListner sortListner = new SortDialog.SortListner(){

        @Override
        public void closeLocation() {
            orderBy = "area_code";
            desc = "desc";
            if(isCan)getCanList();
            else getNeedList();
        }

        @Override
        public void newRegister() {
            orderBy = "regdt";
            desc = "desc";
            if(isCan)getCanList();
            else getNeedList();
        }

        @Override
        public void rowPrice() {
            orderBy = "price";
            desc = "asc";
            if(isCan)getCanList();
            else getNeedList();
        }

        @Override
        public void selectCategory() {
            new CategoryDialog(MainActivity.this, categoryListner).show();
        }

        //added by Adonis 2017.07.04
        @Override
        public void openMapView(){
            gotoMap = true;
            orderBy = "area_code";
            if(isCan) getCanList();
            else getNeedList();
        }
    };

    private CategoryDialog.CategoryListner categoryListner = new CategoryDialog.CategoryListner() {
        @Override
        public void selectCategory(int category) {
            MainActivity.this.category = category;
            if(isCan)getCanList();
            else getNeedList();
        }
    };

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case APP_FINISH:
                    isFinsh = false;
                    break;
            }
        }
    };

    @Override
    public void onBackPressed() {
        if(!isFinsh){
            setToast("한 번 더 누르면 앱이 종료됩니다.");
            isFinsh = true;
            mHandler.sendEmptyMessageDelayed(APP_FINISH, FINISH_TIME);
        }else{
            setToast("앱이 종료되었습니다.");
            finish();
        }
    }
}
