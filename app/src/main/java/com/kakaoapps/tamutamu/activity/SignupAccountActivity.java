package com.kakaoapps.tamutamu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.model.User;
import com.kakaoapps.tamutamu.utils.CheckPatternUtils;
import com.loopj.android.http.RequestParams;

/**
 * Created by john on 2016-02-01.
 */
public class SignupAccountActivity extends CommonActivity implements View.OnClickListener{

    private EditText emailEt, passEt, confirmEt;
    private CheckBox cb1, cb2;
    private boolean isEmailCheck = false;
    private String checkEmailStr = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_account);

        init();

    }

    private void init(){
        setTitlebar();
        titlebar.centerTv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText("회원가입");


        emailEt = (EditText)findViewById(R.id.email_et);
        passEt = (EditText)findViewById(R.id.pass_et);
        confirmEt = (EditText)findViewById(R.id.confirm_et);

        Button provisionBt1 = (Button)findViewById(R.id.provision_bt1);
        Button provisionBt2 = (Button)findViewById(R.id.provision_bt2);
        provisionBt1.setOnClickListener(this);
        provisionBt2.setOnClickListener(this);

        cb1 = (CheckBox)findViewById(R.id.cb1);
        cb2 = (CheckBox)findViewById(R.id.cb2);

        Button emailCheckBt = (Button)findViewById(R.id.check_bt);
        emailCheckBt.setOnClickListener(this);
        footer();
    }

    private void footer(){
        TextView tv1 = (TextView)findViewById(R.id.tv1);
        TextView tv2 = (TextView)findViewById(R.id.tv2);

        tv1.setText("이전");
        tv2.setText("다음");
        tv1.setOnClickListener(this);
        tv2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()){
            case R.id.tv1:
                finish();
                break;
            case R.id.tv2:
                check();
                break;
            case R.id.provision_bt1:
                intent = new Intent(this, ProvisionActivity.class);
                intent.putExtra("type", 1);
                break;
            case R.id.provision_bt2:
                intent = new Intent(this, ProvisionActivity.class);
                intent.putExtra("type", 2);
                break;
            case R.id.check_bt:
                emailCheck();
                break;
        }
        if(intent != null)startActivity(intent);
    }

    private void check(){

        if(emailEt.getText().toString().trim().length() == 0){
            setToast("이메일을 넣어주세요.");
            return;
        }

        if(!new CheckPatternUtils().checkPattern(emailEt.getText().toString(), CheckPatternUtils.PatternType.EMAIL)){
            setToast("이메일형식이 맞지 않습니다.");
            return;
        }

        if(passEt.getText().toString().trim().length() == 0){
            setToast("비밀번호를 넣어주세요.");
            return;
        }

        if(passEt.getText().toString().trim().length() < 4){
            setToast("비밀번호는 4자리 이상입니다.");
            return;
        }

        if(confirmEt.getText().toString().trim().length() == 0){
            setToast("비밀번호를 확인해주세요");
            return;
        }

        if(!passEt.getText().toString().equals(confirmEt.getText().toString())){
            setToast("비밀번호가 맞지 않습니다.");
            return;
        }

        if(!cb1.isChecked()){
            setToast("이용약관에 동의해주세요.");
            return;
        }

        if(!cb2.isChecked()){
            setToast("개인정보취급방침에 동의해주세요.");
            return;
        }

        if(!isEmailCheck && !checkEmailStr.equals(emailEt.getText().toString())){
            setToast("이메일 중복확인를 해주세요.");
            isEmailCheck = false;
            return;
        }

        next();
    }

    private void emailCheck(){

        if(emailEt.getText().toString().length() == 0){
            setToast("이메일을 넣어주세요.");
            return;
        }

        if(!new CheckPatternUtils().checkPattern(emailEt.getText().toString(), CheckPatternUtils.PatternType.EMAIL)){
            setToast("이메일형식이 맞지 않습니다.");
            return;
        }

        if(!networkCheck())return;

        RequestParams params = new RequestParams();
        params.put("user_id", emailEt.getText().toString());
        new HttpsManager(this).emailCheck(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                setToast((String)result);
                isEmailCheck = true;
                checkEmailStr = emailEt.getText().toString();
            }

            @Override
            public void onFailure(String errorStr) {
                isEmailCheck = false;
                setToast(errorStr);
            }
        });
    }



    private void next(){
        User user = new User();
        user.setUserId(emailEt.getText().toString());
        user.setPassword(passEt.getText().toString());
        user.setUserType(1);
        Intent intent = new Intent(this, SignupProfileActivity.class);
        intent.putExtra("user", user);
        startActivity(intent);
    }
}
