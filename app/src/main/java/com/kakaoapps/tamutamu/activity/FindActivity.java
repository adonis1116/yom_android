package com.kakaoapps.tamutamu.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.dialog.OneButtonDialog;
import com.kakaoapps.tamutamu.dialog.TwoButtonDialog;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;


import org.json.JSONObject;

import org.apache.http.Header;

/**
 * Created by john on 2016-02-01.
 */
public class FindActivity extends CommonActivity implements View.OnClickListener{

    private EditText mobileEt1, idEt, mobileEt2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find);

        setTitlebar();
        titlebar.backIv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText("아이디/비밀번호찾기");
        Button confirmBt1 = (Button)findViewById(R.id.confirm_bt1);
        Button confirmBt2 = (Button)findViewById(R.id.confirm_bt2);
        confirmBt1.setOnClickListener(this);
        confirmBt2.setOnClickListener(this);

        mobileEt1 = (EditText)findViewById(R.id.mobile_et1);
        idEt = (EditText)findViewById(R.id.id_et);
        mobileEt2 = (EditText)findViewById(R.id.mobile_et2);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirm_bt1:
                findId();
                break;
            case R.id.confirm_bt2:
                findPw(idEt.getText().toString(), mobileEt2.getText().toString());
                break;
        }
    }

    /**아이디 찾기
     *
     */
    private void findId(){

        if(mobileEt1.getText().toString().trim().length() == 0){
            setToast("핸드폰 번호를 입력해주세요");
            return;
        }
        if(!networkCheck())return;

        DialogView.progressDialogShow(this);

        RequestParams params = new RequestParams();
        params.put("user_hp", mobileEt1.getText().toString());
        new HttpsManager(this).findId(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(final Object result) {
                DialogView.progressDialogClose();
                new TwoButtonDialog(FindActivity.this, "아이디 확인", String.format(getString(R.string.id_confirm), (String)result), "로그인", "비밀번호 찾기", new TwoButtonDialog.ChoiceListner() {
                    @Override
                    public void confirm() {
                        finish();
                    }

                    @Override
                    public void cancle() {
                        findPw((String)result, mobileEt1.getText().toString());
                    }
                }).show();
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }

    /**
     * 비밀번호 찾기
     */
    private void findPw(String id, String mobileStr){
        if(id.trim().length() == 0){
            setToast("아이디를 입력해주세요");
            return;
        }

        if(mobileStr.trim().length() == 0){
            setToast("핸드폰 번호를 입력해주세요");
            return;
        }

        if(!networkCheck())return;

        DialogView.progressDialogShow(this);

        RequestParams params = new RequestParams();
        params.put("user_id", id);
        params.put("user_hp", mobileStr);
        new HttpsManager(this).findPw(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                new OneButtonDialog(FindActivity.this, "임시비밀번호 확인", String.format(getString(R.string.password_confirm), (String)result), "로그인", new OneButtonDialog.ChoiceListner() {
                    @Override
                    public void confirm() {
                        finish();
                    }
                }).show();
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }
}
