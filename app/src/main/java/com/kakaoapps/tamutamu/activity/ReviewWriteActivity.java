package com.kakaoapps.tamutamu.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.dialog.TwoButtonDialog;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.loopj.android.http.RequestParams;

/**
 * Created by john on 2016-02-11.
 */
public class ReviewWriteActivity extends CommonActivity implements View.OnClickListener{

    private final int STAR_MAX = 5;
    private ViewGroup.LayoutParams params;
    private ImageView starIvs[] = new ImageView[STAR_MAX];
    private TextView countTv;
    private EditText reviewEt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_write);


        setTitlebar();
        titlebar.backIv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText("리뷰작성");
        titlebar.rightTv.setText("완료");
        titlebar.rightTv.setVisibility(View.VISIBLE);
        titlebar.rightTv.setOnClickListener(this);

        params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayout starLl = (LinearLayout)findViewById(R.id.star_ll);
        for(int i=0; i<STAR_MAX; i++){
            starIvs[i] = new ImageView(this);
            starIvs[i].setLayoutParams(params);
            starIvs[i].setImageResource(R.drawable.uncheck_star);
            starIvs[i].setOnClickListener(this);
            starLl.addView(starIvs[i]);
        }


        countTv = (TextView)findViewById(R.id.count_tv);
        countTv.setText("0.0");

        reviewEt = (EditText)findViewById(R.id.review_et);



    }

    private void writeReview(){
        if(!networkCheck())return;
        RequestParams params = new RequestParams();
        params.put("bidx", getIntent().getIntExtra("user_id", 0));
        params.put("user_no", pm.getUser().getUserNo());
        params.put("content", reviewEt.getText().toString());
        params.put("point", countTv.getText().toString().substring(0));
        new HttpsManager(this).writeReview(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                finish();
            }

            @Override
            public void onFailure(String errorStr) {
                setToast(errorStr);
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v == titlebar.rightTv){


            if(reviewEt.getText().toString().trim().length() == 0){
                setToast("내용을 입렵하세요.");
                return;
            }else{
                new TwoButtonDialog(this, "리뷰작성 완료", "작성된 내용으로\n리뷰를 등록하시겠습니까?", "확인", "취소", new TwoButtonDialog.ChoiceListner() {
                    @Override
                    public void confirm() {
                        writeReview();
                    }

                    @Override
                    public void cancle() {

                    }
                }).show();
            }



        }else{
            for(int i=0; i<STAR_MAX; i++){
                if(v == starIvs[i]){
                    for(int j=0; j<=i; j++){
                        starIvs[j].setImageResource(R.drawable.check_star);
                    }

                    for(int j=i+1; j<STAR_MAX; j++){
                        starIvs[j].setImageResource(R.drawable.uncheck_star);
                    }

                    countTv.setText((i+1) * 2 + ".0");
                    break;
                }
            }
        }
    }

    @Override
    public void onBack(View v) {
        showCanclePopup();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            showCanclePopup();
            return true;
        }else{
            return super.onKeyDown(keyCode, event);
        }
    }

    private void showCanclePopup(){
        new TwoButtonDialog(this, "작성취소", "리뷰작성을 취소하시겠습니까?", "확인", "취소", new TwoButtonDialog.ChoiceListner() {
            @Override
            public void confirm() {
                finish();
            }

            @Override
            public void cancle() {

            }
        }).show();
    }
}
