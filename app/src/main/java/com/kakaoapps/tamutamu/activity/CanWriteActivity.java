package com.kakaoapps.tamutamu.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.angads25.filepicker.controller.DialogSelectionListener;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.kakaoapps.tamutamu.Const;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.adapter.CloseTagAdapter;
import com.kakaoapps.tamutamu.adapter.CommonBaseAdapter;
import com.kakaoapps.tamutamu.adapter.ViewHolder;
import com.kakaoapps.tamutamu.dialog.CategoryDialog;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.dialog.RowTwoButtonDialog;
import com.kakaoapps.tamutamu.dialog.SecondCategoryDialog;
import com.kakaoapps.tamutamu.manager.DataBaseManager;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.manager.SelectPhotoManager;
import com.kakaoapps.tamutamu.model.Can;
import com.kakaoapps.tamutamu.model.Tag;
import com.kakaoapps.tamutamu.utils.DebugUtils;
import com.kakaoapps.tamutamu.widget.HorizontalListView;
import com.loopj.android.http.RequestParams;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by john on 2016-02-05.
 */
public class CanWriteActivity extends CommonActivity implements View.OnClickListener{
    private final String TAG = getClass().getSimpleName();

    private final int SELECT_LOC_REQUEST_CODE = 111;
    private SelectPhotoManager mSelectPhotoManager;
    private ArrayList<PhotoType> photoList;
    private ArrayList<File> filesList;
    private PhotoAdapter adapter;
    private final int PHOTO_MAX = 5;
    private CloseTagAdapter tagAdapter;
    private ArrayList<Tag> tagList;

    private TextView locTv, attach_tv;//added by Adonis 2017.07.16;
    private final int REQUEST_FILE = 1001;
    private EditText subjectEt, priceEt, contentEt;
    private int areaCode, subAreaCode;
    private String latStr, lngStr;
    private Can mItem;
    private ArrayList<File> files;

    private int mPosition = -1;

    class PhotoType{
        Bitmap bitmap;
        String path;
    }
    FilePickerDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_can_write);
        mItem = getIntent().getParcelableExtra("can");
        files = new ArrayList<File>();
        init();
    }

    private void init() {
        setTitlebar();
        titlebar.backIv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText("해줄게욤 글쓰기");
        titlebar.rightTv.setVisibility(View.VISIBLE);
        titlebar.rightTv.setText(mItem == null ? "완료" : "수정");
        titlebar.rightTv.setOnClickListener(this);

        subjectEt = (EditText)findViewById(R.id.subject_et);
        priceEt = (EditText)findViewById(R.id.price_et);
        contentEt = (EditText)findViewById(R.id.content_et);

        photoList = new ArrayList<PhotoType>();
        filesList = new ArrayList<File>();//added by Adonis 2017.07.16
        HorizontalListView tagHlv = (HorizontalListView) findViewById(R.id.tag_hlv);
        tagList = new ArrayList<Tag>();

        Button cateBt = (Button)findViewById(R.id.cate_bt);
        cateBt.setOnClickListener(this);

        Button locBt = (Button) findViewById(R.id.loc_bt);
        locBt.setOnClickListener(this);
        locTv = (TextView)findViewById(R.id.loc_tv);

        //added by Adonis 2017.07.16
        Button attach_bt = (Button)findViewById(R.id.attach_bt);
        attach_bt.setOnClickListener(this);
        attach_tv = (TextView)findViewById(R.id.attach_tv);

        DialogProperties properties = new DialogProperties();
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.selection_type = DialogConfigs.FILE_SELECT;
        properties.root = new File(DialogConfigs.DEFAULT_DIR);
        properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
        properties.offset = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = null;
        dialog = new FilePickerDialog(this,properties);
        dialog.setTitle("파일을 선택하세요.");
        dialog.setDialogSelectionListener(new DialogSelectionListener() {
            @Override
            public void onSelectedFilePaths(String[] files) {
                for(String path:files) {
                    File file = new File(path);
                    filesList.clear();
                    filesList.add(file);
                    attach_tv.setText("첨부파일:" + file.getName());
                    break;
                }
            }
        });

        if(mItem != null){
            subjectEt.setText(mItem.getSubject());
            priceEt.setText("" + mItem.getPrice());
            contentEt.setText(mItem.getContent());

            tagList = mItem.getTagList();
            DataBaseManager db = new DataBaseManager(this);
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("id", mItem.getArea());
            String areaStr = db.getArea(map).getName();
            map.put("id", mItem.getSubArea());
            String subAreaStr = db.getSubArea(map).getName();
            locTv.setText(areaStr + ">" + subAreaStr);

            if (mItem.getFileList().size() > 0)
                attach_tv.setText("첨부파일 있음");//added by Adonis 2017.07.16


            areaCode = mItem.getArea();
            subAreaCode = mItem.getSubArea();

            for(String path : mItem.getImgList()){
                PhotoType type = new PhotoType();
                type.path = path;
                photoList.add(type);
            }

        }
        adapter = new PhotoAdapter(this);
        tagAdapter = new CloseTagAdapter(this, tagList);
        tagHlv.setAdapter(tagAdapter);

        HorizontalListView hlv = (HorizontalListView) findViewById(R.id.hlv);
        hlv.setAdapter(adapter);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SelectPhotoManager.CROP_FROM_CAMERA
                || requestCode == SelectPhotoManager.PICK_FROM_CAMERA
                || requestCode == SelectPhotoManager.PICK_FROM_ALBUM) {
            mSelectPhotoManager.onActivityResult(requestCode, resultCode, data);
        }

        if(requestCode == SelectPhotoManager.CROP_FROM_CAMERA) {
            Bitmap bitmap = data.getExtras().getParcelable("data");
            if (photoList.size() <= PHOTO_MAX) {
                PhotoType photoType = new PhotoType();
                photoType.bitmap = bitmap;
                if (mPosition < photoList.size()) photoList.remove(mPosition);
                photoList.add(mPosition, photoType);
            }
            adapter.notifyDataSetChanged();
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                if (!resultUri.equals("") || resultUri != null){
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap photo = BitmapFactory.decodeFile(resultUri.getPath(), options);
                    if (photoList.size() <= PHOTO_MAX) {
                        PhotoType photoType = new PhotoType();
                        photoType.bitmap = photo;
                        if (mPosition < photoList.size()) photoList.remove(mPosition);
                        photoList.add(mPosition, photoType);
                    }
                    adapter.notifyDataSetChanged();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                return;
            }
        }

        if(requestCode == SELECT_LOC_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                areaCode = data.getIntExtra("area", 0);
                subAreaCode = data.getIntExtra("subarea", 0);
                latStr = data.getStringExtra("lat");
                lngStr = data.getStringExtra("lng");
                locTv.setText(data.getStringExtra("str"));
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.right_tv:
                check();
                break;
            case R.id.cate_bt:
                if(tagAdapter.getCount() < 5) {
                    new CategoryDialog(this, new CategoryDialog.CategoryListner() {
                        @Override
                        public void selectCategory(final int category) {
                            new SecondCategoryDialog(CanWriteActivity.this, category, new SecondCategoryDialog.SecondCategoryListner() {
                                @Override
                                public void onSelectCategory(int position, String cateStr) {
                                    Tag tag = new Tag();
                                    tag.cate = category;
                                    tag.tag = cateStr;
                                    tagList.add(tag);
                                    tagAdapter.notifyDataSetChanged();
                                }
                            }).show();
                        }
                    }).show();
                }else setToast("최대 5개까지만 등록됩니다.");

                break;
            case R.id.loc_bt:
                startActivityForResult(new Intent(this, SelectLocationActivity.class), SELECT_LOC_REQUEST_CODE);
                break;
            case R.id.attach_bt:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int permissionCheck = ContextCompat.checkSelfPermission(CanWriteActivity.this,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                        dialog.show();
                    } else {
                        ActivityCompat.requestPermissions(CanWriteActivity.this,
                                new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,
                                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA}, SelectPhotoManager.MY_CAMERA_REQUEST_CODE);
                    }
                    return;
                } else {
                    dialog.show();
                }

                break;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case FilePickerDialog.EXTERNAL_READ_PERMISSION_GRANT: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(dialog!=null)
                    {   //Show dialog if the read permission has been granted.
                        dialog.show();
                    }
                }
                else {
                    //Permission has not been granted. Notify the user.
                    Toast.makeText(this,"Permission is Required for getting list of files",Toast.LENGTH_SHORT).show();
                }
            }
        }

        if (requestCode == SelectPhotoManager.MY_CAMERA_REQUEST_CODE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(this, "기기에 대한 접근이 허용되었습니다.", Toast.LENGTH_LONG).show();

            } else {

                Toast.makeText(this, "기기에 접근을 제한하면 앱이 비정상으로 동작할수도 있습니다.", Toast.LENGTH_LONG).show();

            }
        }
    }
    private void check(){
        if(subjectEt.getText().toString().trim().length() == 0){
            setToast("제목을 입력하세요.");
            return;
        }

        if(priceEt.getText().toString().trim().length() == 0){
            setToast("금액을 입력하세요.");
            return;
        }

        if(priceEt.getText().toString().trim().length() > 15){
            setToast("금액은 15자리수를 넘을수 없습니다.");
            return;
        }

        if(contentEt.getText().toString().trim().length() == 0){
            setToast("내용을 입력하세요.");
            return;
        }

        if(tagList.size() == 0){
            setToast("카테고리를 선택해주세요");
            return;
        }

        if(locTv.getText().toString().length() == 0){
            setToast("지역을 선택해주세요");
            return;
        }

        if(photoList.size() == 0){
//            setToast("사진을 첨부해주세요");
//            return;
       }

        canWrite();
    }

    private void canWrite(){
        if(!networkCheck()) return;
        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        params.put("subject", subjectEt.getText().toString());
        params.put("content", contentEt.getText().toString());
        params.put("price", priceEt.getText().toString());
        params.put("area_code", areaCode);
        params.put("subarea_code", subAreaCode);
        params.put("lat", latStr);
        params.put("lng", lngStr);

        if(mItem != null){
            params.put("idx", mItem.getId());
            params.put("act", "modify");
        }

        for(int i=0; i<tagList.size(); i++){
            params.put("cate" + (i+1), tagList.get(i).cate);
            params.put("tag" + (i+1), tagList.get(i).tag);
        }

        for(int i=0; i<photoList.size(); i++){
            if(photoList.get(i) != null){
                if(photoList.get(i).bitmap != null){
                    FileOutputStream fos = null;

                    String imagePath =  Environment.getExternalStorageDirectory() + "/" + System.currentTimeMillis() + ".jpg";
                    try {

                        fos = new FileOutputStream(imagePath);
                        photoList.get(i).bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                        File file = new File(imagePath);
                        files.add(file);
                        params.put("img" + (i+1), file);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }finally {
                        if(fos != null)
                            try {
                                fos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                    }
                }else{
                    params.put("img" + (i+1), photoList.get(i).path);
                }
            }
        }

        for(int i=0; i<filesList.size(); i++){
            if (filesList.get(i) != null){
                try{
                    params.put("file" + (i+1), filesList.get(i));
                }
                catch (FileNotFoundException e){
                    e.printStackTrace();
                }
            }
        }

        new HttpsManager(this).writeCan(params, mItem != null, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                setToast("등록되었습니다.");
                deleteImage();

                if(mItem != null){
                    mItem.setSubject(subjectEt.getText().toString());
                    mItem.setPrice(priceEt.getText().toString());
                    mItem.setContent(contentEt.getText().toString());
                    mItem.setTagList(tagList);
                    mItem.setImgList((ArrayList<String>)result);
                    mItem.setArea(areaCode);
                    mItem.setSubArea(subAreaCode);
                    Intent intent = new Intent();
                    intent.putExtra("can", mItem);
                    setResult(RESULT_OK, intent);
                }

                finish();
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
                deleteImage();
            }
        });
    }

    private void deleteImage(){
        for(File file : files){
            if(file != null&& file.exists())file.delete();
        }
    }


    class PhotoAdapter extends CommonBaseAdapter {

        private final String TAG = getClass().getSimpleName();
        private Context mContext;
        private LayoutInflater inflater;



        public PhotoAdapter(Context context) {
            mContext = context;
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return PHOTO_MAX;
        }

        @Override
        public Object getItem(int position) {
            return photoList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View v, ViewGroup parent) {
            if (v == null) v = inflater.inflate(R.layout.item_photo, null);
            RelativeLayout rl = ViewHolder.get(v, R.id.rl);

            if (photoList.size() >= position) {
                rl.setVisibility(View.VISIBLE);
                ImageView photoIv = ViewHolder.get(v, R.id.photo_iv);
                ImageView closeIv = ViewHolder.get(v, R.id.close_iv);
                photoIv.setVisibility(View.VISIBLE);
                mSelectPhotoManager = new SelectPhotoManager(mContext);
                mSelectPhotoManager.from_where = "can";
                photoIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mPosition = position;
                        new RowTwoButtonDialog(mContext, "사진 가져오기", "앨범에서 가져오기", "사진 촬영하기", choiceListner).show();
                    }
                });
                if (photoList.size() > position) {
                    closeIv.setVisibility(View.VISIBLE);
                    PhotoType item = photoList.get(position);
                    if(item.bitmap != null)photoIv.setImageBitmap(item.bitmap);
                    else{
                        Glide.with(mContext)
                                .load(item.path)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(photoIv);
                    }
                    closeIv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            photoList.remove(position);
                            notifyDataSetChanged();
                        }
                    });
                } else {
                    closeIv.setVisibility(View.GONE);
                }
            } else {
                rl.setVisibility(View.GONE);
            }

            return v;
        }


        private RowTwoButtonDialog.ChoiceListner choiceListner = new RowTwoButtonDialog.ChoiceListner() {

            @Override
            public void firstClick() {
                mSelectPhotoManager.doTakeAlbumAction();
            }

            @Override
            public void secondClick() {
                mSelectPhotoManager.doTakePhotoAction();
            }
        };

    }
}
