package com.kakaoapps.tamutamu.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.adapter.RankAdapter;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.model.Rank;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;

/**
 * Created by developer on 2016-06-30.
 */
public class RankActivity extends CommonActivity implements View.OnClickListener{
    private RankAdapter adapter;
    private TextView canTv, needTv;

    private ListView lv;
    private TextView myRankTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rank);
        init();
    }


    private void init(){

        setTitlebar();
        titlebar.backIv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText("랭킹 순위");
        titlebar.centerTv.setVisibility(View.VISIBLE);

        canTv = (TextView)findViewById(R.id.can_tv);
        needTv = (TextView)findViewById(R.id.need_tv);
        canTv.setText("판매자 랭킹");
        needTv.setText("구매자 랭킹");
        canTv.setOnClickListener(this);
        needTv.setOnClickListener(this);
        myRankTv = (TextView)findViewById(R.id.myrank_tv);
        myRankTv.setText(Html.fromHtml(String.format(getString(R.string.my_ranking), 10)));

        lv = (ListView)findViewById(R.id.lv);
        buyerList();

    }

    private void buyerList(){

        RequestParams params = new RequestParams();
        params.put("user_id", pm.getUser().getUserId());
        new HttpsManager(this).getBuyerRankList(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                ArrayList<Rank> list = (ArrayList<Rank>)result;
                adapter = new RankAdapter(RankActivity.this, list);
                lv.setAdapter(adapter);
                String myRankStr = list.get(0).myRank;
                if(myRankStr.equals("null"))myRankTv.setText(Html.fromHtml(getString(R.string.my_not_ranking)));
                else myRankTv.setText(Html.fromHtml(String.format(getString(R.string.my_ranking), myRankStr)));
            }

            @Override
            public void onFailure(String errorStr) {
                setToast(errorStr);
            }
        });
    }

    private void sellerList(){
        RequestParams params = new RequestParams();
        params.put("user_id", pm.getUser().getUserId());
        new HttpsManager(this).getSellerRankList(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                ArrayList<Rank> list = (ArrayList<Rank>)result;
                adapter = new RankAdapter(RankActivity.this, list);
                lv.setAdapter(adapter);
                String myRankStr ="null";
                if(list.size() > 0) myRankStr = list.get(0).myRank;
                if(myRankStr.equals("null"))myRankTv.setText(Html.fromHtml(getString(R.string.my_not_ranking)));
                else myRankTv.setText(Html.fromHtml(String.format(getString(R.string.my_ranking), myRankStr)));
            }

            @Override
            public void onFailure(String errorStr) {
                setToast(errorStr);
            }
        });
    }

    private void setTab(int resId){
        if(R.id.can_tv == resId){
            canTv.setTextColor(getResources().getColor(R.color.purple));
            canTv.setBackgroundColor(Color.WHITE);
            needTv.setTextColor(getResources().getColor(R.color.gray));
            needTv.setBackgroundResource(R.drawable.gradient_round);
            buyerList();
        }else{
            canTv.setTextColor(getResources().getColor(R.color.gray));
            canTv.setBackgroundResource(R.drawable.gradient_round);
            needTv.setTextColor(getResources().getColor(R.color.purple));
            needTv.setBackgroundColor(Color.WHITE);
            sellerList();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.can_tv:
                setTab(R.id.can_tv);
                break;
            case R.id.need_tv:
                setTab(R.id.need_tv);
                break;
        }
    }
}
