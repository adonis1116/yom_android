package com.kakaoapps.tamutamu.activity;

import android.content.Intent;
import android.content.pm.PackageInstaller;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.kakao.auth.AuthType;
import com.kakao.auth.ISessionCallback;
import com.kakao.auth.Session;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.MeResponseCallback;
import com.kakao.usermgmt.response.model.UserProfile;
import com.kakao.util.exception.KakaoException;
import com.kakaoapps.tamutamu.CommonApplication;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.manager.ActivityManager;
import com.kakaoapps.tamutamu.manager.AppManager;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.model.User;
import com.kakaoapps.tamutamu.utils.DebugUtils;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by John on 2016-01-29.
 */
public class LoginActivity extends CommonActivity implements View.OnClickListener{
    private final String TAG = getClass().getSimpleName();

    // added by Adonis 2017.07.14
    private String userName;
    private String userId;
    private String profileUrl;

    private SessionCallback mKakaocallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ActivityManager.actList.add(this);

        init();
    }

    private void init(){
        setTitlebar();
        titlebar.logoIv.setVisibility(View.VISIBLE);

        LinearLayout kakaoLl = (LinearLayout)findViewById(R.id.kakao_ll);
        LinearLayout emailLl = (LinearLayout)findViewById(R.id.email_ll);
        kakaoLl.setOnClickListener(this);
        emailLl.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.kakao_ll:
                kakaoLogin();
                break;
            case R.id.email_ll:
                startActivity(new Intent(this, EmailLoginActivity.class));
                break;
        }
    }

    private void kakaoLogin(){

        UserProfile profile = UserProfile.loadFromCache();
        DebugUtils.setLog(TAG, "profile : " + profile);
        if(profile != null && profile.getId() > 0){
            String id = String.valueOf(profile.getId());
            userName = profile.getNickname();
            profileUrl = profile.getProfileImagePath();
            login(id);
        }else{
            // 카카오 세션을 오픈한다
            mKakaocallback = new SessionCallback();
            com.kakao.auth.Session.getCurrentSession().addCallback(mKakaocallback);
            com.kakao.auth.Session.getCurrentSession().checkAndImplicitOpen();
            com.kakao.auth.Session.getCurrentSession().open(AuthType.KAKAO_LOGIN_ALL, LoginActivity.this);
        }
    }

    private class SessionCallback implements ISessionCallback {
        @Override
        public void onSessionOpened() {
            Log.d("TAG" , "세션 오픈됨");
            // 사용자 정보를 가져옴, 회원가입 미가입시 자동가입 시킴
            KakaorequestMe();
        }

        @Override
        public void onSessionOpenFailed(KakaoException exception) {
            if(exception != null) {
                Log.d("TAG" , exception.getMessage());
            }
        }
    }
    /**
     * 사용자의 상태를 알아 보기 위해 me API 호출을 한다.
     */
    protected void KakaorequestMe() {
        UserManagement.requestMe(new MeResponseCallback() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                int ErrorCode = errorResult.getErrorCode();
                int ClientErrorCode = -777;

                if (ErrorCode == ClientErrorCode) {
                    Toast.makeText(getApplicationContext(), "카카오톡 서버의 네트워크가 불안정합니다. 잠시 후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                } else {
                    Log.d("TAG" , "오류로 카카오로그인 실패 ");
                }
            }

            @Override
            public void onSessionClosed(ErrorResult errorResult) {
                Log.d("TAG" , "오류로 카카오로그인 실패 ");
            }

            @Override
            public void onSuccess(UserProfile userProfile) {

                userId = String.valueOf(userProfile.getId());
                userName = userProfile.getNickname();

                if ( userName == null ){
                    userName = "닉넴을 넣어주세요";
                }

                profileUrl = userProfile.getProfileImagePath();
/*                if ( userId == null || userName == null){
                    setToast("카카오계정정보를 불러오지 못했습니다.");
                    return;
                }*/
                login(userId);
            }

            @Override
            public void onNotSignedUp() {
                // 자동가입이 아닐경우 동의창
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Session.getCurrentSession().handleActivityResult(requestCode, resultCode, data)) {
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void login(final String kakaoId){

        if(!networkCheck())return;

        DialogView.progressDialogShow(this);

        RequestParams params = new RequestParams();
        params.put("user_type", 2);
        params.put("user_id", kakaoId);
        params.put("device_type", 1);
        params.put("device_f_id", AppManager.getDeviceSerialNumber());
        params.put("device_push_token", pm.getPushToken());
        params.put("user_nickname", userName);//added by Adonis 2017.07.14
        params.put("user_profileimg", profileUrl);//added by Adonis 2017.07.14
        params.put("user_overview", "고객님의 소개글을 넣으세요.");//added by Adonis 2017.07.07

        new HttpsManager(this).login(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DebugUtils.setLog(TAG, "login Success");
                DialogView.progressDialogClose();
                pm.setIsLogin(true);
                pm.setUser((User)result);

                getAlarm();
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                if("회원정보가 없습니다.".equals(errorStr)){
                    Intent intent = new Intent(LoginActivity.this, SignupProfileActivity.class);
                    User user = new User();
                    user.setUserId(kakaoId);
                    user.setUserType(2);
                    intent.putExtra("user", user);
                    intent.putExtra("fromKakao", true);
                    startActivity(intent);
                }else setToast(errorStr);
            }
        });
    }

    private void getAlarm(){
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        new HttpsManager(this).getAlarm(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DebugUtils.setLog(TAG, "getAlarm Success");
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                ActivityManager.allActivityFinish();
            }

            @Override
            public void onFailure(String errorStr) {

            }
        });
    }
}
