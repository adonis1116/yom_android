package com.kakaoapps.tamutamu.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.adapter.CloseTagAdapter;
import com.kakaoapps.tamutamu.dialog.CategoryDialog;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.dialog.SecondCategoryDialog;
import com.kakaoapps.tamutamu.manager.DataBaseManager;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.model.Need;
import com.kakaoapps.tamutamu.model.Tag;
import com.kakaoapps.tamutamu.widget.HorizontalListView;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by john on 2016-02-11.
 */
public class NeedWriteActivity extends CommonActivity implements View.OnClickListener{
    private final String TAG = getClass().getSimpleName();
    private final int SELECT_LOC_REQUEST_CODE = 111;
    private CloseTagAdapter tagAdapter;
    private ArrayList<Tag> tagList;


    private EditText subjectEt, priceEt, dateEt, contentEt;
    private TextView locTv;

    private int areaCode, subAreaCode;
    private String latStr, lngStr;
    private Need mItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_need_write);
        mItem = getIntent().getParcelableExtra("need");
        init();
    }

    private void init() {

        setTitlebar();
        titlebar.backIv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText("해주세욤 글쓰기");
        titlebar.rightTv.setVisibility(View.VISIBLE);
        titlebar.rightTv.setText(mItem == null ? "완료" : "수정");
        titlebar.rightTv.setOnClickListener(this);


        subjectEt = (EditText)findViewById(R.id.subject_et);
        priceEt = (EditText)findViewById(R.id.price_et);
        dateEt = (EditText)findViewById(R.id.date_et);
        contentEt = (EditText)findViewById(R.id.content_et);

        Button cateBt = (Button)findViewById(R.id.cate_bt);
        cateBt.setOnClickListener(this);

        Button locBt = (Button) findViewById(R.id.loc_bt);
        locBt.setOnClickListener(this);
        locTv = (TextView)findViewById(R.id.loc_tv);
        tagList = new ArrayList<Tag>();

        if(mItem != null){
            subjectEt.setText(mItem.getSubject());
            priceEt.setText("" + mItem.getPrice());
            dateEt.setText(mItem.getTerm());
            contentEt.setText(mItem.getContent());

            tagList = mItem.getTagList();

            DataBaseManager db = new DataBaseManager(this);
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("id", mItem.getArea());
            String areaStr = db.getArea(map).getName();
            map.put("id", mItem.getSubarea());
            String subAreaStr = db.getSubArea(map).getName();
            locTv.setText(areaStr + ">" + subAreaStr);


            areaCode = mItem.getArea();
            subAreaCode = mItem.getSubarea();
        }

        HorizontalListView tagHlv = (HorizontalListView) findViewById(R.id.tag_hlv);

        tagAdapter = new CloseTagAdapter(this, tagList);
        tagHlv.setAdapter(tagAdapter);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.right_tv:
                check();
                break;
            case R.id.cate_bt:
                if(tagAdapter.getCount() < 5){
                    new CategoryDialog(this, new CategoryDialog.CategoryListner() {
                        @Override
                        public void selectCategory(final int category) {
                            new SecondCategoryDialog(NeedWriteActivity.this, category, new SecondCategoryDialog.SecondCategoryListner() {
                                @Override
                                public void onSelectCategory(int position, String cateStr) {
                                    Tag tag = new Tag();
                                    tag.cate = category;
                                    tag.tag = cateStr;
                                    tagList.add(tag);
                                    tagAdapter.notifyDataSetChanged();
                                }
                            }).show();
                        }
                    }).show();

                }
                else setToast("최대 5개까지만 등록됩니다.");
                break;
            case R.id.loc_bt:
                startActivityForResult(new Intent(this, SelectLocationActivity.class), SELECT_LOC_REQUEST_CODE);
                break;
        }
    }

    private void check(){
        if(subjectEt.getText().toString().trim().length() == 0){
            setToast("제목을 입력하세요.");
            return;
        }

        if(priceEt.getText().toString().trim().length() == 0){
            setToast("금액을 입력하세요.");
            return;
        }

        if(priceEt.getText().toString().trim().length() > 15){
            setToast("금액은 15자리수를 넘을수 없습니다.");
            return;
        }

        if(dateEt.getText().toString().trim().length() == 0){
            setToast("희망기간을 입력하세요.");
            return;
        }

        if(contentEt.getText().toString().trim().length() == 0){
            setToast("내용을 입력하세요.");
            return;
        }

        if(tagList.size() == 0){
            setToast("카테고리를 선택해주세요");
            return;
        }

        if(locTv.getText().toString().length() == 0){
            setToast("지역을 선택해주세요");
            return;
        }

        writeNeed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == SELECT_LOC_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                areaCode = data.getIntExtra("area", 0);
                subAreaCode = data.getIntExtra("subarea", 0);
                latStr = data.getStringExtra("lat");
                lngStr = data.getStringExtra("lng");
                locTv.setText(data.getStringExtra("str"));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void writeNeed(){
        if(!networkCheck())return;

        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        params.put("subject", subjectEt.getText().toString());
        params.put("content", contentEt.getText().toString());
        params.put("price", priceEt.getText().toString());
        params.put("hope_date", dateEt.getText().toString());
        params.put("area_code", areaCode);
        params.put("subarea_code", subAreaCode);
        params.put("lat", latStr);
        params.put("lng", lngStr);


        if(mItem != null){
            params.put("act", "modify");
            params.put("idx", mItem.getId());
        }
        for(int i=0; i<tagList.size(); i++){
            params.put("cate" + (i+1), tagList.get(i).cate);
            params.put("tag" + (i+1), tagList.get(i).tag);
        }




        new HttpsManager(this).writeNeed(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                setToast("등록되었습니다.");
                if(mItem != null){
                    Intent intent = new Intent();
                    mItem.setSubject(subjectEt.getText().toString());
                    mItem.setPrice(priceEt.getText().toString());
                    mItem.setTerm(dateEt.getText().toString());
                    mItem.setContent(contentEt.getText().toString());
                    mItem.setTagList(tagList);
                    mItem.setArea(areaCode);
                    mItem.setSubarea(subAreaCode);
                    intent.putExtra("need", mItem);
                    setResult(RESULT_OK, intent);
                }

                finish();
            }

            @Override
            public void onFailure(String errorStr) {
                setToast(errorStr);
            }
        });

    }
}
