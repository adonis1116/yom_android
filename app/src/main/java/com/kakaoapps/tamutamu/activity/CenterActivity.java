package com.kakaoapps.tamutamu.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.adapter.NoticeAdapter;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.model.Notice;
import com.kakaoapps.tamutamu.widget.FloatingActionButton;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;

/**
 * Created by developer on 2016-06-30.
 */
public class CenterActivity extends CommonActivity implements View.OnClickListener{

    private int page = 1;
    private int ROW_LIMIT = 10;

    private int qnaPage = 1;// added by Adonis 2017.07.06
    private int QNA_ROW_LIMIT = 10;// added by Adonis 2017.07.06
    private ExpandableListView elv, elv_faq; // modified by Adonis 2017.07.06
    private LinearLayout ll;
    private TextView canTv, needTv;

    private ArrayList<Notice> list = new ArrayList<Notice>();
    private ArrayList<Notice> qnaList = new ArrayList<Notice>(); // added by Adonis 2017.07.06
    private NoticeAdapter adapter;

    private EditText subjectEt, contentEt;
    private FloatingActionButton writeRegist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_center);

        init();
        getFaqList();
        getQnaList();
    }

    private void init(){
        setTitlebar();
        titlebar.backIv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText("고객센터");
        titlebar.centerTv.setVisibility(View.VISIBLE);
        titlebar.rightTv.setText("완료");
        titlebar.rightTv.setOnClickListener(this);

        canTv = (TextView)findViewById(R.id.can_tv);
        needTv = (TextView)findViewById(R.id.need_tv);
        canTv.setText("자주하는 질문");
        needTv.setText("건의하기");
        canTv.setOnClickListener(this);
        needTv.setOnClickListener(this);

        //added by Adonis 2017.07.06
        writeRegist = (FloatingActionButton)findViewById(R.id.fab);
        writeRegist.setOnClickListener(this);

        //


        elv_faq = (ExpandableListView)findViewById(R.id.elv_faq);

        elv_faq.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (list.size() > 0 && elv_faq.getLastVisiblePosition() >= totalItemCount - 1 && page * ROW_LIMIT < list.get(0).getTotal()) {
                    if(DialogView.mDialog == null){
                        ++page;
                        getFaqList();
                    }
                }
            }
        });

        // added by Adonis 2017.07.06
        elv = (ExpandableListView)findViewById(R.id.elv);
        elv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (qnaList.size() > 0 && elv.getLastVisiblePosition() >= totalItemCount - 1 && qnaPage * QNA_ROW_LIMIT < qnaList.get(0).getTotal()) {
                    if(DialogView.mDialog == null){
                        ++qnaPage;
                        getQnaList();
                    }
                }
            }
        });
        ////////////////////////////

        ll = (LinearLayout)findViewById(R.id.ll);
        subjectEt = (EditText)findViewById(R.id.subject_et);
        contentEt = (EditText)findViewById(R.id.content_et);
    }

    private void setTab(int resId){

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(contentEt.getWindowToken(), 0);

        if(R.id.can_tv == resId){
            canTv.setTextColor(getResources().getColor(R.color.purple));
            canTv.setBackgroundColor(Color.WHITE);
            needTv.setTextColor(getResources().getColor(R.color.gray));
            needTv.setBackgroundResource(R.drawable.gradient_round);
            elv_faq.setVisibility(View.VISIBLE);
            elv.setVisibility(View.GONE);
            ll.setVisibility(View.GONE);
            titlebar.rightTv.setVisibility(View.GONE);
            writeRegist.setVisibility(View.GONE);
        }else{
            canTv.setTextColor(getResources().getColor(R.color.gray));
            canTv.setBackgroundResource(R.drawable.gradient_round);
            needTv.setTextColor(getResources().getColor(R.color.purple));
            needTv.setBackgroundColor(Color.WHITE);
            elv_faq.setVisibility(View.GONE);
            elv.setVisibility(View.VISIBLE);
            writeRegist.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.can_tv:
                setTab(R.id.can_tv);
                break;
            case R.id.need_tv:
                setTab(R.id.need_tv);
                break;
            case R.id.right_tv:
                registFaq();
                break;
            //added by Adonis 2017.07.06
            case R.id.fab:
                ll.setVisibility(View.VISIBLE);
                titlebar.rightTv.setVisibility(View.VISIBLE);
                writeRegist.setVisibility(View.GONE);
                break;
        }
    }

    private void getFaqList(){
        if(!networkCheck())return;

        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("start", page);
        params.put("pgNum", ROW_LIMIT);
        new HttpsManager(this).getFaqList(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                if(page == 1){
                    list = (ArrayList<Notice>)result;
                    adapter = new NoticeAdapter(CenterActivity.this, list);
                    elv_faq.setAdapter(adapter);
                }else{
                    list.addAll((ArrayList<Notice>) result);
                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }

    // added by Adonis 2017.07.06
    private void getQnaList(){
        if(!networkCheck())return;

        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("start", qnaPage);
        params.put("pgnum", QNA_ROW_LIMIT);
        params.put("user_no", pm.getUser().getUserNo());
        new HttpsManager(this).getQnaList(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                if(qnaPage == 1){
                    qnaList = (ArrayList<Notice>)result;
                    adapter = new NoticeAdapter(CenterActivity.this, qnaList);
                    elv.setAdapter(adapter);
                }else{
                    qnaList.addAll((ArrayList<Notice>) result);
                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }
    ///////////////

    private void registFaq(){
        if(subjectEt.getText().toString().trim().length() == 0){
            setToast("제목을 입력하세요");
            subjectEt.requestFocus();
            return;
        }

        if(contentEt.getText().toString().trim().length() == 0){
            setToast("내용을 입력하세요");
            contentEt.requestFocus();
            return;
        }


        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        params.put("subject", subjectEt.getText().toString().trim());
        params.put("content", contentEt.getText().toString().trim());
        new HttpsManager(this).registFaq(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                subjectEt.setText("");
                contentEt.setText("");
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(contentEt.getWindowToken(), 0);
                setToast("등록되었습니다.");
                getQnaList();
                ll.setVisibility(View.GONE);
                titlebar.rightTv.setVisibility(View.GONE);
                elv_faq.setVisibility(View.GONE);
                elv.setVisibility(View.VISIBLE);
                writeRegist.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(String errorStr) {
                setToast(errorStr);
            }
        });
    }
}
