package com.kakaoapps.tamutamu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kakaoapps.tamutamu.Const;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.UrlInfo;
import com.kakaoapps.tamutamu.adapter.HeaderImageAdapter;
import com.kakaoapps.tamutamu.adapter.ReviewAdapter;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.dialog.ReportDialog;
import com.kakaoapps.tamutamu.manager.DataBaseManager;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.model.Area;
import com.kakaoapps.tamutamu.model.Review;
import com.kakaoapps.tamutamu.model.SubArea;
import com.kakaoapps.tamutamu.model.User;
import com.kakaoapps.tamutamu.utils.DebugUtils;
import com.kakaoapps.tamutamu.widget.FloatingActionButton;
import com.loopj.android.http.RequestParams;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

import static java.security.AccessController.getContext;

/**
 * Created by john on 2016-02-11.
 */
public class OtherPageActivity extends CommonActivity implements View.OnClickListener{
    private final String TAG =getClass().getSimpleName();
    private ListView lv;
    private User mUser;
    private ArrayList<Review> reviewList = new ArrayList<Review>();
    private ArrayList<String> portfolio = new ArrayList<String>();
    private ReviewAdapter adapter;
    private HeaderImageAdapter headerImageAdapter = null;
    private boolean isFlag;

    private int page = 1;
    private int ROW_LIMIT = 10;
    private TextView reviewCountTv;
    TextView worknumTv;
    TextView avgprice_tv;
    private ViewPager vp;
    TextView overview_tv;
    private LinearLayout ll_portfolio;
    Button question_bt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otherpage);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getOtherList();
    }

    private void getOtherList(){
        if(!networkCheck())return;
        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        int idx = getIntent().getIntExtra("write_id", 0);
        if(idx > 0) params.put("idx", idx);
        else params.put("idx2", getIntent().getStringExtra("receive_id"));
        params.put("user_no", pm.getUser().getUserId());
        params.put("start", page);
        params.put("pgNum", ROW_LIMIT);
        new HttpsManager(this).getOtherProfile(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                HashMap<String, Object> map = (HashMap<String, Object>) result;
                mUser = (User) map.get("user");
                DebugUtils.setLog(TAG, "user : " + mUser);

                // added by Adonis 2017.07.05
                portfolio.addAll((ArrayList<String>) map.get("portfolio"));
                for(int i = 0; i < portfolio.size(); i++){
                    int j = i;
                    if ( portfolio.get(i).equals("")){
                        portfolio.remove(i);
                        i = j - 1;
                    }
                }

                if (!isFlag) {
                    init();
                    isFlag = true;
                }

                if(page == 1){
                    reviewList = (ArrayList<Review>) map.get("reviews");
                    if(reviewList.size() > 0) reviewCountTv.setText(reviewList.get(0).getTotal() + "개 등록");
                    adapter = new ReviewAdapter(OtherPageActivity.this, reviewList);
                    lv.setAdapter(adapter);
                }else{
                    reviewList.addAll((ArrayList<Review>) map.get("reviews"));
                    adapter.notifyDataSetChanged();
                }



                if(portfolio.isEmpty())
                    ll_portfolio.setVisibility(View.GONE);
                else
                    ll_portfolio.setVisibility(View.VISIBLE);

                headerImageAdapter.notifyDataSetChanged();

                overview_tv.setText(mUser.getOverview());
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
                finish();
            }
        });
    }

    private void init(){
        setTitlebar();
        titlebar.backIv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText(mUser.getUserNick());
        titlebar.reportIv.setVisibility(View.VISIBLE);
        titlebar.reportIv.setOnClickListener(this);

        lv = (ListView)findViewById(R.id.lv);
        lv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (reviewList.size() > 0 && lv.getLastVisiblePosition() >= totalItemCount - 1 && page * ROW_LIMIT < reviewList.get(0).getTotal()) {
                    if(DialogView.mDialog == null){
                        ++page;
                        getOtherList();
                    }
                }
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);

        setHeader();
    }

    private void setHeader() {

        View headerView = View.inflate(this, R.layout.header_otherpage, null);
        lv.removeHeaderView(headerView);
        CircularImageView picIv = (CircularImageView) headerView.findViewById(R.id.pic_iv);
        TextView nickTv = (TextView) headerView.findViewById(R.id.nick_tv);
        TextView kakaoTv = (TextView) headerView.findViewById(R.id.kakao_tv);

        // added by Adonis 2017.07.04
        worknumTv = (TextView) headerView.findViewById(R.id.work_num_tv);
        avgprice_tv = (TextView) headerView.findViewById(R.id.avg_price_tv);
        //

        vp = (ViewPager) headerView.findViewById(R.id.portfolio_vp); // added by Adonis 2017.07.05
        headerImageAdapter = new HeaderImageAdapter(OtherPageActivity.this, portfolio);
        vp.setAdapter(headerImageAdapter);

        overview_tv = (TextView) headerView.findViewById(R.id.overview_tv);

        ll_portfolio = (LinearLayout) headerView.findViewById(R.id.ll_portfolio);

        question_bt = (Button) headerView.findViewById(R.id.question_bt);
        question_bt.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent intent = new Intent(OtherPageActivity.this, QuestionActivity.class);
                        intent.putExtra("partner_no", mUser.getUserNo());
                        intent.putExtra("partner_id", mUser.getUserId());
                        startActivity(intent);
                    }
                }
        );


        DebugUtils.setLog(TAG, mUser.getUserNick());

        if(mUser.getUserPic() != null && mUser.getUserPic().trim().length() > 0){
            if (!mUser.getUserPic().contains("http")) {
                mUser.setUserPic(UrlInfo.URL_HEADER + UrlInfo.API_HEADER + "data/profile/" + mUser.getUserPic());
            }
            Glide.with(this).load(mUser.getUserPic())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(picIv);
        }
        nickTv.setText(mUser.getUserNick());
        kakaoTv.setText(mUser.getSnsId());

        int areas [] = {mUser.getArea(), mUser.getArea2(), mUser.getArea3()};
        int subAreas [] = {mUser.getSubArea(), mUser.getSubArea2(), mUser.getSubArea3()};
        DataBaseManager db = new DataBaseManager(this);
        for(int i=0; i< Const.LOCAITON_LIMIT; i++){
            if(areas[i] > 0){
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("id", areas[i]);
                DebugUtils.setLog(TAG, "area : " +  areas[i]);
                Area area = db.getArea(map);
                map.put("id", subAreas[i]);
                DebugUtils.setLog(TAG, "subarea : " + subAreas[i]);
                SubArea subArea = db.getSubArea(map);
                String str = area.getName() + ">" + subArea.getName();

                int resId = getResources().getIdentifier("area_tv" + (i+1), "id", getPackageName());
                TextView areaTv = (TextView)headerView.findViewById(resId);
                areaTv.setVisibility(View.VISIBLE);
                areaTv.setText(str);
            }
        }

        reviewCountTv = (TextView)headerView.findViewById(R.id.review_count_tv);

        //added by Adonis 2017.07.04

        RequestParams params = new RequestParams();
        params.put("user_no", mUser.getUserNo());
        params.put("user_id", mUser.getUserId());

        new HttpsManager(this).getUserWorkInfo(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {

                double[] proList = (double[])result;

                worknumTv.setText(NumberFormat.getNumberInstance(Locale.US).format(proList[0]) + "건");
                avgprice_tv.setText(NumberFormat.getNumberInstance(Locale.US).format(proList[1]) + "원");// 원->포인트 2017.07.05
            }

            @Override
            public void onFailure(String errorStr) {
//                Toast.makeText(this, errorStr, Toast.LENGTH_SHORT).show();
            }
        });

        lv.addHeaderView(headerView);

    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()){
            case R.id.fab:
                intent = new Intent(this, ReviewWriteActivity.class);
                intent.putExtra("user_id", mUser.getUserNo());
                break;
            case R.id.report_iv:
                new ReportDialog(this, String.format(getString(R.string.because_report), "'" + mUser.getUserNick() + "'"), new ReportDialog.ChoiceListner() {
                    @Override
                    public void confirm(String content) {
                        report(content);
                    }

                    @Override
                    public void cancle() {

                    }
                }).show();
                break;
        }

        if(intent != null)startActivity(intent);
    }


    private void report(String content){
        if(content.trim().length() == 0){
            setToast("신고사유를 적어주세요");
            return;
        }

        if(!networkCheck())return;

        DialogView.progressDialogShow(this);

        RequestParams params = new RequestParams();
        params.put("user_no", mUser.getUserNo());
        params.put("comment", content);

        new HttpsManager(this).report(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                setToast("신고되었습니다");
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });


    }
}
