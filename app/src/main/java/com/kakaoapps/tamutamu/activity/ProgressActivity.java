package com.kakaoapps.tamutamu.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.adapter.CanProgressPagerAdapter;
import com.kakaoapps.tamutamu.adapter.NeedProgressPagerAdapter;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.model.Process;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;

/**
 * Created by john on 2016-03-03.
 */
public class ProgressActivity extends CommonActivity implements View.OnClickListener{
    private final String TAG = getClass().getSimpleName();
    public static final String ACTION_RECEIVE_PUSH = "ACTION.receiver_push"; // added by RGJ 2017.02.03

    private CanProgressPagerAdapter canAdapter;
    private NeedProgressPagerAdapter needAdapter;
    private TextView canTv, needTv;
    private ViewPager vp;
    private PagerSlidingTabStrip tabs;
    private int process_type=0;
    private int req_type = 0;

    private boolean isCan = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress);

        // added by RGJ 2017.02.03
        IntentFilter filter = new IntentFilter(ProgressActivity.ACTION_RECEIVE_PUSH);
        LocalBroadcastManager.getInstance(this).registerReceiver( mHandleMessageDelivery, filter);
        // added end

        process_type = getIntent().getIntExtra("type", 0);
        req_type = getIntent().getIntExtra("req_type", 0);

        init();

        // added by RGJ 2017.02.03
        if(req_type == 0){
            isCan = false;
            setTab(R.id.can_tv);}
        else {
            isCan = true;
            setTab(R.id.need_tv);
        }

    }


    private void init(){
        setTitlebar();
        titlebar.backIv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText("진행상황");

        canTv = (TextView)findViewById(R.id.can_tv);
        needTv = (TextView)findViewById(R.id.need_tv);

        canTv.setOnClickListener(this);
        needTv.setOnClickListener(this);

        vp = (ViewPager)findViewById(R.id.vp);

        tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setIndicatorColorResource(R.color.purple);

        getCanList();

    }

    private void getCanList(){
        if(!networkCheck())return;

        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());

        new HttpsManager(this).canMyRegistList(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                canAdapter = new CanProgressPagerAdapter(ProgressActivity.this, (ArrayList<Process>)result);
                vp.setAdapter(canAdapter);
                tabs.setViewPager(vp);
                vp.setCurrentItem(process_type); //RGJ
                //process_type = 0; //RGJ
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }

    private void getNeedList(){
        if(!networkCheck())return;

        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());

        new HttpsManager(this).needMyRegistList(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                needAdapter = new NeedProgressPagerAdapter(ProgressActivity.this, (ArrayList<Process>) result);
                vp.setAdapter(needAdapter);
                tabs.setViewPager(vp);
                vp.setCurrentItem(process_type); //RGJ
                //process_type = 0; //RGJ
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.can_tv:
                if(!isCan) process_type = 0; // added by RGJ 2017.02.03
                setTab(R.id.can_tv);
                break;
            case R.id.need_tv:
                if(isCan) process_type = 0; // added by RGJ 2017.02.03
                setTab(R.id.need_tv);
                break;
        }
    }

    private void setTab(int resId){

        if(R.id.can_tv == resId){
            if(isCan){
                return;
            }
            canTv.setTextColor(getResources().getColor(R.color.purple));
            canTv.setBackgroundColor(Color.WHITE);
            needTv.setTextColor(getResources().getColor(R.color.gray));
            needTv.setBackgroundResource(R.drawable.gradient_round);
            isCan = true;
            getCanList();
        }else{
            if(!isCan){
                return;
            }
            canTv.setTextColor(getResources().getColor(R.color.gray));
            canTv.setBackgroundResource(R.drawable.gradient_round);
            needTv.setTextColor(getResources().getColor(R.color.purple));
            needTv.setBackgroundColor(Color.WHITE);
            isCan = false;
            getNeedList();
        }

    }

    // added by RGJ 2017.02.03
    private final BroadcastReceiver mHandleMessageDelivery = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("TAG", "N Push");

            process_type = intent.getIntExtra("type", 0);
            req_type = intent.getIntExtra("req_type", 0);
            if(req_type == 0) {
                isCan = false;
                setTab(R.id.can_tv);
                //getCanList();
            }else {
                isCan = true;
                setTab(R.id.need_tv);
               // getNeedList();
            }
            /*
            if(intent.hasExtra("canAction")) {
                getCanList();*/
        }
    };
}

