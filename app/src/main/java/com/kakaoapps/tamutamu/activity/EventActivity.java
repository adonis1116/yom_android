package com.kakaoapps.tamutamu.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.manager.DataBaseManager;
import com.kakaoapps.tamutamu.model.Event;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by john on 2016-02-04.
 */
public class EventActivity extends CommonActivity implements View.OnClickListener{

    private final String TAG = getClass().getSimpleName();

    private  Event event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        event = getIntent().getParcelableExtra("event");
        ImageView iv = (ImageView)findViewById(R.id.iv);
        iv.setOnClickListener(this);

        if(event.getImage() != null) Glide.with(this)
                .load(event.getImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(iv);

        footer();
    }

    private void footer(){
        TextView tv1 = (TextView)findViewById(R.id.tv1);
        TextView tv2 = (TextView)findViewById(R.id.tv2);

        tv1.setText("오늘하루 보지않기");
        tv2.setText("닫기");
        tv1.setOnClickListener(this);
        tv2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv1:
                DataBaseManager db = new DataBaseManager(this);

                SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                String today = format.format(new Date());
                Event value = event;
                value.setCloseDate(today);
                db.updateEvent(event);
                finish();
                break;
            case R.id.tv2:
                finish();
                break;
            case R.id.iv:
                Uri uri = Uri.parse(event.getLink());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;
        }
    }
}
