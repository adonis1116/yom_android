package com.kakaoapps.tamutamu.activity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.LogoutResponseCallback;
import com.kakao.usermgmt.callback.UnLinkResponseCallback;
import com.kakao.util.helper.log.Logger;
import com.kakaoapps.tamutamu.Const;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.UrlInfo;
import com.kakaoapps.tamutamu.adapter.CanAdapter;
import com.kakaoapps.tamutamu.adapter.MypageCanAdapter;
import com.kakaoapps.tamutamu.adapter.MypageNeedAdapter;
import com.kakaoapps.tamutamu.adapter.NeedAdapter;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.dialog.SetPushDialog;
import com.kakaoapps.tamutamu.dialog.TwoButtonDialog;
import com.kakaoapps.tamutamu.manager.ActivityManager;
import com.kakaoapps.tamutamu.manager.DataBaseManager;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.model.Area;
import com.kakaoapps.tamutamu.model.Can;
import com.kakaoapps.tamutamu.model.Need;
import com.kakaoapps.tamutamu.model.SubArea;
import com.kakaoapps.tamutamu.model.User;
import com.kakaoapps.tamutamu.utils.DebugUtils;
import com.loopj.android.http.RequestParams;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by john on 2016-02-11.
 */
public class MypageActivity extends CommonActivity implements View.OnClickListener{
    private final String TAG = getClass().getSimpleName();
    private final int MODIFY_PROFILE = 112;
    private ListView lv;
    private CircularImageView picIv;
    private User mUser;
    private TextView nickTv, phoneTv, snsTv, overviewTv;
    private View headerView;

    private MypageCanAdapter canAdapter;
    private MypageNeedAdapter needAdapter;

    private ArrayList<Can> canList = new ArrayList<Can>();
    private ArrayList<Need> needList = new ArrayList<Need>();

    private int page = 1;
    private int ROW_LIMIT = 10;

    private boolean isCan = true;

    private Button canBt, needBt;
    private boolean isTabClick = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypage);
        mUser = pm.getUser();
        init();
    }

    private void init(){
        setTitlebar();
        titlebar.backIv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText("마이페이지");
        titlebar.centerTv.setVisibility(View.VISIBLE);


        lv = (ListView)findViewById(R.id.lv);

        lv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int size = 0;
                boolean isLast = false;
                if(isCan){
                    size = canList.size();
                    if(size > 0)isLast = page * ROW_LIMIT < canList.get(0).getTotal();
                }else{
                    size = needList.size();
                    if(size > 0)isLast = page * ROW_LIMIT < needList.get(0).getTotal();
                }

                if (size > 0 && lv.getLastVisiblePosition() >= totalItemCount - 1 && isLast) {
                    if(DialogView.mDialog == null){
                        ++page;
                        if(isCan)getCanList();
                        else getNeedList();
                    }
                }
            }
        });

        if(headerView == null)setHeader();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(isCan)getCanList();
        else getNeedList();

        getMyPoint();

    }

    private void getMyPoint(){
        RequestParams params = new RequestParams();
        params.put("user_no", mUser.getUserNo());
        new HttpsManager(this).getMyPoint(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {

            }

            @Override
            public void onFailure(String errorStr) {

            }
        });
    }

    private void getCanList(){

        if(!networkCheck()) return;

        RequestParams params = new RequestParams();
        params.put("orderby", "regdt");
        params.put("desc", "desc");
        params.put("user_no", pm.getUser().getUserNo());
        params.put("start", page);
        params.put("pgNum", ROW_LIMIT);
        params.put("ismy", 1);
        DialogView.progressDialogShow(this);

        new HttpsManager(this).getCanList(params, new HttpsManager.HttpListner() {

            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();

                HashMap<String, Object> map = (HashMap<String, Object>) result;

                if (page == 1) {
                    canList = (ArrayList<Can>) map.get("list");
                    canAdapter = new MypageCanAdapter(MypageActivity.this, canList);
                    lv.setAdapter(canAdapter);
                    if(isTabClick) {
                        lv.setSmoothScrollbarEnabled(true);
//                        lv.setSelection(canList.size());
                    }
                } else {
                    canList.addAll((ArrayList<Can>) map.get("list"));
                    canAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }


    private void getNeedList(){
        if(!networkCheck()) return;

        RequestParams params = new RequestParams();
        params.put("orderby", "regdt");
        params.put("desc", "desc");
        params.put("user_no", pm.getUser().getUserNo());
        params.put("start", page);
        params.put("pgNum", ROW_LIMIT);
        params.put("ismy", 1);
        DialogView.progressDialogShow(this);

        new HttpsManager(this).getNeedList(params, new HttpsManager.HttpListner() {

            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();

                HashMap<String, Object> map = (HashMap<String, Object>) result;

                if (page == 1) {
                    needList = (ArrayList<Need>) map.get("list");
                    needAdapter = new MypageNeedAdapter(MypageActivity.this, (ArrayList<Need>) needList);
                    lv.setAdapter(needAdapter);
                    if(isTabClick) {
                        lv.setSmoothScrollbarEnabled(true);
 //                       lv.setSelection(needList.size());
                    }
                } else {
                    needList.addAll((ArrayList<Need>) map.get("list"));
                    needAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }

    // set header including user profile
    private void setHeader(){
        headerView = View.inflate(this, R.layout.header_mypage, null);

        picIv = (CircularImageView)headerView.findViewById(R.id.pic_iv);
        picIv.setOnClickListener(this);


        nickTv = (TextView)headerView.findViewById(R.id.nick_tv);
        phoneTv = (TextView)headerView.findViewById(R.id.phone_tv);
        snsTv = (TextView)headerView.findViewById(R.id.sns_tv);
        overviewTv = (TextView)headerView.findViewById(R.id.overview_tv);//added by Adonis 2017.07.07
        setProfile();

        DebugUtils.setLog(TAG, mUser.getUserPic());

        for (int i=0; i<14; i++){ //changed by RGJ 2017.01.13
            int resId = getResources().getIdentifier("bt"+i, "id", getPackageName());
            Button bt = (Button)headerView.findViewById(resId);
            bt.setOnClickListener(this);
        }

        canBt = (Button)headerView.findViewById(R.id.can_bt);
        canBt.setOnClickListener(this);
        needBt = (Button) headerView.findViewById(R.id.need_bt);
        needBt.setOnClickListener(this);
        lv.addHeaderView(headerView);
    }

    private void setProfile(){
        DebugUtils.setLog(TAG, "nick : " + mUser.getUserNick());

        nickTv.setText(mUser.getUserNick());
        phoneTv.setText(mUser.getUserPhone());
        snsTv.setText(mUser.getSnsId());
        overviewTv.setText(mUser.getOverview());//added by Adonis 2017.07.07
        if(mUser.getUserPic() != null && mUser.getUserPic().trim().length() > 0){
            if (!mUser.getUserPic().contains("http")) {
                mUser.setUserPic(UrlInfo.URL_HEADER + UrlInfo.API_HEADER + "data/profile/" + mUser.getUserPic());
            }
            Glide.with(this).load(mUser.getUserPic())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(picIv);
        }

        int areas [] = {mUser.getArea(), mUser.getArea2(), mUser.getArea3()};
        int subAreas [] = {mUser.getSubArea(), mUser.getSubArea2(), mUser.getSubArea3()};
        DataBaseManager db = new DataBaseManager(this);
        for(int i=0; i< Const.LOCAITON_LIMIT; i++){
            if(areas[i] > 0){
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("id", areas[i]);
                DebugUtils.setLog(TAG, "area : " +  areas[i]);
                Area area = db.getArea(map);
                map.put("id", subAreas[i]);
                DebugUtils.setLog(TAG, "subarea : " + subAreas[i]);
                SubArea subArea = db.getSubArea(map);
                String str = area.getName() + ">" + subArea.getName();

                int resId = getResources().getIdentifier("area_tv" + (i+1), "id", getPackageName());
                TextView areaTv = (TextView)headerView.findViewById(resId);
                areaTv.setVisibility(View.VISIBLE);
                areaTv.setText(str);

            }
        }
    }

    private void logout(){
        pm.setIsLogin(false);
        pm.setIsMemoPush(false);
        pm.setIsPush(false);
        if(pm.getKakaoId() != null){
            UserManagement.requestLogout(new LogoutResponseCallback() {
                @Override
                public void onCompleteLogout() {
                    DebugUtils.setLog(TAG, "onCompleteLogout called!!!");
                }
            });
        }
        ActivityManager.allActivityFinish();
        //setResult(RESULT_OK);

        finish();
        startActivity(new Intent(this, LoginActivity.class));
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()){
            case R.id.bt0:
                new SetPushDialog(this).show();
                break;
            case R.id.bt1:
                intent = new Intent(this, NoticeActivity.class);
                break;
            case R.id.bt2:
                intent = new Intent(this, PointManagerActivity.class);
                break;
            case R.id.bt3:
                intent = new Intent(this, ProgressActivity.class);
                break;
            case R.id.bt4:
                intent = new Intent(this, LikeListActivity.class);
                break;
            case R.id.bt5:
                intent = new Intent(this, MemoBoxActivity.class);
                break;
            case R.id.bt6:
                intent = new Intent(this, RankActivity.class);
                break;
            case R.id.bt7:
                intent = new Intent(this, ProvisionActivity.class);
                intent.putExtra("type", 1);
                break;
            case R.id.bt8:
                intent = new Intent(this, UseHowActivity.class);
                break;
            case R.id.bt9:
                intent = new Intent(this, UseGuideActivity.class);
                break;
            case R.id.bt10:
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://tamu.kakaoapps.co.kr/mypage/company_reg.php"));
                break;
            case R.id.bt11:
                intent = new Intent(this, CenterActivity.class);
                break;
            case R.id.bt13: //Added by RGJ 2017.01.13
                intent = new Intent(this, PointExchangeActivity.class);
                break;
            case R.id.bt12:
                new TwoButtonDialog(this, "로그아웃", "로그아웃 하시겠습니까?", "확인", "취소", new TwoButtonDialog.ChoiceListner() {
                    @Override
                    public void confirm() {
                        logout();
                    }

                    @Override
                    public void cancle() {

                    }
                }).show();
                break;
            case R.id.can_bt:
                if(!isCan){
                    canBt.setBackgroundColor(Color.parseColor("#88ffffff"));
                    canBt.setTextColor(getResources().getColor(R.color.purple));
                    needBt.setBackgroundColor(Color.parseColor("#44ffffff"));
                    needBt.setTextColor(Color.parseColor("#616161"));
                    page = 1;
                    isCan = true;
                    isTabClick = true;
                    getCanList();
                }
                break;
            case R.id.need_bt:
                if(isCan){
                    canBt.setBackgroundColor(Color.parseColor("#44ffffff"));
                    canBt.setTextColor(Color.parseColor("#616161"));
                    needBt.setBackgroundColor(Color.parseColor("#88ffffff"));
                    needBt.setTextColor(getResources().getColor(R.color.purple));
                    page = 1;
                    isCan = false;
                    isTabClick = true;
                    getNeedList();
                }

                break;
            case R.id.pic_iv:
                startActivityForResult(new Intent(this, ModifyProfileActivity.class), MODIFY_PROFILE);
                break;

        }

        if(intent != null)startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (RESULT_OK == resultCode){
            if(requestCode == MODIFY_PROFILE){
                mUser = pm.getUser();
                setProfile();
            }
        }
    }
}
