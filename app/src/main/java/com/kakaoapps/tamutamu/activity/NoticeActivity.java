package com.kakaoapps.tamutamu.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ExpandableListView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.adapter.NoticeAdapter;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.model.Notice;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;

/**
 * Created by john on 2016-02-11.
 */
public class NoticeActivity extends CommonActivity{

    private ExpandableListView elv;
    private int page = 1;
    private int ROW_LIMIT = 10;

    private ArrayList<Notice> list = new ArrayList<Notice>();
    private NoticeAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice);
        init();
        getNoticeList();
    }

    private void init(){
        setTitlebar();
        titlebar.backIv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText("공지사항");
        elv = (ExpandableListView)findViewById(R.id.elv);
        elv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (list.size() > 0 && elv.getLastVisiblePosition() >= totalItemCount - 1 && page * ROW_LIMIT < list.get(0).getTotal()) {
                    if(DialogView.mDialog == null){
                        ++page;
                        getNoticeList();
                    }
                }
            }
        });
    }

    private void getNoticeList(){
        if(!networkCheck())return;

        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("start", page);
        params.put("pgNum", ROW_LIMIT);
        new HttpsManager(this).getNoticeList(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                if(page == 1){
                    list = (ArrayList<Notice>)result;
                    adapter = new NoticeAdapter(NoticeActivity.this, list);
                    elv.setAdapter(adapter);
                }else{
                    list.addAll((ArrayList<Notice>) result);
                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }
}
