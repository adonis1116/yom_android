package com.kakaoapps.tamutamu.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.adapter.MallAdapter;
import com.kakaoapps.tamutamu.adapter.PointHistoryAdapter;
import com.kakaoapps.tamutamu.dialog.BackbookDialog;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.dialog.RowTwoButtonDialog;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.model.Mall;
import com.kakaoapps.tamutamu.model.PointHistory;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by developer on 2016-06-29.
 */
public class PointManagerActivity extends CommonActivity implements View.OnClickListener{

    private TextView canTv, needTv;
    private GridView gv;
    private ListView lv;

    private MallAdapter mallAdapter;
    private PointHistoryAdapter adapter;
    private ArrayList<PointHistory> mList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point_manager);
        init();
    }

    private void init(){
        setTitlebar();
        titlebar.backIv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText("포인트 구매");
        titlebar.centerTv.setVisibility(View.VISIBLE);

        canTv = (TextView)findViewById(R.id.can_tv);
        needTv = (TextView)findViewById(R.id.need_tv);
        canTv.setText("포인트 몰");
        needTv.setText("포인트 내역");
        canTv.setOnClickListener(this);
        needTv.setOnClickListener(this);

        gv = (GridView)findViewById(R.id.gv);

        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                new RowTwoButtonDialog(PointManagerActivity.this, "결제하기", "카드결제", "실시간 계좌이체", new RowTwoButtonDialog.ChoiceListner() {
                    @Override
                    public void firstClick() {
                        Intent intent = new Intent(PointManagerActivity.this, WebViewActivity.class);
                        intent.putExtra("idx", mallAdapter.getItem(position).idx);
                        startActivity(intent);
                    }

                    @Override
                    public void secondClick() {
                        //new BackbookDialog(PointManagerActivity.this, mallAdapter.getItem(position).idx, new BackbookDialog.BackbookDialogListener() {
                        //    @Override
                        //    public void gotoATM(int pointId) {
                                Intent intent = new Intent(PointManagerActivity.this, WebViewGalaxia.class);
                                intent.putExtra("idx", mallAdapter.getItem(position).idx);
                                startActivity(intent);
                        //    }
                        //}).show();
                    }
                }).show();
            }
        });

        lv = (ListView)findViewById(R.id.lv);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        getMallList();
    }

    private void setTab(int resId){
        if(R.id.can_tv == resId){
            canTv.setTextColor(getResources().getColor(R.color.purple));
            canTv.setBackgroundColor(Color.WHITE);
            needTv.setTextColor(getResources().getColor(R.color.gray));
            needTv.setBackgroundResource(R.drawable.gradient_round);
            gv.setVisibility(View.VISIBLE);
            lv.setVisibility(View.GONE);
        }else{
            canTv.setTextColor(getResources().getColor(R.color.gray));
            canTv.setBackgroundResource(R.drawable.gradient_round);
            needTv.setTextColor(getResources().getColor(R.color.purple));
            needTv.setBackgroundColor(Color.WHITE);
            gv.setVisibility(View.GONE);
            lv.setVisibility(View.VISIBLE);
            getHistoryList();
        }
    }

    private void getMallList(){
        DialogView.progressDialogShow(this);
        new HttpsManager(this).getPointMall(new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                ArrayList<Mall> mallList = (ArrayList<Mall>)result;
                mallAdapter = new MallAdapter(PointManagerActivity.this, mallList);
                gv.setAdapter(mallAdapter);
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }

    private void getHistoryList(){
        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        params.put("user_id", pm.getUser().getUserId());
        new HttpsManager(this).getPointHistory(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                mList = (ArrayList<PointHistory>)result;
                adapter = new PointHistoryAdapter(PointManagerActivity.this, mList);
                lv.setAdapter(adapter);
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.can_tv:
                setTab(R.id.can_tv);
                break;
            case R.id.need_tv:
                setTab(R.id.need_tv);
                break;
        }
    }
}
