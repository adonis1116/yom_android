package com.kakaoapps.tamutamu.activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.gcm.QuickstartPreferences;
import com.kakaoapps.tamutamu.gcm.RegistrationIntentService;
import com.kakaoapps.tamutamu.manager.AppManager;
import com.kakaoapps.tamutamu.manager.DataBaseManager;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.model.Area;
import com.kakaoapps.tamutamu.model.SubArea;
import com.kakaoapps.tamutamu.model.User;
import com.kakaoapps.tamutamu.utils.DebugUtils;
import com.loopj.android.http.RequestParams;

import java.security.MessageDigest;
import java.util.ArrayList;

/**
 * Created by John on 2016-01-29.
 */
public class SplashActivity extends CommonActivity{

    private final String TAG = getClass().getSimpleName();

    private final long FINISH_INTERVAL_TIME = 1000 * 3;

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;


    private Handler mHander = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(msg.what == 0){
                Intent intent = null;
                if(pm.isLogin()){
                    Uri uri = getIntent().getData();

                    if(uri != null) {

                        String type = uri.getQueryParameter("type");
                        String idx = uri.getQueryParameter("idx");
                        DebugUtils.setLog(TAG, "type : " + type);
                        DebugUtils.setLog(TAG, "idx : " + idx);

                        if(type.equals("1")){
                            intent = new Intent(SplashActivity.this, CanDetailActivity.class);
                        }else{
                            intent = new Intent(SplashActivity.this, NeedDetailActivity.class);
                        }
                        intent.putExtra("idx", Integer.parseInt(idx));
                        intent.putExtra("goMain", true);
                    }else{
                        intent = new Intent(SplashActivity.this, MainActivity.class);
                    }

                }else{
                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                }
                startActivity(intent);
                finish();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        registBroadcastReceiver();

        getInstanceIdToken();
//        getAppKeyHash();

        getArea();
    }

    //지역정보 얻은후 SQLite 저장
    private void getArea(){

        final DataBaseManager db = new DataBaseManager(this);
        ArrayList<Area> areaList = db.getAreaList(null);
        ArrayList<SubArea> subareaList = db.getSubAreaList(null);
        if(areaList == null && subareaList == null){
            if(!networkCheck()){
                finish();
            }else{
                new HttpsManager(this).getArea(new HttpsManager.HttpListner() {
                    @Override
                    public void onSuccess(Object result) {
                        db.insertAllArea((ArrayList<Area>)result);
                        mHander.sendEmptyMessageDelayed(0, FINISH_INTERVAL_TIME);
                    }

                    @Override
                    public void onFailure(String errorStr) {
                        setToast(errorStr);
                    }
                });
            }
        }else{
            mHander.sendEmptyMessageDelayed(0, FINISH_INTERVAL_TIME);
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    //키스토어 해시값
    private void getAppKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                Log.d("hash key:", something);
//                DebugUtils.setLog("Hash key", something);

 //RGJ
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                // set title
                alertDialogBuilder.setTitle("Your Title");
                // set dialog message
                alertDialogBuilder
                        .setMessage(something)
                        .setCancelable(false)
                        .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                            }
                        })
                        .setNegativeButton("No",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                // show it
                alertDialog.show();
            }
        } catch (Exception e) {
            DebugUtils.setLog("name not found", e.toString());
        }
    }


    private void getInstanceIdToken(){
        if(checkPlayServices()){
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    public void registBroadcastReceiver(){
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();


                if(action.equals(QuickstartPreferences.REGISTRATION_READY)){
                    // 액션이 READY일 경우
                } else if(action.equals(QuickstartPreferences.REGISTRATION_GENERATING)){
                    // 액션이 GENERATING일 경우
                } else if(action.equals(QuickstartPreferences.REGISTRATION_COMPLETE)){
                    // 액션이 COMPLETE일 경우
                    String token = intent.getStringExtra("token");
                    pm.setPushToken(token);
                }

            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_READY));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_GENERATING));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));

    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                DebugUtils.setLog(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

   /* private void getAppKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                DebugUtils.setLog("Hash key", something);
                setToast("Hash key : " + something);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            DebugUtils.setLog("name not found", e.toString());
        }
    }*/
}
