package com.kakaoapps.tamutamu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.adapter.LikeAdapter;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.model.Like;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;

/**
 * Created by developer on 2016-07-01.
 */
public class LikeListActivity extends CommonActivity {

    private ListView lv;
    private LikeAdapter adapter;
    private ArrayList<Like> list = new ArrayList<Like>();

    private int page = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_likelist);

        setTitlebar();
        titlebar.backIv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText("관심리스트");
        titlebar.centerTv.setVisibility(View.VISIBLE);

        lv = (ListView)findViewById(R.id.lv);
        lv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (list.size() > 0 && lv.getLastVisiblePosition() >= totalItemCount - 1 && page * 10 < list.get(0).totalCnt) {
                    if(DialogView.mDialog == null){
                        ++page;
                        getList();
                    }
                }
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Like item = list.get(position);
                Intent intent = null;
                boolean isCan = item.type.equals("C") ? true : false;
                if(isCan)intent = new Intent(LikeListActivity.this, CanDetailActivity.class);
                else intent = new Intent(LikeListActivity.this, NeedDetailActivity.class);
                intent.putExtra("idx", item.gidx);
                startActivity(intent);
            }
        });
        getList();
    }

    private void getList(){
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        params.put("start", page);
        new HttpsManager(this).getLikeList(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                if(page == 1){
                    list = (ArrayList<Like>)result;
                    adapter = new LikeAdapter(LikeListActivity.this, list);
                    lv.setAdapter(adapter);
                }else{
                    list.addAll((ArrayList<Like>)result);
                    adapter.notifyDataSetChanged();
                }

                if ( list.size() > 0 ){
                    TextView noneTv = (TextView) findViewById(R.id.tvContentNone);
                    noneTv.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(String errorStr) {
                setToast(errorStr);
            }
        });
    }
}
