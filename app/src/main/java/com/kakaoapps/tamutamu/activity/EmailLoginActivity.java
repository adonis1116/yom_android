package com.kakaoapps.tamutamu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Debug;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.manager.ActivityManager;
import com.kakaoapps.tamutamu.manager.AppManager;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.model.User;
import com.kakaoapps.tamutamu.utils.CheckPatternUtils;
import com.kakaoapps.tamutamu.utils.DebugUtils;
import com.loopj.android.http.RequestParams;

/**
 * Created by john on 2016-02-01.
 */
public class EmailLoginActivity extends CommonActivity implements View.OnClickListener{
    private final String TAG = getClass().getSimpleName();

    private EditText emailEt, passwordEt;
    private TextView emailTv, passwordTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_login);
        ActivityManager.actList.add(this);
        init();

    }

    private void init(){
        setTitlebar();
        titlebar.logoIv.setVisibility(View.VISIBLE);
        emailEt = (EditText)findViewById(R.id.email_et);
        passwordEt = (EditText)findViewById(R.id.password_et);

        Button loginBt = (Button)findViewById(R.id.login_bt);
        Button signupBt = (Button)findViewById(R.id.signup_bt);
        Button findBt = (Button)findViewById(R.id.find_bt);

        loginBt.setOnClickListener(this);
        signupBt.setOnClickListener(this);
        findBt.setOnClickListener(this);

        emailTv = (TextView)findViewById(R.id.email_tv);
        passwordTv = (TextView)findViewById(R.id.password_tv);

    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()){
            case R.id.login_bt:
                loginCheck();
                break;
            case R.id.signup_bt:
                intent = new Intent(this, SignupAccountActivity.class);
                break;
            case R.id.find_bt:
                intent = new Intent(this, FindActivity.class);
                break;
        }

        if(intent != null) startActivity(intent);
    }

    private void loginCheck(){
        emailTv.setVisibility(View.GONE);
        passwordTv.setVisibility(View.GONE);

        if(emailEt.getText().toString().trim().length() == 0){
            emailTv.setVisibility(View.VISIBLE);
            emailTv.setText("이메일을 넣어주세요");
            return;
        }

        if(!new CheckPatternUtils().checkPattern(emailEt.getText().toString(), CheckPatternUtils.PatternType.EMAIL)){
            emailTv.setVisibility(View.VISIBLE);
            emailTv.setText("이메일형식이 맞지 않습니다.");
            return;
        }

        if(passwordEt.getText().toString().trim().length() == 0){
            passwordTv.setVisibility(View.VISIBLE);
            passwordTv.setText("비밀번호를 넣어주세요");
            return;
        }

        if(passwordEt.getText().toString().trim().length() < 4){
            passwordTv.setVisibility(View.VISIBLE);
            passwordTv.setText("비밀번호는 4자리 이상입니다.");
            return;
        }

        login();
    }

    private void login(){
        RequestParams params = new RequestParams();
        params.put("user_type", 1);
        params.put("user_id", emailEt.getText().toString());
        params.put("user_pass", passwordEt.getText().toString());
        params.put("device_type", 1);
        params.put("device_f_id", AppManager.getDeviceSerialNumber());
        params.put("device_push_token", pm.getPushToken());

        DebugUtils.setLog(TAG, AppManager.getDeviceSerialNumber());
        //DebugUtils.setLog(TAG, pm.getPushToken());

        new HttpsManager(this).login(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                pm.setIsLogin(true);
                User newUser = ((User) result);
                newUser.setPassword(passwordEt.getText().toString());
                pm.setUser(newUser);
                getAlarm();
            }

            @Override
            public void onFailure(String errorStr) {
                setToast(errorStr);
            }
        });
    }


    private void getAlarm(){
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        new HttpsManager(this).getAlarm(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                Intent intent = new Intent(EmailLoginActivity.this, MainActivity.class);
                startActivity(intent);
                ActivityManager.allActivityFinish();
            }

            @Override
            public void onFailure(String errorStr) {

            }
        });
    }
}
