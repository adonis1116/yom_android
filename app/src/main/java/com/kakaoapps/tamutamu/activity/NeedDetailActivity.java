package com.kakaoapps.tamutamu.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kakaoapps.tamutamu.Const;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.UrlInfo;
import com.kakaoapps.tamutamu.adapter.ReplyAdapter;
import com.kakaoapps.tamutamu.dialog.BuyConfirmAskDialogue;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.dialog.PaymentDialog;
import com.kakaoapps.tamutamu.dialog.RegistDialog;
import com.kakaoapps.tamutamu.dialog.SNSShareDialog;
import com.kakaoapps.tamutamu.dialog.TwoButtonDialog;
import com.kakaoapps.tamutamu.manager.ActivityManager;
import com.kakaoapps.tamutamu.manager.DataBaseManager;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.manager.PreferencesManager;
import com.kakaoapps.tamutamu.model.*;
import com.kakaoapps.tamutamu.model.Process;
import com.loopj.android.http.RequestParams;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by john on 2016-02-05.
 */
public class NeedDetailActivity extends CommonActivity implements View.OnClickListener{
    private final int MODIFY_NEED = 114;
    private ListView lv;
    private Need item;
    private EditText replyEt;
    private View headerView;
    private Process mProcess;

    private Button statusBt;


    private int page = 1;
    private int ROW_LIMIT = 10;

    private ArrayList<Reply> replyList = new ArrayList<Reply>();
    private ReplyAdapter adapter;

    private ImageView likeIv;
    private boolean isReply = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_need_detail);
        ActivityManager.actList.add(this);
        item = getIntent().getParcelableExtra("need");
        if(item == null){
            getDetail();
        }else{
            init();
        }

    }


    private void init() {
        setTitlebar();
        titlebar.backIv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText(item.getNickName() + "님이 도움을 요청해요");
        if(pm.getUser().getUserId().equals(item.getUserId())){
            titlebar.editLl.setVisibility(View.VISIBLE);
            titlebar.penIv.setOnClickListener(this);
            titlebar.deleteIv.setOnClickListener(this);
            // titlebar.deleteIv.setVisibility(View.GONE); // commented by Adonis 2017.6.29
        }

        replyEt = (EditText)findViewById(R.id.reply_et);
        Button replyBt = (Button)findViewById(R.id.reply_bt);
        replyBt.setOnClickListener(this);

        lv = (ListView)findViewById(R.id.lv);
        lv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (replyList.size() > 0 && lv.getLastVisiblePosition() >= totalItemCount - 1 && page * ROW_LIMIT < replyList.get(0).getTotal()) {
                    if(DialogView.mDialog == null){
                        ++page;
                        getReplyList();
                    }
                }
            }
        });

        setHeader();
        getProgress();
        getReplyList();
    }

    private void getDetail(){
        RequestParams params = new RequestParams();
        params.put("idx", getIntent().getIntExtra("idx", 0));
        new HttpsManager(this).getNeedDetail(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                item = ((ArrayList<Need>)result).get(0);
                init();
            }

            @Override
            public void onFailure(String errorStr) {
                setToast(errorStr);
            }
        });

    }

    private void getReplyList(){
        if(!networkCheck())return;

        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("idx", item.getId());
        params.put("gubun", "N");
        params.put("start", page);
        params.put("pgNum", ROW_LIMIT);
        new HttpsManager(this).getReplyList(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                if (page == 1) {
                    replyList = (ArrayList<Reply>) result;
                    adapter = new ReplyAdapter(NeedDetailActivity.this, replyList);
                    lv.setAdapter(adapter);
                    if(isReply){
                        lv.setSmoothScrollbarEnabled(true);
                        lv.setSelection(replyList.size());
                    }
                } else {
                    replyList.addAll((ArrayList<Reply>) result);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
            }
        });
    }

    private void setHeader(){

        headerView = View.inflate(this, R.layout.header_needdetail, null);
        ImageView userIv = (ImageView)headerView.findViewById(R.id.user_iv);
        TextView nickTv = (TextView)headerView.findViewById(R.id.nick_tv);
        TextView subjectTv = (TextView)headerView.findViewById(R.id.subject_tv);
        TextView priceTv = (TextView)headerView.findViewById(R.id.price_tv);
        TextView wantTv = (TextView)headerView.findViewById(R.id.hope_date_tv);
        TextView resLocTv = (TextView)headerView.findViewById(R.id.loc_tv);
        TextView regdtTv = (TextView)headerView.findViewById(R.id.regdt_tv);
//        TextView contentTv = (TextView)headerView.findViewById(R.id.content_tv); // commented by Adonis 2017.07.03

        // added by Adonis 2017.07.03
        WebView contentWebView = (WebView)headerView.findViewById(R.id.contentWebView);
        WebSettings settings = contentWebView.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        settings.setJavaScriptEnabled(true);
        String content = item.getContent();
        content = "<html><body>" +  content.replace("\n", "<br>") + "</body></html>";
        contentWebView.loadData(content, "text/html; charset=utf-8", "UTF-8");
        ///////////

        Button questionBt = (Button)headerView.findViewById(R.id.question_bt);
        if(pm.getUser().getUserNo() == item.getUserNo())questionBt.setVisibility(View.INVISIBLE);

        statusBt = (Button)headerView.findViewById(R.id.status_bt);
        questionBt.setOnClickListener(this);
        statusBt.setOnClickListener(this);

        if(item.getUserPic() != null && item.getUserPic().length() > 0) {
            if (!item.getUserPic().contains("http")) {
                item.setUserPic(UrlInfo.URL_HEADER + UrlInfo.API_HEADER + "data/profile/" + item.getUserPic());
            }
            Glide.with(this)
                    .load(item.getUserPic())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(userIv);
        }
        else{// added by RGJ 2017.02.17
            Glide.with(this)
                    .load(R.drawable.no_image)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(userIv);
        }
        userIv.setOnClickListener(this);

        nickTv.setText(item.getNickName());
        subjectTv.setText(item.getSubject());
        priceTv.setText(Const.MONEY_DF.format(Long.valueOf(item.getPrice())));
        wantTv.setText(Html.fromHtml(String.format(getString(R.string.want_date), item.getTerm())));
        regdtTv.setText("등록시간:" + item.getRegdt());
        DataBaseManager db = new DataBaseManager(this);
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("id", item.getArea());
        String areaStr = db.getArea(map).getName();
        map.put("id", item.getSubarea());
        String subAreaStr = db.getSubArea(map).getName();

        resLocTv.setText(Html.fromHtml(String.format(getString(R.string.res_loc), areaStr + ">" + subAreaStr)));
        // contentTv.setText(item.getContent());//commented by Adonis 2017.07.03


        LinearLayout actionLl = (LinearLayout)headerView.findViewById(R.id.action_ll);
        if(item.getUserNo() != pm.getUser().getUserNo()){
            actionLl.setVisibility(View.VISIBLE);
            headerView.findViewById(R.id.share_iv).setOnClickListener(this);
            headerView.findViewById(R.id.singo_iv).setOnClickListener(this);
            likeIv = (ImageView) headerView.findViewById(R.id.like_iv);
            likeIv.setOnClickListener(this);
        }

        lv.addHeaderView(headerView);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == MODIFY_NEED){
            if(resultCode == RESULT_OK){
                item = data.getParcelableExtra("need");
                lv.removeHeaderView(headerView);
                init();
            }
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch(v.getId()){
            case R.id.pen_iv:
               /* intent = new Intent(this, NeedWriteActivity.class);
                intent.putExtra("need", item);*/
                startActivityForResult(new Intent(this, NeedWriteActivity.class).putExtra("need", item), MODIFY_NEED);
                break;
            case R.id.delete_iv:
                new TwoButtonDialog(this, "게시글 삭제", "현재 게시글을\n삭제하시겠습니까?", "확인", "취소", new TwoButtonDialog.ChoiceListner() {
                    @Override
                    public void confirm() {
                        delete();
                    }

                    @Override
                    public void cancle() {

                    }
                }).show();
                break;
            case R.id.user_iv:
                if(pm.getUser().getUserNo() == item.getUserNo()){
                    intent = new Intent(this, MypageActivity.class);
                }else{
                    intent = new Intent(this, OtherPageActivity.class);
                    intent.putExtra("write_id", item.getUserNo());
                }
                break;
            case R.id.reply_bt:
                writeReply();
                break;

            case R.id.question_bt:
                intent = new Intent(this, QuestionActivity.class);
                intent.putExtra("partner_no", item.getUserNo());
                intent.putExtra("partner_id", item.getUserId());
                break;

            case R.id.status_bt:
                if(item.getUserNo() == pm.getUser().getUserNo()){
                    intent = new Intent(this, RegistListActivity.class);
                    intent.putExtra("idx", item.getId());
                    intent.putExtra("is_can", false);
                }else{
                    showProcessDialog();
                }
                break;
            case R.id.share_iv:
                new SNSShareDialog(NeedDetailActivity.this, NeedDetailActivity.this, item).show();
                break;
            case R.id.singo_iv:
                new TwoButtonDialog(this, "신고하기", "부적절한 컨텐츠로\n신고하시겠습니까?", "예", "아니오", new TwoButtonDialog.ChoiceListner() {
                    @Override
                    public void confirm() {
                        singo();
                    }

                    @Override
                    public void cancle() {

                    }
                }).show();
                break;
            case R.id.like_iv:
                setLike();
                break;
        }

        if(intent != null)startActivity(intent);
    }

    private void singo(){
        if(!networkCheck())return;
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        params.put("gidx", item.getId());

        new HttpsManager(this).registSingo(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                //TODO 성공시 처리
            }

            @Override
            public void onFailure(String errorStr) {
                setToast(errorStr);
            }
        });

    }
    private void setLike(){
        if(!networkCheck())return;
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        params.put("gidx", item.getId());
        params.put("status", item.isWish() ? 0 : 1);
        new HttpsManager(this).registWish(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                //TODO 성공시 처리
                likeIv.setImageResource(R.drawable.icon_like_on);
            }

            @Override
            public void onFailure(String errorStr) {
                setToast(errorStr);
            }
        });
    }


    private void showProcessDialog(){
        switch (mProcess.getType()) {
            case 0:
                new RegistDialog(this, new RegistDialog.ChoiceListner() {
                    @Override
                    public void confirm(String content, String price) {
                        regist(content, price);
                    }

                    @Override
                    public void cancle() {

                    }
                }).show();
                break;
            case 3:
                new BuyConfirmAskDialogue(this, new BuyConfirmAskDialogue.ChoiceListner() {
                    @Override
                    public void confirm(File file) {
                        if (file == null){
                            action();
                        }
                        else{
                            send(file);
                        }
                    }
                    @Override
                    public void cancel() {

                    }
                }).show();
/*                new TwoButtonDialog(this, "구매확정요청하기", "구매확정 요청을 보내시겠습니까?", "확인", "취소", new TwoButtonDialog.ChoiceListner() {
                    @Override
                    public void confirm() {
                        action();
                    }

                    @Override
                    public void cancle() {

                    }
                }).show();*/
                break;
        }
    }


    private void regist(String content, String price){
        if(!networkCheck())return;
        DialogView.progressDialogShow(this);
        // added by Adonis 2017.07.10
        Long userPointLong = Long.parseLong(pm.getUser().getPoint());
        Long priceLong = Long.parseLong(price);
        if (userPointLong < priceLong) {
            new PaymentDialog(this, price, 1, mProcess, new PaymentDialog.PaymetListner() {
                @Override
                public void onSuccess() {
//                    mProcess.setType(mProcess.getType()+1);
//                    setStatusBt();
                }
            }).show();
        }
        else{
            RequestParams params = new RequestParams();
            params.put("user_no", pm.getUser().getUserNo());
            params.put("idx", item.getId());
            params.put("content", content);
            params.put("price", price);
            params.put("writer_no", item.getUserNo());
            mProcess.setType(1);// added by Adonis 2017.07.08
            params.put("type", mProcess.getType());// added by Adonis 2017.07.08
            new HttpsManager(this).needAction(params, new HttpsManager.HttpListner() {
                //new HttpsManager(this).needAction(params, new HttpsManager.HttpListner() { // commented by Adonis 2017.07.08
                @Override
                public void onSuccess(Object result) {
                    DialogView.progressDialogClose();
                    setToast("금액제안을 하였습니다.");
                /*mProcess.setType(1);*///commented by Adonis 2017.07.08
                    setStatusBt();
                }

                @Override
                public void onFailure(String errorStr) {
                    DialogView.progressDialogClose();
                    setToast(errorStr);
                }
            });
        }
    }

    private void action(){
        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("idx", mProcess.getId());
        params.put("type", mProcess.getType());
        params.put("user_no", pm.getUser().getUserNo());

        new HttpsManager(this).needAction(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                mProcess.setType(mProcess.getType() + 1);
                setStatusBt();
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);

            }
        });
    }

    private void send(final File file){
        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        PreferencesManager pm = new PreferencesManager(this);
        params.put("user_no", pm.getUser().getUserNo());
        params.put("user_id", pm.getUser().getUserId());
        params.put("g_user_no", item.getUserNo());
        params.put("buy_confirm", 1); // 구매확정요청인가 0: 아니 1: 예
        params.put("g_type", 2); // 해줄게욤인가? 1: 예 2: 해주세욤
        params.put("idx",  mProcess.getId());
        if(file != null){
            try {
                params.put("upfile", file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        new HttpsManager(this).registMemo(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                action();
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast("첨부파일을 보낼수가 없네요. 쪽지를 이용해 시도해보세요.");
            }
        });

    }
    // 나의 페이지 해주세욤 게시물 삭제 commented by Adonis 2017.6.29
    private void delete(){
        if(!networkCheck())return;

        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("idx", item.getId());
        new HttpsManager(this).productDelete(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                setToast("삭제되었습니다.");
                finish();
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }

    private void writeReply(){
        if(replyEt.getText().toString().trim().length() == 0){
            setToast("댓글을 입력하세요");
            return;
        }

        if(!networkCheck())return;

        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();

        params.put("bidx", item.getId());
        params.put("user_no", pm.getUser().getUserNo());
        params.put("content", replyEt.getText().toString());
        params.put("gubun", "N");
        new HttpsManager(this).writeReply(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                isReply = true;
                replyEt.setText("");
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(replyEt.getWindowToken(), 0);
                getReplyList();
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }


    private void getProgress(){
        if(item.getUserNo() == pm.getUser().getUserNo()){
            statusBt.setText("진행상황");
            return;
        }

        if(!networkCheck())return;

        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("idx", item.getId());
        params.put("user_no", pm.getUser().getUserNo());
        new HttpsManager(this).needDetail(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                mProcess = (Process) result;
                setStatusBt();
            }

            @Override
            public void onFailure(String errorStr) {
                setToast(errorStr);
            }
        });
    }

    public void setStatusBt(){
        String strs[] = {"금액제안", "결제대기중", "결제대기중", "구매확정요청", "완료대기중", "완료됨", "거절"};
//        String strs[] = {"금액제안", "수락대기중", "결제대기중", "작업완료", "승인대기중", "완료", "거절"};// commented by Adonis 2017.07.08

        switch (mProcess.getType()) {
            case 0:
                statusBt.setText(strs[0]);
                break;
            case 1:
                statusBt.setText(strs[1]);
                break;
            case 2:
                statusBt.setText(strs[2]);
                break;
            case 3:
                statusBt.setText(strs[3]);
                break;
            case 4:
                statusBt.setText(strs[4]);
                break;
            case 5:
                statusBt.setText(strs[5]);
                break;
            case 100:
                statusBt.setText(strs[6]);
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(getIntent().getBooleanExtra("goMain", false)){
            finish();
        }else{
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }
}
