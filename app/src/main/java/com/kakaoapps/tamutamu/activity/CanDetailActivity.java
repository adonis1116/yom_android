package com.kakaoapps.tamutamu.activity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.kakaoapps.tamutamu.Const;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.UrlInfo;
import com.kakaoapps.tamutamu.adapter.HeaderImageAdapter;
import com.kakaoapps.tamutamu.adapter.ReplyAdapter;
import com.kakaoapps.tamutamu.adapter.TagAdapter;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.dialog.PaymentDialog;
import com.kakaoapps.tamutamu.dialog.RegistDialog;
import com.kakaoapps.tamutamu.dialog.SNSShareDialog;
import com.kakaoapps.tamutamu.dialog.TwoButtonDialog;
import com.kakaoapps.tamutamu.manager.ActivityManager;
import com.kakaoapps.tamutamu.manager.DataBaseManager;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.manager.SelectPhotoManager;
import com.kakaoapps.tamutamu.model.*;
import com.kakaoapps.tamutamu.model.Process;
import com.kakaoapps.tamutamu.utils.DebugUtils;
import com.kakaoapps.tamutamu.widget.HorizontalListView;
import com.loopj.android.http.RequestParams;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;

import static com.kakaoapps.tamutamu.UrlInfo.API_HEADER;

/**
 * Created by john on 2016-02-04.
 */
public class CanDetailActivity extends CommonActivity implements View.OnClickListener{
    private final String TAG = getClass().getSimpleName();
    private final int MODIFY_CAN = 115;
    private Can mItem;
    private ListView lv;
    private EditText replyEt;
    private View headerView;
    private Button statusBt, attachBt;
    private Process mProcess;

    private int page = 1;
    private int ROW_LIMIT = 10;

    private ArrayList<Reply> replyList = new ArrayList<Reply>();
    private ReplyAdapter adapter;

    private ImageView likeIv;
    private boolean isReply = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_can_detail);
        ActivityManager.actList.add(this);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        mItem = getIntent().getParcelableExtra("can");

        if(mItem == null){
            getDetail();
        }else{
            init();
        }
    }

    private void init(){
        setTitlebar();
        titlebar.backIv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText(mItem.getNickName() + "님이 해줄 수 있어요");
        if(pm.getUser().getUserId().equals(mItem.getUserId())){
            titlebar.editLl.setVisibility(View.VISIBLE);
            titlebar.penIv.setOnClickListener(this);
            titlebar.deleteIv.setOnClickListener(this);
        }

        lv = (ListView)findViewById(R.id.lv);
        replyEt = (EditText)findViewById(R.id.reply_et);
        Button replyBt = (Button)findViewById(R.id.reply_bt);
        replyBt.setOnClickListener(this);

        setHeader();
        getProgress();
        getReplyList();
    }

    private void getDetail(){
        RequestParams params = new RequestParams();
        params.put("idx", getIntent().getIntExtra("idx", 0));
        new HttpsManager(this).getCanDetail(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                mItem = ((ArrayList<Can>)result).get(0);
                init();
            }

            @Override
            public void onFailure(String errorStr) {
                setToast(errorStr);
            }
        });
    }

    private void getReplyList(){
        if(!networkCheck())return;

        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("idx", mItem.getId());
        params.put("gubun", "C");
        params.put("start", page);
        params.put("pgNum", ROW_LIMIT);
        new HttpsManager(this).getReplyList(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                if(page == 1){
                    replyList = (ArrayList<Reply>) result;
                    adapter = new ReplyAdapter(CanDetailActivity.this, replyList);
                    lv.setAdapter(adapter);
                    if(isReply){
                        lv.setSmoothScrollbarEnabled(true);
                        lv.setSelection(replyList.size());
                    }
                }else{
                    replyList.addAll((ArrayList<Reply>) result);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
            }
        });
    }

    // 해주세욤 상세내용 설정 RGJ
    private void setHeader(){
        headerView = View.inflate(this, R.layout.header_candetail, null);
        HeaderImageAdapter adapter = new HeaderImageAdapter(this, mItem.getImgList());
        ViewPager vp = (ViewPager)headerView.findViewById(R.id.vp);
        vp.setAdapter(adapter);

        Button questionBt = (Button)headerView.findViewById(R.id.question_bt);
        statusBt = (Button)headerView.findViewById(R.id.status_bt); //현재 상태 단추 : 진행상황
        questionBt.setOnClickListener(this);
        if(pm.getUser().getUserNo() == mItem.getUserNo())questionBt.setVisibility(View.INVISIBLE);
        statusBt.setOnClickListener(this);

        attachBt = (Button)headerView.findViewById(R.id.attach_bt);
        attachBt.setOnClickListener(this);
        if(pm.getUser().getUserNo() == mItem.getUserNo()) attachBt.setVisibility(View.GONE);
        if(mItem.getFileList().size() == 0) attachBt.setVisibility(View.GONE);
        TextView symbolTv = (TextView)headerView.findViewById(R.id.symbol_tv);
        symbolTv.setText(Currency.getInstance(Locale.KOREA).getSymbol());


        TextView subjectTv = (TextView)headerView.findViewById(R.id.subject_tv);
        TextView nickTv = (TextView)headerView.findViewById(R.id.nick_tv);
        TextView regdtTv = (TextView)headerView.findViewById(R.id.regdt_tv);

//        TextView contentTv = (TextView)headerView.findViewById(R.id.content_tv);

        //added by Adonis 2017.07.02
        WebView contentWebView = (WebView)headerView.findViewById(R.id.contentWebView);
        WebSettings settings = contentWebView.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        settings.setJavaScriptEnabled(true);
        String content = mItem.getContent();
//      content = content.replaceAll( "src=\"/assets/", "src=\"" + ServiceParams.assetsBaseUrl);
            content = "<html><body>" + content.replace("\n", "<br>") + "</body></html>";

        contentWebView.loadData(content, "text/html; charset=utf-8", "UTF-8");
        ///////////

        TextView locTv = (TextView)headerView.findViewById(R.id.loc_tv);
        TextView priceTv = (TextView)headerView.findViewById(R.id.price_tv);

        subjectTv.setText(mItem.getSubject());
        nickTv.setText(mItem.getNickName());
//        contentTv.setText(mItem.getContent());
        regdtTv.setText("등록시간:" + mItem.getRegdt());
        DataBaseManager db = new DataBaseManager(this);

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("id", mItem.getArea());
        String areaStr = db.getArea(map).getName();
        map.put("id", mItem.getSubArea());
        String subAreaStr = db.getSubArea(map).getName();
        locTv.setText(areaStr + ">" + subAreaStr);
        priceTv.setText(Const.MONEY_DF.format(Long.valueOf(mItem.getPrice())));

        HorizontalListView hlv = (HorizontalListView)headerView.findViewById(R.id.hlv);
        TagAdapter tagAdapter = new TagAdapter(this, mItem.getTagList());
        hlv.setAdapter(tagAdapter);

        CircularImageView photoIv = (CircularImageView)headerView.findViewById(R.id.photo_iv);
        photoIv.setOnClickListener(this);
//        photoIv.setBorderWidth(0);


        if(mItem.getUserImage() != null && mItem.getUserImage().length() > 0){
            if ( !mItem.getUserImage().contains("http")){
                mItem.setUserImage(UrlInfo.URL_HEADER + UrlInfo.API_HEADER + "data/profile/" + mItem.getUserImage());

            }
            Glide.with(this)
                .load(mItem.getUserImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(photoIv);
        }else{// added by RGJ 2017.02.17
            Glide.with(this)
                    .load(R.drawable.no_image)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(photoIv);
        }

        LinearLayout actionLl = (LinearLayout)headerView.findViewById(R.id.action_ll);

        if(mItem.getUserNo() != pm.getUser().getUserNo()){
            actionLl.setVisibility(View.VISIBLE);
            headerView.findViewById(R.id.share_iv).setOnClickListener(this);
            headerView.findViewById(R.id.singo_iv).setOnClickListener(this);
            likeIv = (ImageView) headerView.findViewById(R.id.like_iv);
            likeIv.setImageResource(mItem.isWish() ? R.drawable.icon_like_on : R.drawable.icon_like_off);
            likeIv.setOnClickListener(this);
        }

        lv.addHeaderView(headerView);
    }



    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()){
            case R.id.question_bt:
                intent = new Intent(this, QuestionActivity.class);
                intent.putExtra("partner_no", mItem.getUserNo());
                intent.putExtra("partner_id", mItem.getUserId());
                break;
            case R.id.pen_iv:
                startActivityForResult(new Intent(this, CanWriteActivity.class).putExtra("can", mItem), MODIFY_CAN);
                break;
            case R.id.delete_iv:
                new TwoButtonDialog(this, "게시글 삭제", "현재 게시글을\n삭제하시겠습니까?", "확인", "취소", new TwoButtonDialog.ChoiceListner() {
                    @Override
                    public void confirm() {
                        delete();
                    }
                    @Override
                    public void cancle() { }
                }).show();
                break;
            case R.id.photo_iv: //modified by Adonis 2017.07.04

                if(pm.getUser().getUserNo() == mItem.getUserNo()){
                    intent = new Intent(this, MypageActivity.class);
                }else{
                    intent = new Intent(this, OtherPageActivity.class);
                    intent.putExtra("write_id", mItem.getUserNo());
                }

                break;
            case R.id.reply_bt:
                writeReply();
                break;
            case R.id.status_bt:
                if(mItem.getUserNo() == pm.getUser().getUserNo()){
                    intent = new Intent(this, RegistListActivity.class);
                    intent.putExtra("idx", mItem.getId());
                    intent.putExtra("is_can", true);
                }else{
                    showProcessDialog();
                }

                break;
            case R.id.share_iv:
                new SNSShareDialog(CanDetailActivity.this, CanDetailActivity.this, mItem).show();
                break;
            case R.id.singo_iv:
                new TwoButtonDialog(this, "신고하기", "부적절한 컨텐츠로\n신고하시겠습니까?", "예", "아니오", new TwoButtonDialog.ChoiceListner() {
                    @Override
                    public void confirm() {
                        singo();
                    }

                    @Override
                    public void cancle() {

                    }
                }).show();
                break;
            case R.id.like_iv:
                setLike();
                break;
            case R.id.attach_bt:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int permissionCheck = ContextCompat.checkSelfPermission(CanDetailActivity.this,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                        fileDownload();
                    } else {
                        ActivityCompat.requestPermissions(CanDetailActivity.this,
                                new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,
                                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA}, SelectPhotoManager.MY_CAMERA_REQUEST_CODE);
                    }
                    return;
                } else {
                    fileDownload();
                }

                break;
        }

        if(intent != null)startActivity(intent);
    }

    private static String getExtension(String fileStr) {
        return fileStr.substring(fileStr.lastIndexOf(".") + 1, fileStr.length());
    }

    private void fileDownload(){

        if(mItem.getFileList().get(0).length() == 0) return;
        String strFileUrl = mItem.getFileList().get(0);
// Getting file name
        int dotPosition = strFileUrl.lastIndexOf('.');
        int slashPosition = strFileUrl.lastIndexOf('/');
        final String strExt = strFileUrl.substring(dotPosition + 1);
        final String strFileName = strFileUrl.substring(slashPosition + 1, dotPosition );

        final File file = new File(Const.SAVE_FILE_DIR + strFileName + "." + strExt);
        if(file.exists()) {
            try{

                Uri uri = Uri.fromFile(file);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_DEFAULT);

                if (file.toString().contains(".doc") || file.toString().contains(".docx")) {
                    // Word document
                    intent.setDataAndType(uri, "application/msword");
                } else if(file.toString().contains(".pdf")) {
                    // PDF file
                    intent.setDataAndType(uri, "application/pdf");
                } else if(file.toString().contains(".ppt") || file.toString().contains(".pptx")) {
                    // Powerpoint file
                    intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
                } else if(file.toString().contains(".xls") || file.toString().contains(".xlsx")) {
                    // Excel file
                    intent.setDataAndType(uri, "application/vnd.ms-excel");
                } else if(file.toString().contains(".zip") || file.toString().contains(".rar")) {
                    // WAV audio file
                    intent.setDataAndType(uri, "application/zip");
                } else if(file.toString().contains(".rtf")) {
                    // RTF file
                    intent.setDataAndType(uri, "application/rtf");
                } else if(file.toString().contains(".wav") || file.toString().contains(".mp3")) {
                    // WAV audio file
                    intent.setDataAndType(uri, "audio/x-wav");
                } else if(file.toString().contains(".gif")) {
                    // GIF file
                    intent.setDataAndType(uri, "image/gif");
                } else if(file.toString().contains(".jpg") || file.toString().contains(".jpeg") || file.toString().contains(".png")) {
                    // JPG file
                    intent.setDataAndType(uri, "image/jpeg");
                } else if(file.toString().contains(".txt")) {
                    // Text file
                    intent.setDataAndType(uri, "text/plain");
                } else if(file.toString().contains(".3gp") || file.toString().contains(".mpg") || file.toString().contains(".mpeg") || file.toString().contains(".mpe") || file.toString().contains(".mp4") || file.toString().contains(".avi")) {
                    // Video files
                    intent.setDataAndType(uri, "video/*");
                } else if (file.toString().contains("hwp")) {
                    intent.setDataAndType(uri, "application/hansofthwp");
                } else {
                    intent.setDataAndType(uri, "*/*");
                }

                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                getApplicationContext().startActivity(intent);
                return;
            }
            catch(ActivityNotFoundException e)
            {
                setToast(strFileName + "." + strExt + "을 확인할 수 있는 앱이 없습니다.");
            }
            return;
        }
        DialogView.progressDialogShow(this);
        attachBt.setText("다운로드중..");
        new HttpsManager(this).download(strFileUrl, strFileName + "." + strExt, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                setToast(Const.SAVE_FILE_DIR + (String)result + " 에 저장하였습니다.");
                attachBt.setText("첨부파일열기");
                DialogView.progressDialogClose();
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
            }
        });
    }

    private void singo(){
        if(!networkCheck())return;
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        params.put("gidx", mItem.getId());

        new HttpsManager(this).registSingo(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                //TODO 성공시 처리

            }

            @Override
            public void onFailure(String errorStr) {
                setToast(errorStr);
            }
        });

    }
    private void setLike(){
        if(!networkCheck())return;
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        params.put("gidx", mItem.getId());
        params.put("status", mItem.isWish() ? 0 : 1);

        new HttpsManager(this).registWish(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                if(mItem.isWish()) likeIv.setImageResource(R.drawable.icon_like_off);
                else likeIv.setImageResource(R.drawable.icon_like_on);
                mItem.setWish(!mItem.isWish());
            }

            @Override
            public void onFailure(String errorStr) {
                setToast(errorStr);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == MODIFY_CAN){
            if(resultCode == RESULT_OK){
                mItem = data.getParcelableExtra("can");
                DebugUtils.setLog(TAG, "subject : " + mItem.getSubject());

                lv.removeHeaderView(headerView);
                setHeader();
                // added by Adonis
                getProgress();
                getReplyList();
            }
        }
    }

    // 나의 페이지 해줄게욤 게시물 삭제 commented by Adonis 2017.6.29
    private void delete(){
        if(!networkCheck())return;

        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("idx", mItem.getId());
        new HttpsManager(this).productDelete(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                setToast("삭제되었습니다.");
                finish();
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }

    private void writeReply(){

        if(replyEt.getText().toString().trim().length() == 0){
            setToast("댓글을 입력하세요");
            return;
        }

        if(!networkCheck())return;

        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("bidx", mItem.getId());
        params.put("user_no", pm.getUser().getUserNo());
        params.put("content", replyEt.getText().toString());
        params.put("gubun", "C");
        new HttpsManager(this).writeReply(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {

                isReply = true;
                replyEt.setText("");
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(replyEt.getWindowToken(), 0);
                getReplyList();
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }

    private void getProgress(){
        if(mItem.getUserNo() == pm.getUser().getUserNo()){
            statusBt.setText("진행상황");
            return;
        }

        if(!networkCheck())return;

        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("idx", mItem.getId());
        params.put("user_no", pm.getUser().getUserNo());
        new HttpsManager(this).canDetail(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                mProcess = (Process) result;
                setStatusBt();
            }

            @Override
            public void onFailure(String errorStr) {
                setToast(errorStr);
            }
        });
    }

    private void showProcessDialog(){
        switch (mProcess.getType()) {
            case 0:
                new RegistDialog(this, new RegistDialog.ChoiceListner() {
                    @Override
                    public void confirm(String content, String price) {
                        regist(content, price);
                    }
                    @Override
                    public void cancle() {
                    }
                }).show();
                break;
            case 2:
                break;
            case 4:
                final Intent intent = new Intent(this, ReviewWriteActivity.class);
                new TwoButtonDialog(this, "구매확정하기", "거래를 완료하시겠습니까?", "확인", "취소", new TwoButtonDialog.ChoiceListner() {
                    @Override
                    public void confirm() {
                        action();
                        intent.putExtra("user_id", mProcess.getUserNo());
                        if(intent != null)startActivity(intent);
                    }

                    @Override
                    public void cancle() {

                    }
                }).show();
                break;
        }
    }

    private void regist(String content, String price){
        if(!networkCheck())return;

        DialogView.progressDialogShow(this);
        // added by Adonis 2017.07.10
        Long userPointLong = Long.parseLong(pm.getUser().getPoint());
        Long priceLong = Long.parseLong(price);
        if (userPointLong < priceLong) {
            new PaymentDialog(this, mItem.getPrice(), 1, mProcess, new PaymentDialog.PaymetListner() {
                @Override
                public void onSuccess() {
//                    mProcess.setType(mProcess.getType()+1);
//                    setStatusBt();
                }
            }).show();
        }
        else{

            RequestParams params = new RequestParams();
            params.put("user_no", pm.getUser().getUserNo());
            params.put("idx", mItem.getId());
            params.put("content", content);
            params.put("price", price);
            params.put("writer_no", mItem.getUserNo());
            mProcess.setType(1);
            params.put("type", mProcess.getType());

            //  new HttpsManager(this).canRegist(params, new HttpsManager.HttpListner() {
            new HttpsManager(this).canAction(params, new HttpsManager.HttpListner() {
                @Override
                public void onSuccess(Object result) {
                    DialogView.progressDialogClose();
                    setToast("주문되었습니다.");
//                mProcess.setType(1);//commented by Adonis 2017.07.08
                    setStatusBt();
                }

                @Override
                public void onFailure(String errorStr) {
                    DialogView.progressDialogClose();
                    setToast(errorStr);
                }
            });
        }
    }

    private void action(){
        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("idx", mProcess.getId());
        params.put("type", mProcess.getType());
        params.put("user_no", pm.getUser().getUserNo());

        new HttpsManager(this).canAction(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                mProcess.setType(mProcess.getType()+1);
                setStatusBt();
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);

            }
        });
    }

    public void setStatusBt(){
        String strs[] = {"결제 주문", "수락대기중", "결제완료됨", "결제완료", "구매확정하기", "완료됨", "거절됨"}; // added by Adonis 2017.07.08
        //String strs[] = {"주문하기", "수락대기중", "결제하기", "결제완료", "승인하기", "완료", "거절"}; //commented by Adonis 2017.07.08

        switch (mProcess.getType()) {
            case 0:
                statusBt.setText(strs[0]);
                break;
            case 1:
                statusBt.setText(strs[1]);
                break;
            case 2:
                statusBt.setText(strs[2]);
                break;
            case 3:
                statusBt.setText(strs[3]);
                break;
            case 4:
                statusBt.setText(strs[4]);
                break;
            case 5:
                statusBt.setText(strs[5]);
                break;
            case 100:
                statusBt.setText(strs[6]);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        if (requestCode == SelectPhotoManager.MY_CAMERA_REQUEST_CODE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(this, "기기에 대한 접근이 허용되었습니다.", Toast.LENGTH_LONG).show();

            } else {

                Toast.makeText(this, "기기에 접근을 제한하면 앱이 비정상으로 동작할수도 있습니다.", Toast.LENGTH_LONG).show();

            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(!getIntent().getBooleanExtra("goMain", false)){
            finish();
        }else{
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }
}
