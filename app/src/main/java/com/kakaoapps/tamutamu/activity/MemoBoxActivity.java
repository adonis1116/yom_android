package com.kakaoapps.tamutamu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.adapter.MemoAdapter;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.model.Memo;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;

/**
 * Created by developer on 2016-06-30.
 */
public class MemoBoxActivity extends CommonActivity {

    private MemoAdapter adapter;
    private ListView lv;
    private ArrayList<Memo> list = new ArrayList<>();
    private int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memobox);

        setTitlebar();
        titlebar.backIv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText("쪽지함");
        titlebar.centerTv.setVisibility(View.VISIBLE);

        lv = (ListView)findViewById(R.id.lv);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MemoBoxActivity.this, QuestionActivity.class);
                Memo item = adapter.getItem(position);

                String partnerId = item.receiveId;

                if(partnerId.equals(pm.getUser().getUserId())){
                    partnerId = item.sendId;
                }

                intent.putExtra("partner_no", item.partnerNo);
                intent.putExtra("partner_id", partnerId);
                startActivity(intent);
            }
        });

        lv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (list.size() > 0 && lv.getLastVisiblePosition() >= totalItemCount - 1 && page * 10 < list.get(0).totalRow) {
                    ++page;
                    getList();
                }
            }
        });
        getList();
    }

    @Override
    public void onStart(){
        super.onStart();
        getList();
    }

    private void getList(){
        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        params.put("start", page);
        new HttpsManager(this).getChatRoomList(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                if(page == 1){
                    list = (ArrayList<Memo>)result;
                    adapter = new MemoAdapter(MemoBoxActivity.this, list);
                    lv.setAdapter(adapter);
                }else{
                    list.addAll((ArrayList<Memo>)result);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }
}
