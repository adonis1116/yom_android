package com.kakaoapps.tamutamu.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kakaoapps.tamutamu.CommonApplication;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.manager.AppManager;
import com.kakaoapps.tamutamu.manager.PreferencesManager;
import com.kakaoapps.tamutamu.model.User;
import com.kakaoapps.tamutamu.utils.DebugUtils;
import com.kakaoapps.tamutamu.utils.RecycleUtils;

/**
 * Created by John on 2016-01-29.
 */
public class CommonActivity extends Activity {
    private final String TAG = getClass().getSimpleName();
    protected PreferencesManager pm;

    class Titlebar{
        ImageView logoIv, backIv, penIv, deleteIv, settingIv, reportIv;
        LinearLayout editLl;
        TextView centerTv, rightTv;
        Button processBt, btMemo;
        RelativeLayout rlNoti;
    }

    protected Titlebar titlebar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CommonApplication.setCurrentActivity(this);
        pm = new PreferencesManager(this);
    }

    protected void setTitlebar(){
        titlebar = new Titlebar();
        titlebar.logoIv = (ImageView)findViewById(R.id.logo_iv);
        titlebar.backIv = (ImageView)findViewById(R.id.back_iv);
        titlebar.penIv = (ImageView)findViewById(R.id.pen_iv);
        titlebar.deleteIv = (ImageView)findViewById(R.id.delete_iv);
        titlebar.settingIv = (ImageView)findViewById(R.id.setting_iv);
        titlebar.processBt = (Button) findViewById(R.id.process_bt);
        titlebar.editLl = (LinearLayout)findViewById(R.id.edit_ll);
        titlebar.centerTv = (TextView)findViewById(R.id.center_tv);
        titlebar.rightTv = (TextView)findViewById(R.id.right_tv);
        titlebar.reportIv = (ImageView)findViewById(R.id.report_iv);
        titlebar.btMemo = (Button)findViewById(R.id.btMemobox);
        titlebar.rlNoti = (RelativeLayout)findViewById(R.id.rl_top_noti);
    }

    /*@Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("login_user", CommonApplication.getLoginUser());

    }

     @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
         super.onRestoreInstanceState(savedInstanceState);
         User user = savedInstanceState.getParcelable("login_user");
         CommonApplication.setLoginUser(user);
    }*/

    public void onBack(View v){
        onBackPressed();
    }

    protected void setToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }


    public boolean networkCheck(){

        if(!AppManager.isNetworkCoonnection(this)){
            setToast(getString(R.string.network_not_connect));
            return false;
        }
        return true;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearReferences();
        RecycleUtils.recursiveRecycle(getWindow().getDecorView());
        System.gc();
    }

    private void clearReferences() {
        Activity currActivity = CommonApplication.getCurrentActivity();
        if (currActivity != null && currActivity.equals(this)) {
            CommonApplication.setCurrentActivity(null);
        }
    }
}
