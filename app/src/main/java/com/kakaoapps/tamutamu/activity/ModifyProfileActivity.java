package com.kakaoapps.tamutamu.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.LogoutResponseCallback;
import com.kakao.usermgmt.callback.UnLinkResponseCallback;
import com.kakaoapps.tamutamu.Const;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.UrlInfo;
import com.kakaoapps.tamutamu.adapter.CommonBaseAdapter;
import com.kakaoapps.tamutamu.adapter.ViewHolder;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.dialog.RowTwoButtonDialog;
import com.kakaoapps.tamutamu.dialog.TwoButtonDialog;
import com.kakaoapps.tamutamu.manager.ActivityManager;
import com.kakaoapps.tamutamu.manager.DataBaseManager;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.manager.SelectPhotoManager;
import com.kakaoapps.tamutamu.model.Area;
import com.kakaoapps.tamutamu.model.SubArea;
import com.kakaoapps.tamutamu.model.User;
import com.kakaoapps.tamutamu.utils.DebugUtils;
import com.kakaoapps.tamutamu.widget.HorizontalListView;
import com.loopj.android.http.RequestParams;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by john on 2016-02-11.
 */
public class ModifyProfileActivity extends CommonActivity implements View.OnClickListener{
    private final String TAG = getClass().getSimpleName();

    private User mUser;

    private CircularImageView picIv;
    private EditText nickEt, phoneEt, snsEt, passEt, passConfirmEt, overviewEt;//added by Adonis 2017.07.07

    private final int SELECT_LOC_REQUEST_CODE = 111;

    private SelectPhotoManager mSelectPhotoManager, mSelectPhotoManagerForPotfolio;

    private final int PHOTO_MAX = 5;
    private ArrayList<PhotoType> photoList;
    private PhotoAdapter adapter;
    private int mPosition = -1;

    private File file;
    private Bitmap selectBitmap;

    private boolean isNickCheck;
    private String checkNickStr = "";
    private boolean isPortfolio;
    class LocationLayout{
        LinearLayout locLl;
        TextView locTv;
        ImageView locIv;
    }

    class SelectArea{
        int area;
        int subArea;
        String selectStr;
    }


    private LocationLayout locationLayout[] = new LocationLayout[Const.LOCAITON_LIMIT];
    private ArrayList<SelectArea> selectLocList = new ArrayList<SelectArea>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_profile);
        mUser = pm.getUser();
        init();
    }

    private void init(){
        setTitlebar();
        titlebar.backIv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText("프로필 편집");
        titlebar.centerTv.setVisibility(View.VISIBLE);
        titlebar.rightTv.setVisibility(View.VISIBLE);
        titlebar.rightTv.setText("완료");
        titlebar.rightTv.setOnClickListener(this);

        picIv = (CircularImageView)findViewById(R.id.pic_iv);
        picIv.setOnClickListener(this);
        nickEt = (EditText)findViewById(R.id.nick_et);
        phoneEt = (EditText)findViewById(R.id.phone_et);
        snsEt = (EditText)findViewById(R.id.sns_et);

        overviewEt = (EditText)findViewById(R.id.overview_et);//added by Adonis 2017.07.07

        Button nickCheckBt = (Button)findViewById(R.id.check_bt);
        nickCheckBt.setOnClickListener(this);

        Button btDeletAccount = (Button) findViewById(R.id.bt12);
        btDeletAccount.setOnClickListener(this);

        nickEt.setText(mUser.getUserNick());
        phoneEt.setText(mUser.getUserPhone());
        snsEt.setText(mUser.getSnsId());
        if (pm.getUser().getUserType() == 2){
            snsEt.setFocusable(false);
        }
        overviewEt.setText(mUser.getOverview());//added by Adonis 2017.07.07
        photoList = new ArrayList<PhotoType>();
        if (mUser.getPortfolio() != null) {
            for (String item : mUser.getPortfolio()) {
                PhotoType type = new PhotoType();
                String photoPath = item;
                String findStr = "http";
                int lastIndex = 0;
                int lastHttpIndex = 0;
                int count = 0;

                while(lastIndex != -1){

                    lastIndex = photoPath.indexOf(findStr,lastIndex);

                    if(lastIndex != -1){
                        count ++;
                        lastIndex += findStr.length();
                        lastHttpIndex = lastIndex;
                    }
                }
                if (count > 1){
                    photoPath = photoPath.substring(lastHttpIndex - 4);
                }
                type.path = photoPath;
                photoList.add(type);
            }
        }

        if(mUser.getUserPic() != null && mUser.getUserPic().trim().length() > 0) {
            if (!mUser.getUserPic().contains("http")) {
                mUser.setUserPic(UrlInfo.URL_HEADER + UrlInfo.API_HEADER + "data/profile/" + mUser.getUserPic());
            }
            Glide.with(this).load(mUser.getUserPic())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(picIv);
        }

        mSelectPhotoManager = new SelectPhotoManager(this, picIv, new SelectPhotoManager.SelectPhotoListner() {
            @Override
            public void selectPhoto(Bitmap bitmap) {
                selectBitmap = bitmap;
            }
        });
        mSelectPhotoManager.from_where = "profile";

        Button locBt = (Button)findViewById(R.id.loc_bt);
        locBt.setOnClickListener(this);

        setSelectArea(mUser.getArea(), mUser.getSubArea());
        setSelectArea(mUser.getArea2(), mUser.getSubArea2());
        setSelectArea(mUser.getArea3(), mUser.getSubArea3());

        for(int i=0; i<Const.LOCAITON_LIMIT; i++){
            locationLayout[i] = new LocationLayout();
            int resId = getResources().getIdentifier("loc_ll" + i, "id", getPackageName());
            locationLayout[i].locLl = (LinearLayout)findViewById(resId);
            resId = getResources().getIdentifier("loc_tv" + i, "id", getPackageName());
            locationLayout[i].locTv = (TextView)findViewById(resId);
            resId = getResources().getIdentifier("loc_iv" + i, "id", getPackageName());
            locationLayout[i].locIv = (ImageView)findViewById(resId);

            final int position = i;

            locationLayout[i].locIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectLocList.remove(position);
                    setLocLayout();
                }
            });
        }

        setLocLayout();

        LinearLayout passLl = (LinearLayout)findViewById(R.id.pass_ll);
        if(mUser.getUserType() == 2)passLl.setVisibility(View.GONE);
        passEt = (EditText)findViewById(R.id.new_pass_et); //changed by RGJ 2017.02.03
        passConfirmEt = (EditText)findViewById(R.id.confirm_et);

        adapter = new PhotoAdapter(this);
        HorizontalListView hlv = (HorizontalListView) findViewById(R.id.hlv);
        hlv.setAdapter(adapter);
    }


    private void setSelectArea(int areaInt, int subAreaInt){

        if(areaInt > 0 && subAreaInt > 0){
            DataBaseManager db = new DataBaseManager(this);
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("id", areaInt);
            Area area = db.getArea(map);
            map.put("id", subAreaInt);
            SubArea subArea = db.getSubArea(map);

            SelectArea item = new SelectArea();
            item.area = area.getId();
            item.subArea = subArea.getId();
            item.selectStr = area.getName() + ">" + subArea.getName();
            selectLocList.add(item);
        }
    }

    private void setLocLayout(){
        for(int i=0; i<Const.LOCAITON_LIMIT; i++){
            if(i < selectLocList.size()){
                DebugUtils.setLog(TAG, "i : " + i);
                locationLayout[i].locLl.setVisibility(View.VISIBLE);
                locationLayout[i].locTv.setText(selectLocList.get(i).selectStr);
            }else{
                locationLayout[i].locLl.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        if (requestCode == SelectPhotoManager.MY_CAMERA_REQUEST_CODE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(this, "기기에 대한 접근이 허용되었습니다.", Toast.LENGTH_LONG).show();
                if (isPortfolio) mSelectPhotoManagerForPotfolio.doTakePhotoAction();
                else mSelectPhotoManager.doTakePhotoAction();

            } else {

                Toast.makeText(this, "기기에 접근을 제한하면 앱이 비정상으로 동작할수도 있습니다.", Toast.LENGTH_LONG).show();

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SelectPhotoManager.CROP_FROM_CAMERA
                || requestCode == SelectPhotoManager.PICK_FROM_CAMERA
                || requestCode == SelectPhotoManager.PICK_FROM_ALBUM) {
            if (isPortfolio) mSelectPhotoManagerForPotfolio.onActivityResult(requestCode, resultCode, data);
            else mSelectPhotoManager.onActivityResult(requestCode, resultCode, data);
        }

        if(requestCode == SelectPhotoManager.CROP_FROM_CAMERA) {
            if (isPortfolio) {
                Bitmap bitmap = data.getExtras().getParcelable("data");
                if (photoList.size() <= PHOTO_MAX) {
                    PhotoType photoType = new PhotoType();
                    photoType.bitmap = bitmap;
                    if (mPosition < photoList.size()) photoList.remove(mPosition);
                    photoList.add(mPosition, photoType);
                }
                adapter.notifyDataSetChanged();
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                if (!resultUri.equals("") || resultUri != null){
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap photo = BitmapFactory.decodeFile(resultUri.getPath(), options);
                    if (isPortfolio) {
                        if (photoList.size() <= PHOTO_MAX) {
                            PhotoType photoType = new PhotoType();
                            photoType.bitmap = photo;
                            if (mPosition < photoList.size()) photoList.remove(mPosition);
                            photoList.add(mPosition, photoType);
                        }
                        adapter.notifyDataSetChanged();
                    }
                    else{
                        mSelectPhotoManager.onActivityResult(requestCode, resultCode, data);
                    }
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                return;
            }
        }

        if(requestCode == SELECT_LOC_REQUEST_CODE){
            if(resultCode == RESULT_OK){
                SelectArea item = new SelectArea();
                item.area = data.getIntExtra("area", 0);
                item.subArea = data.getIntExtra("subarea", 0);
                item.selectStr = data.getStringExtra("str");
                selectLocList.add(item);
                setLocLayout();

                // added by Adonis 2017.07.04
                if(mUser.getUserPic() != null && mUser.getUserPic().trim().length() > 0) {
                    if (!mUser.getUserPic().contains("http")) {
                        mUser.setUserPic(UrlInfo.URL_HEADER + UrlInfo.API_HEADER + "data/profile/" + mUser.getUserPic());
                    }
                    Glide.with(this).load(mUser.getUserPic())
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(picIv);
                }

            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.loc_bt:
                if(selectLocList.size() < Const.LOCAITON_LIMIT){
                    startActivityForResult(new Intent(this, SelectLocationActivity.class), SELECT_LOC_REQUEST_CODE);
                }else{
                    setToast("지역은 최대 3까지 등록 가능합니다.");
                }
                break;

            case R.id.pic_iv:
                mSelectPhotoManager.from_where = "profile";
                isPortfolio = false;
                new RowTwoButtonDialog(this, "사진 가져오기", "앨범에서 가져오기", "사진 촬영하기", choiceListner).show();
                break;

            case R.id.right_tv:
                new TwoButtonDialog(this, "프로필 수정 완료", "작성하신 내용으로\n프로필을 등록 하시겠어요?", "확인", "취소", new TwoButtonDialog.ChoiceListner() {
                    @Override
                    public void confirm() {
                        check();
                    }

                    @Override
                    public void cancle() {
                    }
                }).show();
                break;
            case R.id.check_bt:
                nickCheck();
                break;
            case R.id.bt12:
                new TwoButtonDialog(this, "회원탈퇴", "고객님의 모든 자료가 삭제되고 복구할수 없습니다. 정말 탈퇴하시겠습니까?", "예", "아니", new TwoButtonDialog.ChoiceListner() {
                    @Override
                    public void confirm() {
                        deleteMyAccount();
                    }
                    @Override
                    public void cancle() {}
                }).show();
                break;
        }
    }

    //added by Adonis 2017.07.03
    private void deleteMyAccount(){
        pm.setIsLogin(false);
        pm.setIsMemoPush(false);
        pm.setIsPush(false);
        if(pm.getKakaoId() != null){
            UserManagement.requestLogout(new LogoutResponseCallback() {
                @Override
                public void onCompleteLogout() {
                }
            });
            UserManagement.requestUnlink(new UnLinkResponseCallback() {
                @Override
                public void onFailure(ErrorResult errorResult) {

                }

                @Override
                public void onSessionClosed(ErrorResult errorResult) {

                }

                @Override
                public void onNotSignedUp() {

                }

                @Override
                public void onSuccess(Long userId) {

                }
            });
        }
        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("user_no", mUser.getUserNo());
        new HttpsManager(this).deleteProfile(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                if(file != null && file.exists()) file.delete();
                //selectBitmap = null;
                ActivityManager.allActivityFinish();
                //setResult(RESULT_OK);
                finish();
                startActivity(new Intent(ModifyProfileActivity.this, LoginActivity.class));
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }
    /////////////


    private void nickCheck(){

        if(nickEt.getText().toString().length() == 0){
            setToast("닉네임을 넣어주세요.");
            return;
        }


        if(!networkCheck())return;

        RequestParams params = new RequestParams();
        params.put("nick", nickEt.getText().toString());

        new HttpsManager(this).nickCheck(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                setToast((String) result);
                isNickCheck = true;
                checkNickStr = nickEt.getText().toString();
            }

            @Override
            public void onFailure(String errorStr) {
                isNickCheck = false;
                setToast(errorStr);
            }
        });
    }

    private void check(){
        String nickStr = nickEt.getText().toString();
        if(nickStr.trim().length() == 0){
            setToast("닉네임을 입력해주세요.");
            return;
        }

        if(nickStr.trim().length() < 2 || nickStr.trim().length() > 8){
            setToast("닉네임은 2자 이상 최대 8자 이내입니다.");
            return;
        }

        if(phoneEt.getText().toString().length() != 11){
            setToast("전화번호는 11자리입니다.");
            return;
        }



/*        if(snsEt.getText().toString().trim().length() == 0){
            setToast("카카오톡 아이디를 입력해주세요");
            return;
        }*/

        if(selectLocList.size() == 0){
            setToast("지역을 선택해주세요.");
            return;
        }

        if(!mUser.getUserNick().equals(nickEt.getText().toString())){
            if(!isNickCheck && !checkNickStr.equals(nickEt.getText().toString())){
                setToast("닉네임 중복확인를 해주세요.");
                return;
            }
        }

        if(mUser.getUserType() == 1){
            if(passEt.getText().toString().length() == 0){
                setToast("비밀번호를 넣어주세요.");
                return;
            }

            if(passConfirmEt.getText().toString().length() == 0){
                setToast("비밀번호확인 해주세요");
                return;
            }

            if(!passEt.getText().toString().equals(passConfirmEt.getText().toString())){ // changed by RGJ 2017.02.04
                setToast("비밀번호가 맞지 않습니다.");
                return;
            }

/*            if(overviewEt.getText().toString().trim().length() == 0){
                setToast("소개글을 넣어주세요.");
                return;
            }*/
        }

        modifyPortfolio();
    }

    private void modifyProfile(){
        if(!networkCheck()) return;

        DialogView.progressDialogShow(this);

        RequestParams params = new RequestParams();
        params.put("user_no", mUser.getUserNo());
        params.put("user_type", mUser.getUserType());

        if(selectBitmap != null){
            FileOutputStream fos = null;

            String imagePath =  Environment.getExternalStorageDirectory() + "/" + System.currentTimeMillis() + ".jpg";
            try {

                fos = new FileOutputStream(imagePath);
                selectBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                file = new File(imagePath);
                params.put("user_img", file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }finally {
                if(fos != null)
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }
        }

        params.put("user_id", mUser.getUserId());
        params.put("user_nick", nickEt.getText().toString());
        // if(mUser.getUserType() == 1)params.put("user_pass", mUser.getPassword()); //commented by RGJ 2017.02.04
        if(mUser.getUserType() == 1) params.put("user_pass", passEt.getText().toString()); //added by RGj 2017.02.04
        params.put("user_phone", phoneEt.getText().toString());
        for(int i=0; i<selectLocList.size(); i++){
            if(i == 0){
                params.put("user_area", selectLocList.get(i).area);
                params.put("user_sub_area", selectLocList.get(i).subArea);
            }else{
                params.put("user_area" + (i+1), selectLocList.get(i).area);
                params.put("user_sub_area" + (i+1), selectLocList.get(i).subArea);
            }
        }

        params.put("user_sns_id", snsEt.getText().toString());
        params.put("user_overview", overviewEt.getText().toString());//added by Adonis 2017.07.07
        params.put("user_status", "1");
        new HttpsManager(this).modifyProfile(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                if(file != null && file.exists()) file.delete();
                //selectBitmap = null;
                User newUser = ((User) result);
                if (newUser.getUserType() == 1){
                    newUser.setPassword(passEt.getText().toString());
                }
                pm.setUser(newUser);

                mUser = pm.getUser(); // added by Adonis 2017.07.04
                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }

    public void modifyPortfolio(){
        if(!networkCheck()) return;

        DialogView.progressDialogShow(this);

        RequestParams params = new RequestParams();
        params.put("user_no", mUser.getUserNo());

        if  (photoList.size() > 0){
            for(int i=0; i<photoList.size(); i++){
                if(photoList.get(i) != null){
                    if(photoList.get(i).bitmap != null){
                        FileOutputStream fos = null;
                        String imagePath =  Environment.getExternalStorageDirectory() + "/" + System.currentTimeMillis() + ".jpg";
                        try {
                            fos = new FileOutputStream(imagePath);
                            photoList.get(i).bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                            File file = new File(imagePath);
                            params.put("img" + (i+1), file);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }finally {
                            if(fos != null)
                                try {
                                    fos.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                        }
                    }else{
                        String photoPath = photoList.get(i).path.substring(photoList.get(i).path.indexOf("profile") + 8);
                        params.put("img" + (i+1), photoPath);
                    }
                }
            }
        }
        else{
            for( int i = 0; i < 5; i++){
                params.put("img" + (i+1), "");
            }
        }


        new HttpsManager(this).modifyProfilePortfolio(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                if(file != null && file.exists()) file.delete();
//                pm.setUser((User)result);
//                mUser = pm.getUser(); // added by Adonis 2017.07.04
                modifyProfile();
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }

    private RowTwoButtonDialog.ChoiceListner choiceListner = new RowTwoButtonDialog.ChoiceListner() {
        @Override
        public void firstClick() {
            mSelectPhotoManager.doTakeAlbumAction();
        }

        @Override
        public void secondClick() {
            mSelectPhotoManager.doTakePhotoAction();
        }
    };

    @Override
    public void onBack(View v) {
        cancleModify();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            cancleModify();
            return true;
        }else{
            return super.onKeyDown(keyCode, event);
        }
    }

    private void cancleModify(){
        new TwoButtonDialog(this, "프로필 수정 취소", "프로필 수정을\n취소하시겠습니까?", "확인", "취소", new TwoButtonDialog.ChoiceListner() {
            @Override
            public void confirm() {
                finish();
            }
            @Override
            public void cancle() {
            }
        }).show();
    }

    class PhotoType{
        Bitmap bitmap;
        String path;
    }

    class PhotoAdapter extends CommonBaseAdapter {

        private final String TAG = getClass().getSimpleName();
        private Context mContext;
        private LayoutInflater inflater;

        public PhotoAdapter(Context context) {
            mContext = context;
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return PHOTO_MAX;
        }

        @Override
        public Object getItem(int position) {
            return photoList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View v, ViewGroup parent) {
            if (v == null) v = inflater.inflate(R.layout.item_photo, null);
            RelativeLayout rl = ViewHolder.get(v, R.id.rl);

            if (photoList.size() >= position) {
                rl.setVisibility(View.VISIBLE);
                ImageView photoIv = ViewHolder.get(v, R.id.photo_iv);
                ImageView closeIv = ViewHolder.get(v, R.id.close_iv);
                photoIv.setVisibility(View.VISIBLE);
                mSelectPhotoManagerForPotfolio = new SelectPhotoManager(mContext);
                mSelectPhotoManagerForPotfolio.from_where = "can";
                photoIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mPosition = position;
                        isPortfolio = true;
                        new RowTwoButtonDialog(mContext, "사진 가져오기", "앨범에서 가져오기", "사진 촬영하기", choiceListner).show();
                    }
                });
                if (photoList.size() > position) {
                    closeIv.setVisibility(View.VISIBLE);
                    PhotoType item = photoList.get(position);
                    if(item.bitmap != null)photoIv.setImageBitmap(item.bitmap);
                    else{
                        Glide.with(mContext)
                                .load(item.path)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(photoIv);
                    }
                    closeIv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            photoList.remove(position);
                            notifyDataSetChanged();
                        }
                    });
                } else {
                    closeIv.setVisibility(View.GONE);
                }
            } else {
                rl.setVisibility(View.GONE);
            }

            return v;
        }

        private RowTwoButtonDialog.ChoiceListner choiceListner = new RowTwoButtonDialog.ChoiceListner() {

            @Override
            public void firstClick() {
                mSelectPhotoManagerForPotfolio.doTakeAlbumAction();
            }

            @Override
            public void secondClick() {
                mSelectPhotoManagerForPotfolio.doTakePhotoAction();
            }
        };

    }
}
