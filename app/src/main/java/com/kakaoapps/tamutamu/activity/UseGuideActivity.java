package com.kakaoapps.tamutamu.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;

/**
 * Created by developer on 2016-06-30.
 */
public class UseGuideActivity extends CommonActivity implements View.OnClickListener{

    private TextView canTv, needTv;
    private ImageView iv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_useguide);
        init();
    }

    private void init(){

        setTitlebar();
        titlebar.backIv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText("이용가이드");
        titlebar.centerTv.setVisibility(View.VISIBLE);

        canTv = (TextView)findViewById(R.id.can_tv);
        needTv = (TextView)findViewById(R.id.need_tv);
        canTv.setText("구매자 가이드");
        needTv.setText("판매자 가이드");
        canTv.setOnClickListener(this);
        needTv.setOnClickListener(this);

        iv = (ImageView)findViewById(R.id.iv);
    }

    private void setTab(int resId){
        if(R.id.can_tv == resId){
            canTv.setTextColor(getResources().getColor(R.color.purple));
            canTv.setBackgroundColor(Color.WHITE);
            needTv.setTextColor(getResources().getColor(R.color.gray));
            needTv.setBackgroundResource(R.drawable.gradient_round);
            iv.setImageResource(R.drawable.guide_buyer);
        }else{
            canTv.setTextColor(getResources().getColor(R.color.gray));
            canTv.setBackgroundResource(R.drawable.gradient_round);
            needTv.setTextColor(getResources().getColor(R.color.purple));
            needTv.setBackgroundColor(Color.WHITE);
            iv.setImageResource(R.drawable.guide_seller);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.can_tv:
                setTab(R.id.can_tv);
                break;
            case R.id.need_tv:
                setTab(R.id.need_tv);
                break;
        }
    }
}
