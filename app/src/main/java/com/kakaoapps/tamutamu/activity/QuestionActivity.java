package com.kakaoapps.tamutamu.activity;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.github.angads25.filepicker.controller.DialogSelectionListener;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.adapter.QuestionAdapter;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.dialog.RowThreeButtonDialog;
import com.kakaoapps.tamutamu.dialog.RowTwoButtonDialog;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.manager.SelectPhotoManager;
import com.kakaoapps.tamutamu.model.Question;
import com.kakaoapps.tamutamu.utils.DebugUtils;
import com.loopj.android.http.RequestParams;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by developer on 2016-06-28.
 */
public class QuestionActivity extends CommonActivity implements View.OnClickListener{
    private final String TAG = getClass().getSimpleName();
    private final int REQUEST_FILE = 1001;
    public static final String ACTION_RECEIVE_PUSH = "ACTION.receiver_push"; // added by RGJ 2017.02.03

    private ArrayList<Question> list;
    private QuestionAdapter adapter;
    private ListView lv;
    private EditText writeEt;
    private int totalCount = 0;

    private int partnerNo;
    private int page = 1;

    private boolean isSending = false;
    private boolean isRunning = false;

    private SelectPhotoManager mSelectPhotoManager;
    FilePickerDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

/*
        if(savedInstanceState == null)
            partnerNo = getIntent().getIntExtra("partner_no",0);
        else*/
        partnerNo = getIntent().getIntExtra("partner_no", 0);
        String partnerId = getIntent().getStringExtra("partner_id");

        mSelectPhotoManager = new SelectPhotoManager(this);
        mSelectPhotoManager.from_where = "question";
        setTitlebar();
        titlebar.centerTv.setText(getIntent().getStringExtra("partner_id"));
        titlebar.centerTv.setVisibility(View.VISIBLE);
        titlebar.backIv.setVisibility(View.VISIBLE);

        lv = (ListView)findViewById(R.id.lv);
        lv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(list != null && DialogView.mDialog == null && totalCount > list.size() && lv.getFirstVisiblePosition() == 0){
                    DebugUtils.setLog(TAG, "firstVisiblePosition" + lv.getFirstVisiblePosition());
                    ++page;
                    getList();
                }
            }
        });

        ImageView picIv = (ImageView)findViewById(R.id.pic_iv);
        picIv.setOnClickListener(this);
        writeEt = (EditText)findViewById(R.id.write_et);


        DialogProperties properties = new DialogProperties();
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.selection_type = DialogConfigs.FILE_SELECT;
        properties.root = new File(DialogConfigs.DEFAULT_DIR);
        properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
        properties.offset = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = null;
        dialog = new FilePickerDialog(this,properties);
        dialog.setTitle("파일을 선택하세요.");
        dialog.setDialogSelectionListener(new DialogSelectionListener() {
            @Override
            public void onSelectedFilePaths(String[] files) {
                for(String path:files) {
                    File file = new File(path);
                    send(file);
                    break;
                }
            }
        });

        findViewById(R.id.send_bt).setOnClickListener(this);
        getList();

        // added by RGJ 2017.02.03
        IntentFilter filter = new IntentFilter(QuestionActivity.ACTION_RECEIVE_PUSH);
        LocalBroadcastManager.getInstance(this).registerReceiver( mHandleMessageDelivery, filter);
        // added end
        isRunning = true;
    }

    private void getList(){
        if(!networkCheck()) return;

        DialogView.progressDialogShow(this);

        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        params.put("user_id", pm.getUser().getUserId());
        params.put("g_user_no", partnerNo);
        params.put("start", page);


        new HttpsManager(this).getQuestList(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                isSending = false;
                DialogView.progressDialogClose();
                if(page == 1){
                    list = (ArrayList<Question>)result;
                    if(list.size() > 0)totalCount = list.get(0).totalCount;
                    adapter = new QuestionAdapter(QuestionActivity.this, list);
                    lv.setAdapter(adapter);

                }else{
                    ArrayList<Question> newList = (ArrayList<Question>)result;
                    newList.addAll(list);
                    list = newList;
                    adapter = new QuestionAdapter(QuestionActivity.this, list);
                    lv.setAdapter(adapter);
                }

                if(page < 3){
                    lv.setSelection(list.size() - 1);
                    lv.smoothScrollToPosition(list.size() - 1);
                }
            }

            @Override
            public void onFailure(String errorStr) {
                isSending = false;
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }

    private void send(final File file){
        if(!networkCheck()) return;
        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        params.put("user_id", pm.getUser().getUserId());
        params.put("g_user_no", partnerNo);

        if(file  != null){
            try {
                params.put("upfile", file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }else{
            if(writeEt.getText().toString().trim().length() > 0){
                params.put("content", writeEt.getText().toString());
            }else{
                setToast("내용을 입력해주세요.");
                writeEt.requestFocus();
                return;
            }
        }

        new HttpsManager(this).registMemo(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                DebugUtils.setLog(TAG, "onSuccess called!!!");
                writeEt.setText("");
                page = 1;
                getList();
                lv.smoothScrollToPosition(adapter.getCount() - 1);
                lv.setSelection(adapter.getCount() - 1);

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(writeEt.getWindowToken(), 0);
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.pic_iv:
                new RowThreeButtonDialog(this, "사진 가져오기", "앨범에서 가져오기", "사진 촬영하기", "파일 첨부하기", choiceListner).show();
                break;
            case R.id.send_bt:
                if(!isSending){
                    isSending = true;
                    send(null);
                }
                break;
        }
    }

    private RowThreeButtonDialog.ChoiceListner choiceListner = new RowThreeButtonDialog.ChoiceListner() {

        @Override
        public void firstClick() {
            mSelectPhotoManager.doTakeAlbumAction();
        }

        @Override
        public void secondClick() {
            mSelectPhotoManager.doTakePhotoAction();
        }

        @Override
        public void thirdClick() {
            dialog.show();
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        if (requestCode == SelectPhotoManager.MY_CAMERA_REQUEST_CODE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(this, "기기에 대한 접근이 허용되었습니다.", Toast.LENGTH_LONG).show();

            } else {

                Toast.makeText(this, "기기에 접근을 제한하면 앱이 비정상으로 동작할수도 있습니다.", Toast.LENGTH_LONG).show();

            }
        }
    }

    @Override
    public void onBack(View v) {
        isRunning = false;
        onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SelectPhotoManager.CROP_FROM_CAMERA
                || requestCode == SelectPhotoManager.PICK_FROM_CAMERA
                || requestCode == SelectPhotoManager.PICK_FROM_ALBUM) {
            mSelectPhotoManager.onActivityResult(requestCode, resultCode, data);
        }

        if(requestCode == SelectPhotoManager.CROP_FROM_CAMERA){
            Bitmap bitmap = data.getExtras().getParcelable("data");
            String imagePath =  Environment.getExternalStorageDirectory() + "/" + System.currentTimeMillis() + ".jpg";
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(imagePath);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                File file = new File(imagePath);
                send(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }finally {
                if(fos != null)
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                if (!resultUri.equals("") || resultUri != null){
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap photo = BitmapFactory.decodeFile(resultUri.getPath(), options);
                    String imagePath =  Environment.getExternalStorageDirectory() + "/" + System.currentTimeMillis() + ".jpg";
                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream(imagePath);
                        photo.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                        File file = new File(imagePath);
                        send(file);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }finally {
                        if(fos != null)
                            try {
                                fos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                    }
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                return;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
    // added by RGJ 2017.02.03
    private final BroadcastReceiver mHandleMessageDelivery = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (isRunning) {
                Log.i("TAG", "N Push");
                page = 1;
                getList();
                lv.smoothScrollToPosition(adapter.getCount() - 1);
                lv.setSelection(adapter.getCount() - 1);
            }
        }
    };

}
