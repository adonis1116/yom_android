package com.kakaoapps.tamutamu.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.loopj.android.http.RequestParams;

/**
 * Created by john on 2016-02-12.
 */
public class ProvisionActivity extends CommonActivity implements View.OnClickListener{

    private TextView tv1, tv2, provisionTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provision);

        int type = getIntent().getIntExtra("type", 1);

        setTitlebar();
        titlebar.centerTv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText("약관");
        titlebar.backIv.setVisibility(View.VISIBLE);

        tv1 = (TextView)findViewById(R.id.provision_tv1);
        tv2 = (TextView)findViewById(R.id.provision_tv2);
        provisionTv = (TextView)findViewById(R.id.provision_tv);

        tv1.setOnClickListener(this);
        tv2.setOnClickListener(this);

        getProvision(type);

    }

    @Override
    public void onClick(View v) {
        int type = 1;
        switch (v.getId()){
            case R.id.provision_tv1:
                tv1.setTextColor(getResources().getColor(R.color.purple));
                tv2.setTextColor(getResources().getColor(R.color.gray));
                type = 1;
                break;
            case R.id.provision_tv2:
                tv1.setTextColor(getResources().getColor(R.color.gray));
                tv2.setTextColor(getResources().getColor(R.color.purple));
                type = 2;
                break;
        }

        getProvision(type);
    }

    private void getProvision(int type){
        if(!networkCheck())return;

        DialogView.progressDialogShow(this);

        RequestParams params = new RequestParams();
        params.put("text_type", type == 1 ? "use_info" : "pr_info");

        new HttpsManager(this).getProvision(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                String str = (String)result;
                provisionTv.setText(str);
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });



    }
}
