package com.kakaoapps.tamutamu.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.adapter.UseHowAdapter;

/**
 * Created by developer on 2016-06-30.
 */
public class UseHowActivity extends CommonActivity {
    private final int PAGE_MAX = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usehow);

        setTitlebar();
        titlebar.backIv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText("YOM 소개");
        titlebar.centerTv.setVisibility(View.VISIBLE);

        ViewPager vp = (ViewPager)findViewById(R.id.vp);
        UseHowAdapter adapter = new UseHowAdapter(this);
        vp.setAdapter(adapter);

        LinearLayout dotLl = (LinearLayout)findViewById(R.id.dot_ll);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, 20 ,0);

        final ImageView dotIvs[] = new ImageView[adapter.getCount()];
        for(int i=0; i<dotIvs.length; i++){
            dotIvs[i] = new ImageView(this);
            dotIvs[i].setLayoutParams(params);
            dotIvs[i].setImageResource(R.drawable.icon_circle_off);
            if(i==0)dotIvs[i].setImageResource(R.drawable.icon_circle_on);
            dotLl.addView(dotIvs[i]);
        }
        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for(int i=0; i<dotIvs.length; i++){
                    if(i != position) dotIvs[i].setImageResource(R.drawable.icon_circle_off);
                    else dotIvs[position].setImageResource(R.drawable.icon_circle_on);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}
