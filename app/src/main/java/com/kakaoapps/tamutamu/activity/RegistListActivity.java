package com.kakaoapps.tamutamu.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.adapter.CanProgressListAdapter;
import com.kakaoapps.tamutamu.adapter.NeedProgressListAdapter;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.model.Process;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;

/**
 * Created by john on 2016-03-04.
 */
public class RegistListActivity extends CommonActivity {
    private ListView lv;
    private boolean isCan;
    private int page = 1;
    private int ROW_LIMIT = 10;


    private ArrayList<Process> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regist_list);

        setTitlebar();
        titlebar.backIv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText("진행상황");

        lv = (ListView) findViewById(R.id.lv);
        lv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (list.size() > 0 && lv.getLastVisiblePosition() >= totalItemCount - 1 && page * ROW_LIMIT < list.get(0).getTotal()) {
                    if (DialogView.mDialog == null) {
                        ++page;
                        if (isCan) getCanList();
                        else getNeedList();
                    }
                }
            }
        });

        isCan = getIntent().getBooleanExtra("is_can", true);
        if(isCan)getCanList();
        else getNeedList();

    }

    private void getCanList(){
        if(!networkCheck())return;
        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        params.put("idx", getIntent().getIntExtra("idx", 0));
        params.put("start", page);
        params.put("pgNum", ROW_LIMIT);
        new HttpsManager(this).canRegistList(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                if(page == 1){
                    list = (ArrayList<Process>) result;
                    CanProgressListAdapter adapter = new CanProgressListAdapter(RegistListActivity.this, list);
                    lv.setAdapter(adapter);
                }else{
                    list.addAll((ArrayList<Process>) result);
                    CanProgressListAdapter adapter = (CanProgressListAdapter)lv.getAdapter();
                    adapter.notifyDataSetChanged();
                }
                if ( list.size() > 0 ){
                    TextView noneTv = (TextView) findViewById(R.id.tvContentNone);
                    noneTv.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }

    private void getNeedList(){
        if(!networkCheck())return;
        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        params.put("idx", getIntent().getIntExtra("idx", 0));
        params.put("start", page);
        params.put("pgNum", ROW_LIMIT);
        new HttpsManager(this).needRegistList(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                if(page == 1){
                    list = (ArrayList<Process>) result;
                    NeedProgressListAdapter adapter = new NeedProgressListAdapter(RegistListActivity.this, list);
                    lv.setAdapter(adapter);
                }else{
                    list.addAll((ArrayList<Process>) result);
                    NeedProgressListAdapter adapter = (NeedProgressListAdapter)lv.getAdapter();
                    adapter.notifyDataSetChanged();
                }
                if ( list.size() > 0 ){
                    TextView noneTv = (TextView) findViewById(R.id.tvContentNone);
                    noneTv.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }
}
