package com.kakaoapps.tamutamu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.adapter.AreaAdapter;
import com.kakaoapps.tamutamu.adapter.SubAreaAdapter;
import com.kakaoapps.tamutamu.manager.DataBaseManager;
import com.kakaoapps.tamutamu.model.Area;
import com.kakaoapps.tamutamu.model.SubArea;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by john on 2016-02-03.
 */
public class SelectLocationActivity extends CommonActivity {

    private ListView lv;
    private DataBaseManager db;
    private AreaAdapter areaAdapter;

    private Area mArea;
    private ImageView closeIv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_loc);

        setTitlebar();
        titlebar.backIv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText("지역선택");

        lv = (ListView)findViewById(R.id.lv);
        lv.setFocusable(false);
        db = new DataBaseManager(this);
        ArrayList<Area> list = db.getAreaList(null);

        areaAdapter = new AreaAdapter(this, list, areaListner);

        lv.setAdapter(areaAdapter);
        closeIv = (ImageView)findViewById(R.id.close_iv);
        closeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    private AreaAdapter.AreaListner areaListner = new AreaAdapter.AreaListner() {
        @Override
        public void select(Area area) {
            //closeIv.setVisibility(View.VISIBLE);
            HashMap<String, Object> map = new HashMap<>();
            map.put("area_id", area.getId());
            ArrayList<SubArea> subList = db.getSubAreaList(map);
            SubAreaAdapter subAreaAdapter = new SubAreaAdapter(SelectLocationActivity.this, subList, subAreaListner);
            lv.setAdapter(subAreaAdapter);
            mArea = area;
        }
    };

    private SubAreaAdapter.SubAreaListner subAreaListner = new SubAreaAdapter.SubAreaListner() {
        @Override
        public void select(SubArea subItem) {
            Intent intent = new Intent();

            String str = mArea.getName() + ">" + subItem.getName();
            intent.putExtra("area", mArea.getId());
            intent.putExtra("subarea", subItem.getId());
            intent.putExtra("lat", subItem.getLat());
            intent.putExtra("lng", subItem.getLng());
            intent.putExtra("str", str);
            setResult(RESULT_OK, intent);
            finish();
        }

        @Override
        public void onBack() {

            lv.setAdapter(areaAdapter);
        }
    };
}
