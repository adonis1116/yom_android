package com.kakaoapps.tamutamu.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kakaoapps.tamutamu.Const;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.dialog.OneButtonDialog;
import com.kakaoapps.tamutamu.dialog.RowTwoButtonDialog;
import com.kakaoapps.tamutamu.manager.ActivityManager;
import com.kakaoapps.tamutamu.manager.AppManager;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.manager.SelectPhotoManager;
import com.kakaoapps.tamutamu.model.User;
import com.kakaoapps.tamutamu.utils.DebugUtils;
import com.loopj.android.http.RequestParams;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;


/**
 * Created by john on 2016-02-02.
 */
public class SignupProfileActivity extends CommonActivity implements View.OnClickListener{
    private final String TAG = getClass().getSimpleName();

    private final int SELECT_LOC_REQUEST_CODE = 111;


    private EditText nickEt, phoneEt, kakaoEt, certiEt;

    private CheckBox cb1, cb2;
    private ImageView picIv;

    private SelectPhotoManager mSelectPhotoManager;

    private User user;
    private boolean fromKakao;

    private boolean isNickCheck;
    private String checkNickStr = "";

    private File file;
    private Bitmap selectBitmap;

    private LinearLayout certiLl;
    private String smsCode = null;
    private boolean isCerti = false;
    private Button sendBt;
    private String sendMsg = "회원님의 핸드폰으로\n인증번호가 발송 되었습니다.";

    private ArrayList<SelectArea> selectLocList = new ArrayList<SelectArea>();

    class LocationLayout{
        LinearLayout locLl;
        TextView locTv;
        ImageView locIv;
    }

    class SelectArea{
        int area;
        int subArea;
        String selectStr;
    }

    private LocationLayout locationLayout[] = new LocationLayout[Const.LOCAITON_LIMIT];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_profile);
        user = getIntent().getParcelableExtra("user");
        fromKakao = getIntent().getBooleanExtra("fromKakao", false);
        init();

    }

    private void init(){
        setTitlebar();
        titlebar.centerTv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText("회원가입");

        picIv = (ImageView)findViewById(R.id.pic_iv);
        picIv.setOnClickListener(this);

        Button locBt = (Button)findViewById(R.id.loc_bt);
        locBt.setOnClickListener(this);

        mSelectPhotoManager = new SelectPhotoManager(this, picIv, new SelectPhotoManager.SelectPhotoListner() {
            @Override
            public void selectPhoto(Bitmap bitmap) {
                selectBitmap = bitmap;
            }
        });
        mSelectPhotoManager.from_where = "signup";


        nickEt = (EditText)findViewById(R.id.nick_et);
        phoneEt = (EditText)findViewById(R.id.phone_et);
        kakaoEt = (EditText)findViewById(R.id.kakao_et);

        Button nickCheckBt = (Button)findViewById(R.id.check_bt);
        nickCheckBt.setOnClickListener(this);


        for(int i=0; i<Const.LOCAITON_LIMIT; i++){
            locationLayout[i] = new LocationLayout();
            int resId = getResources().getIdentifier("loc_ll" + i, "id", getPackageName());
            locationLayout[i].locLl = (LinearLayout)findViewById(resId);
            resId = getResources().getIdentifier("loc_tv" + i, "id", getPackageName());
            locationLayout[i].locTv = (TextView)findViewById(resId);
            resId = getResources().getIdentifier("loc_iv" + i, "id", getPackageName());
            locationLayout[i].locIv = (ImageView)findViewById(resId);
            final int position = i;
            locationLayout[i].locIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectLocList.remove(position);
                    setLocLayout();
                }
            });
        }

        if(fromKakao){
            findViewById(R.id.provision_ll).setVisibility(View.VISIBLE);
            cb1 = (CheckBox)findViewById(R.id.cb1);
            cb2 = (CheckBox)findViewById(R.id.cb2);

            Button provisionBt1 = (Button)findViewById(R.id.provision_bt1);
            Button provisionBt2 = (Button)findViewById(R.id.provision_bt2);
            provisionBt1.setOnClickListener(this);
            provisionBt2.setOnClickListener(this);

        }

        sendBt = (Button)findViewById(R.id.send_bt);
        sendBt.setOnClickListener(this);

        certiLl = (LinearLayout)findViewById(R.id.certi_ll);
        certiEt = (EditText)findViewById(R.id.certi_et);
        Button certiBt = (Button)findViewById(R.id.certi_bt);
        certiBt.setOnClickListener(this);
        footer();
    }


    private void footer(){
        TextView tv1 = (TextView)findViewById(R.id.tv1);
        TextView tv2 = (TextView)findViewById(R.id.tv2);

        tv1.setText("이전");
        tv2.setText("완료");
        if(fromKakao)tv1.setVisibility(View.GONE);
        tv1.setOnClickListener(this);
        tv2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()){
            case R.id.pic_iv:
                new RowTwoButtonDialog(this, "사진 가져오기", "앨범에서 가져오기", "사진 촬영하기", choiceListner).show();
                break;
            case R.id.tv1:
                finish();
                break;
            case R.id.tv2:
                check();
                break;
            case R.id.loc_bt:
                if(selectLocList.size() < Const.LOCAITON_LIMIT){
                    startActivityForResult(new Intent(this, SelectLocationActivity.class), SELECT_LOC_REQUEST_CODE);
                }else{
                    setToast("지역은 최대 3까지 등록 가능합니다.");
                }
                break;
            case R.id.check_bt:
                nickCheck();
                break;
            case R.id.provision_bt1:
                intent = new Intent(this, ProvisionActivity.class);
                intent.putExtra("type", 1);
                break;
            case R.id.provision_bt2:
                intent = new Intent(this, ProvisionActivity.class);
                intent.putExtra("type", 2);
                break;
            case R.id.send_bt:
                sendCerti();
                break;
            case R.id.certi_bt:
                doCerti();
                break;
        }
        if(intent != null)startActivity(intent);
    }

    private void sendCerti(){

        if(phoneEt.getText().toString().length() == 0){
            setToast("연락처를 입력해주세요");
            return;
        }

        if(phoneEt.getText().toString().length() != 11){
            setToast("전화번호는 11자리입니다.");
            return;
        }

        RequestParams params = new RequestParams();
        params.put("hp", phoneEt.getText().toString());
        new HttpsManager(this).getSMSCode(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                new OneButtonDialog(SignupProfileActivity.this, "인증번호 발송 안내", sendMsg, "확인", new OneButtonDialog.ChoiceListner() {
                    @Override
                    public void confirm() {

                    }
                }).show();
                isCerti = false;
                certiLl.setVisibility(View.VISIBLE);
                smsCode = (String)result;
                sendBt.setText("재전송");
                sendMsg = "회원님의 핸드폰으로\n인증번호가 재발송 되었습니다.";
            }

            @Override
            public void onFailure(String errorStr) {
                setToast(errorStr);
            }
        });

/*                new OneButtonDialog(SignupProfileActivity.this, "인증번호 발송 안내", sendMsg, "확인", new OneButtonDialog.ChoiceListner() {
                    @Override
                    public void confirm() {

                    }
                }).show();*/

    }

    private void doCerti(){
        if(smsCode != null){
            if(certiEt.getText().toString().length() == 0){
                setToast("인증번호를 입력하세요");
                return;
            }

            if(smsCode.equals(certiEt.getText().toString())){
                setToast("인증에 성공하였습니다.");
                certiLl.setVisibility(View.GONE);
                isCerti = true;
                certiEt.setText("");
            }else{
                setToast("잘못된 인증번호입니다.");
            }
        }

        //TODO:
    }

    private void check(){
        String nickStr = nickEt.getText().toString();
        if(nickStr.trim().length() == 0){
            setToast("닉네임을 입력해주세요.");
            return;
        }

        if(nickStr.trim().length() < 2 || nickStr.trim().length() > 8){
            setToast("닉네임은 2자 이상 최대 8자 이내입니다.");
            return;
        }

        if(phoneEt.getText().toString().length() == 0){
            setToast("연락처를 입력해주세요");
            return;
        }

        if(phoneEt.getText().toString().length() > 11){
            setToast("전화번호는 최대 11자리입니다.");
            return;
        }

/*        if(kakaoEt.getText().toString().trim().length() == 0){
            setToast("카카오톡 아이디를 입력해주세요");
            return;
        }*/

        if(selectLocList.size() == 0){
            setToast("지역을 선택해주세요.");
            return;
        }

/*        if(selectBitmap == null){
            setToast("프로필 사진을 선택해주세요.");
            return;
        }*/

        if(fromKakao){
            if(!cb1.isChecked()){
                setToast("이용약관에 동의해주세요.");
                return;
            }

            if(!cb2.isChecked()){
                setToast("개인정보취급방침에 동의해주세요.");
                return;
            }
        }

        if(!isNickCheck && !checkNickStr.equals(nickEt.getText().toString())){
            setToast("닉네임 중복확인를 해주세요.");
            return;
        }

        if(!isCerti){
            setToast("SMS 인증을 해주시길 바랍니다.");
            //return;
        }
        signup();
    }


    private void nickCheck(){

        if(nickEt.getText().toString().length() == 0){
            setToast("닉네임을 넣어주세요.");
            return;
        }


        if(!networkCheck())return;

        RequestParams params = new RequestParams();
        params.put("nick", nickEt.getText().toString());

        new HttpsManager(this).nickCheck(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                setToast((String) result);
                isNickCheck = true;
                checkNickStr = nickEt.getText().toString();
            }

            @Override
            public void onFailure(String errorStr) {
                isNickCheck = false;
                setToast(errorStr);
            }
        });
    }

    private void signup(){


        if(!networkCheck()) return;

        DialogView.progressDialogShow(this);

        RequestParams params = new RequestParams();
        params.put("user_type", user.getUserType());

        if(selectBitmap != null){
            FileOutputStream fos = null;

            String imagePath =  Environment.getExternalStorageDirectory() + "/" + System.currentTimeMillis() + ".jpg";
            try {

                fos = new FileOutputStream(imagePath);
                selectBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                file = new File(imagePath);
                params.put("user_img", file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }finally {
                if(fos != null)
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }
        }

        params.put("user_id", user.getUserId());
        params.put("user_nick", nickEt.getText().toString());
        if(!fromKakao)params.put("user_pass", user.getPassword());
        params.put("user_phone", phoneEt.getText().toString());

        for(int i=0; i<selectLocList.size(); i++){
            if(i == 0){
                params.put("user_area", selectLocList.get(i).area);
                params.put("user_sub_area", selectLocList.get(i).subArea);
            }else{
                params.put("user_area" + (i+1), selectLocList.get(i).area);
                params.put("user_sub_area" + (i+1), selectLocList.get(i).subArea);
            }

        }

        params.put("user_sns_id", kakaoEt.getText().toString());

        params.put("device_type", 1);
        params.put("device_f_id", AppManager.getDeviceSerialNumber());
        params.put("device_push_token", pm.getPushToken());

        new HttpsManager(this).signup(params, new HttpsManager.HttpListner() {

            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();

                User newUser = ((User) result);
                if (newUser.getUserType() == 1){
                    newUser.setPassword(user.getPassword());
                }
                pm.setUser((User)result);
                getAlarm();
                if(file != null && file.exists()) file.delete();
                selectBitmap = null;
                new OneButtonDialog(SignupProfileActivity.this, "회원가입 확인", getString(R.string.signup_complete), "시작하기", mChoiceListner).show();
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();

                if(file != null && file.exists()) file.delete();
                selectBitmap = null;

                setToast(errorStr);

            }
        });
    }

    private void getAlarm(){
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        new HttpsManager(this).getAlarm(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {

            }

            @Override
            public void onFailure(String errorStr) {

            }
        });
    }

    private OneButtonDialog.ChoiceListner mChoiceListner = new OneButtonDialog.ChoiceListner() {
        @Override
        public void confirm() {
            startActivity(new Intent(SignupProfileActivity.this, MainActivity.class));
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        if (requestCode == SelectPhotoManager.MY_CAMERA_REQUEST_CODE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(this, "기기에 대한 접근이 허용되었습니다.", Toast.LENGTH_LONG).show();

            } else {

                Toast.makeText(this, "기기에 접근을 제한하면 앱이 비정상으로 동작할수도 있습니다.", Toast.LENGTH_LONG).show();

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SelectPhotoManager.CROP_FROM_CAMERA
                || requestCode == SelectPhotoManager.PICK_FROM_CAMERA
                || requestCode == SelectPhotoManager.PICK_FROM_ALBUM) {
            mSelectPhotoManager.onActivityResult(requestCode, resultCode, data);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mSelectPhotoManager.onActivityResult(requestCode, resultCode, data);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                return;
            }
        }

        if(requestCode == SELECT_LOC_REQUEST_CODE){
            if(resultCode == RESULT_OK){
                SelectArea item = new SelectArea();
                item.area = data.getIntExtra("area", 0);
                item.subArea = data.getIntExtra("subarea", 0);
                item.selectStr = data.getStringExtra("str");
                selectLocList.add(item);
                setLocLayout();
            }
        }
    }

    private void setLocLayout(){
        DebugUtils.setLog(TAG, "selectLocList : " + selectLocList.size());
        for(int i=0; i<Const.LOCAITON_LIMIT; i++){
            if(i < selectLocList.size()){
                locationLayout[i].locLl.setVisibility(View.VISIBLE);
                locationLayout[i].locTv.setText(selectLocList.get(i).selectStr);
            }else{
                locationLayout[i].locLl.setVisibility(View.GONE);
            }

        }
    }

    private RowTwoButtonDialog.ChoiceListner choiceListner = new RowTwoButtonDialog.ChoiceListner() {

        @Override
        public void firstClick() {
            mSelectPhotoManager.doTakeAlbumAction();
        }

        @Override
        public void secondClick() {
            mSelectPhotoManager.doTakePhotoAction();
        }
    };
}
