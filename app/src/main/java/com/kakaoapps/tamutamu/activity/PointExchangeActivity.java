// Added by RGJ 2017.01.13
package com.kakaoapps.tamutamu.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kakaoapps.tamutamu.Const;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.adapter.PointChargeHistoryAdapter;
import com.kakaoapps.tamutamu.adapter.PointHistoryAdapter;
import com.kakaoapps.tamutamu.adapter.PointRefundHistoryAdapter;
import com.kakaoapps.tamutamu.adapter.PointUseHistoryAdapter;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.dialog.OneButtonDialog;
import com.kakaoapps.tamutamu.manager.HttpsManager;
import com.kakaoapps.tamutamu.manager.PreferencesManager;
import com.kakaoapps.tamutamu.model.Bank;
import com.kakaoapps.tamutamu.model.PointChargeHistory;
import com.kakaoapps.tamutamu.model.PointHistory;
import com.kakaoapps.tamutamu.model.PointRefundHistory;
import com.kakaoapps.tamutamu.model.PointUseHistory;
import com.loopj.android.http.RequestParams;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class PointExchangeActivity extends CommonActivity implements View.OnClickListener{
    private Long myPoint = 0L;

    private TextView mypointLv; // 현재 포인트 라벨
    private TextView mypointTv; // 현재 포인트
    Bank bank_info; //은행정보

    private TextView chargeTv, useTv, refundTv; //tab page text view
    private ListView chargeLv, useLv, refundLv; //tab page List view
    private View refundHeaderView; //포인트 환급내역 머리정보

    public Button refundBtn;

    PointRefundHistoryAdapter mRefundAdapter;
    PointChargeHistoryAdapter mChargeAdapter; //RGJ
    PointUseHistoryAdapter mUseAdapter; //RGJ
    private ArrayList<PointRefundHistory> mRefundList;
    private ArrayList<PointChargeHistory> mChargeList;
    private ArrayList<PointUseHistory> mUseList;

    // 환급신청정보변수
    private EditText out_point_Et; // 출금포인트
    private EditText refund_master_Et; // 예금주
    private EditText bank_Et; // 은행명
    private EditText account_num_Et; // 계좌번호

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point_exchange);

        init(); //초기화
    }

    private void init(){
        //타이틀바 설정
        setTitlebar();
        titlebar.backIv.setVisibility(View.VISIBLE);
        titlebar.centerTv.setText("포인트 내역/환급신청");
        titlebar.centerTv.setVisibility(View.VISIBLE);

        mypointLv = (TextView)findViewById(R.id.curPoint_lbl_tv); // 내포인트 라벨
        mypointTv = (TextView)findViewById(R.id.curPoint_val_tv); // 내포인트
        // get tab view text box id
        chargeTv = (TextView)findViewById(R.id.charge_point_tv);
        useTv = (TextView)findViewById(R.id.use_point_tv);
        refundTv = (TextView)findViewById(R.id.refund_point_tv);

        // link listener
        chargeTv.setOnClickListener(this);
        useTv.setOnClickListener(this);
        refundTv.setOnClickListener(this);

        chargeLv = (ListView)findViewById(R.id.charge_point_lv);
        useLv = (ListView)findViewById(R.id.use_point_lv);
        refundLv = (ListView)findViewById(R.id.refund_point_lv);

        chargeLv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {}
        });

        if(refundHeaderView == null) setRefundHeader();

        // 환급내역 탭 정보설정
        out_point_Et = (EditText)findViewById(R.id.out_point_et); // 출금포인트
        refund_master_Et = (EditText)findViewById(R.id.refund_master_et); // 예금주
        bank_Et = (EditText)findViewById(R.id.bank_et); // 은행명
        account_num_Et = (EditText)findViewById(R.id.account_et); // 계좌번호

        refundBtn = (Button)findViewById(R.id.refund_point_btn); //환급신청단추
        refundBtn.setOnClickListener(this);

        setTab(R.id.charge_point_tv);
        getMyPoint(); // 내포인트얻기
        getBankNum(); // 은행정보얻기
    }

    // 포인트환급내역내용정보추가
    private void setRefundHeader()
    {
        refundHeaderView = View.inflate(this, R.layout.header_refund_point, null);

        refundLv.addHeaderView(refundHeaderView);
    }

    // 내포인트 얻기
    private void getMyPoint(){
        DialogView.progressDialogShow(this); // 기다림 통보
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        new HttpsManager(this).getMyPoint(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                myPoint = Long.valueOf(result.toString());
 //               mypointTv.setText(Integer.toString(myPoint) + "P");
                mypointTv.setText(Const.MONEY_DF.format(Long.valueOf(myPoint)) + "P");
//                mypointTv.setText(NumberFormat.getNumberInstance(Locale.US).format(result) + "P");
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }

    // 포인트충전내역 정보얻기
    private void getPointChargeHistory(){
        DialogView.progressDialogShow(this); // 기다림 통보
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        params.put("user_id", pm.getUser().getUserId());

        new HttpsManager(this).getPointChargeHistory(params, new HttpsManager.HttpListner(){

            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                mChargeList = (ArrayList< PointChargeHistory>)result;
                mChargeAdapter = new PointChargeHistoryAdapter(PointExchangeActivity.this, mChargeList);
                chargeLv.setAdapter(mChargeAdapter);
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }

    // 포인트사용내역 정보얻기
    private void getPointUseHistory(){
        DialogView.progressDialogShow(this); // 기다림 통보
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        params.put("user_id", pm.getUser().getUserId());

        new HttpsManager(this).getPointUseHistory(params, new HttpsManager.HttpListner(){

            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                mUseList = (ArrayList< PointUseHistory>)result;
                mUseAdapter = new PointUseHistoryAdapter(PointExchangeActivity.this, mUseList);
                useLv.setAdapter(mUseAdapter);
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }

    // 환급내역정보 얻기
    private void getRefundPointHistory(){
        DialogView.progressDialogShow(this); // 잠시만 기다려주세요
        RequestParams params = new RequestParams();
        params.put("user_no", pm.getUser().getUserNo());
        params.put("user_id", pm.getUser().getUserId());

        new HttpsManager(this).getPointRefundHistory(params, new HttpsManager.HttpListner(){

            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                mRefundList = (ArrayList<PointRefundHistory>)result;
                mRefundAdapter = new PointRefundHistoryAdapter(PointExchangeActivity.this, mRefundList);
                refundLv.setAdapter(mRefundAdapter);
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }

    //환급신청하기단추누를때
    private void onClickRefundBtn()
    {
        if((refund_master_Et.getText().length() == 0) || (bank_Et.getText().length() == 0)
                || (out_point_Et.getText().length() == 0) || (account_num_Et.getText().length() == 0)){
            new OneButtonDialog(PointExchangeActivity.this, "알 림", "환급신청정보들을 모두 입력하여 주세요.", "확 인", new OneButtonDialog.ChoiceListner() {
                @Override
                public void confirm() {
                    return;
                }
            }).show();
            return;
        }
        int out_point = Integer.parseInt(out_point_Et.getText().toString());
        if(out_point < 50){
            new OneButtonDialog(PointExchangeActivity.this, "알 림", "50포인트부터 출금가능합니다.", "확 인", new OneButtonDialog.ChoiceListner() {
                @Override
                public void confirm() {
                    return;
                }
            }).show();
            return;
        }else if(out_point > this.myPoint ) {
            new OneButtonDialog(PointExchangeActivity.this, "알 림", "현재포인트가 충분하지 못합니다. 출금포인트를 현재포인트보다 작게 입력하세요.", "확 인", new OneButtonDialog.ChoiceListner() {
                @Override
                public void confirm() {
                    return;
                }
            }).show();
            return;
        }

        DialogView.progressDialogShow(this); // 잠시만 기다려주세요
        // 환급처리
        RequestParams params = new RequestParams();
        params.put("point", out_point_Et.getText()); // 출금포인트
        params.put("buyer", refund_master_Et.getText()); // 예금주
        params.put("bankinfo", bank_Et.getText()); //은행정보
        params.put("account_num", account_num_Et.getText()); //구좌번호
        params.put("user_no", new PreferencesManager(this).getUser().getUserNo());
        params.put("user_id", new PreferencesManager(this).getUser().getUserId());
        new HttpsManager(this).actBankRefund(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                // 포인트환급신청성공
                getRefundPointHistory(); //환급이력 다시 얻기
                new OneButtonDialog(PointExchangeActivity.this, "알 림", "환급신청이 성공하였습니다.", "확 인", new OneButtonDialog.ChoiceListner() {
                    @Override
                    public void confirm() {
                        return;
                    }
                }).show();
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                new OneButtonDialog(PointExchangeActivity.this, "알 림", "환급신청이 실패하였습니다.", "확 인", new OneButtonDialog.ChoiceListner() {
                    @Override
                    public void confirm() {
                        return;
                    }
                }).show();
                setToast(errorStr);
            }
        });
    }

    private void setTab(int resId){
        if(R.id.charge_point_tv == resId){ // 포인트 충전내역 탭
            chargeTv.setTextColor(getResources().getColor(R.color.purple));
            chargeTv.setBackgroundColor(Color.WHITE);
            useTv.setTextColor(getResources().getColor(R.color.gray));
            useTv.setBackgroundResource(R.drawable.gradient_round);
            refundTv.setTextColor(getResources().getColor(R.color.gray));
            refundTv.setBackgroundResource(R.drawable.gradient_round);

            chargeLv.setVisibility(View.VISIBLE);
            useLv.setVisibility(View.GONE);
            refundLv.setVisibility(View.GONE);
            refundBtn.setVisibility(View.GONE);

            mypointLv.setText("현재 포인트");
            getPointChargeHistory(); // 포인트충전내역 얻기

        }else if(R.id.use_point_tv == resId){ // 포인트사용내역 탭
            useTv.setTextColor(getResources().getColor(R.color.purple));
            useTv.setBackgroundColor(Color.WHITE);
            chargeTv.setTextColor(getResources().getColor(R.color.gray));
            chargeTv.setBackgroundResource(R.drawable.gradient_round);
            refundTv.setTextColor(getResources().getColor(R.color.gray));
            refundTv.setBackgroundResource(R.drawable.gradient_round);

            chargeLv.setVisibility(View.GONE);
            useLv.setVisibility(View.VISIBLE);
            refundLv.setVisibility(View.GONE);
            refundBtn.setVisibility(View.GONE);

            mypointLv.setText("현재 포인트");
            getPointUseHistory(); // 포인트사용내역 얻기

        } else if(R.id.refund_point_tv == resId){ //환급내역탭
            refundTv.setTextColor(getResources().getColor(R.color.purple));
            refundTv.setBackgroundColor(Color.WHITE);
            chargeTv.setTextColor(getResources().getColor(R.color.gray));
            chargeTv.setBackgroundResource(R.drawable.gradient_round);
            useTv.setTextColor(getResources().getColor(R.color.gray));
            useTv.setBackgroundResource(R.drawable.gradient_round);

            chargeLv.setVisibility(View.GONE);
            useLv.setVisibility(View.GONE);
            refundLv.setVisibility(View.VISIBLE);
            refundBtn.setVisibility(View.VISIBLE);

            mypointLv.setText("내 포인트");
            getRefundPointHistory(); // 환급처리이력목록 출력
        }
    }

    // 은행정보얻기
    private void getBankNum(){
        DialogView.progressDialogShow(this);
        RequestParams params = new RequestParams();
        params.put("user_no", new PreferencesManager(this).getUser().getUserNo());
        params.put("user_id", new PreferencesManager(this).getUser().getUserId());
        new HttpsManager(this).getBankInfo(params, new HttpsManager.HttpListner() {
            @Override
            public void onSuccess(Object result) {
                DialogView.progressDialogClose();
                bank_info = (Bank)result;
                bank_Et.setText(bank_info.getName()); //은행이름 설정
                refund_master_Et.setText(bank_info.getOwner()); // 예금주
                account_num_Et.setText(bank_info.getNumber()); //구좌번호설정
            }

            @Override
            public void onFailure(String errorStr) {
                DialogView.progressDialogClose();
                setToast(errorStr);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.charge_point_tv:
                setTab(R.id.charge_point_tv);
                break;
            case R.id.use_point_tv:
                setTab(R.id.use_point_tv);
                break;
            case R.id.refund_point_tv:
                setTab(R.id.refund_point_tv);
                break;
            case R.id.refund_point_btn:
                onClickRefundBtn();
                break;
        }
    }
}
