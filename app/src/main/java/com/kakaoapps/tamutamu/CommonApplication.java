package com.kakaoapps.tamutamu;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.facebook.FacebookSdk;
import com.kakao.auth.KakaoSDK;
import com.kakaoapps.tamutamu.model.User;
import com.kakaoapps.tamutamu.sns.KakaoSDKAdapter;

/**
 * Created by John on 2016-01-29.
 */
public class CommonApplication extends MultiDexApplication {
    private static volatile Activity currentActivity = null;
    private static CommonApplication instance = null;

    //private static User mUser = new User();

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        KakaoSDK.init(new KakaoSDKAdapter());
        FacebookSdk.sdkInitialize(getApplicationContext());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(getApplicationContext());
        MultiDex.install(getBaseContext());
        MultiDex.install(this);
    }

    /* public static User getLoginUser(){
        return mUser;
    }

    public static void setLoginUser(User user){
        mUser = user;
    }*/


    public static Activity getCurrentActivity() {
        return currentActivity;
    }

    // Activity가 올라올때마다 Activity의 onCreate에서 호출해줘야한다.
    public static void setCurrentActivity(Activity currentActivity) {
        CommonApplication.currentActivity = currentActivity;
    }

    /**
     * singleton 애플리케이션 객체를 얻는다.
     * @return singleton 애플리케이션 객체
     */
    public static CommonApplication getGlobalApplicationContext() {
        if(instance == null)
            throw new IllegalStateException("this application does not inherit com.kakao.GlobalApplication");
        return instance;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        instance = null;
    }


}
