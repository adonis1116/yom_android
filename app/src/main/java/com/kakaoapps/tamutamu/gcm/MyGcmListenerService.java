package com.kakaoapps.tamutamu.gcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.gcm.GcmListenerService;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.activity.ProgressActivity;
import com.kakaoapps.tamutamu.activity.QuestionActivity;
import com.kakaoapps.tamutamu.manager.PreferencesManager;
import com.kakaoapps.tamutamu.utils.DebugUtils;

/**
 * Created by John on 2015-12-31.
 */
public class MyGcmListenerService extends GcmListenerService {
    private final String TAG = getClass().getSimpleName();

    @Override
    public void onMessageReceived(String from, Bundle data) {
        super.onMessageReceived(from, data);
        DebugUtils.setLog(TAG, "data : " + data.toString());

        String title = data.getString("title");
        String message = data.getString("msg");
        String type = data.getString("type");

        DebugUtils.setLog(TAG, from);
        DebugUtils.setLog(TAG, title);
        DebugUtils.setLog(TAG, message);

        // added by RGJ 2017.02.03
        // added end

        PreferencesManager pm = new PreferencesManager(this);
        if(type.equals("1")){
            if(pm.isPush()){
                sendNotification(data, 1);
            }
        }
        else if(type.equals("3")){
            if(pm.isPush()){
                sendNotification(data, 3);
            }
        }
        else{
            if(pm.isMemoPush()){
                sendNotification(data, 2);
            }
        }

    }

    // changed by RGJ 2017.02.03
    private void sendNotification(Bundle data, int type){
        String title = data.getString("title");
        String message = data.getString("msg");

        NotificationManager nm = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent(); //
        if(type == 1) {
            // added by RGJ 2017.02.03
            int process_type = Integer.parseInt(data.getString("process_type"));
            int req_type =  Integer.parseInt(data.getString("req_type"));
            if (req_type == 0){
                if (process_type == 5) process_type = process_type - 2;
                else process_type --;
            }

            intent = new Intent(this, ProgressActivity.class);
            intent.putExtra("type", process_type);
            intent.putExtra("req_type", req_type);

            Intent intentProgress = new Intent(ProgressActivity.ACTION_RECEIVE_PUSH);
            intentProgress.putExtra("type", process_type);
            intentProgress.putExtra("req_type", req_type);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intentProgress);
        }else if(type == 2) {
            // added by RGJ 2017.02.03
            String nick = data.getString("nick");
            String userid = data.getString("userid");
            int user_no = Integer.parseInt(data.getString("idx"));

            intent = new Intent(this, QuestionActivity.class);
            intent.putExtra("partner_no", user_no);
            intent.putExtra("partner_id", userid);

            Intent intentMemo = new Intent(QuestionActivity.ACTION_RECEIVE_PUSH);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intentMemo);
        }
        int uniqueInt = (int) (System.currentTimeMillis() & 0xfffffff);
        PendingIntent pi = PendingIntent.getActivity(this, uniqueInt, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setContentTitle(title)
                .setContentText(message)
                .setTicker(title)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pi)
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS);
        //.setNumber(Integer.MAX_VALUE);
        Notification noti = builder.build();
        nm.notify(Integer.MAX_VALUE, noti);
    }
}
