package com.kakaoapps.tamutamu.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by John on 2015-12-21.
 */
public class NoteTextView extends TextView{
    private final String TAG = getClass().getSimpleName();

    private Rect mRect;
    private Paint mPaint;

    public NoteTextView(Context context) {
        super(context);
        init();
    }

    public NoteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NoteTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        mRect = new Rect();
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(Color.BLACK);
        mPaint.setStrokeWidth(3);

        DashPathEffect dashPathEffect = new DashPathEffect(new float[]{20, 3}, 1);
        mPaint.setPathEffect(dashPathEffect);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Rect rect = mRect;
        Paint paint = mPaint;
        int line = getLineCount();

        for(int i=0; i<line; i++){
            int baseLine = getLineBounds(i, rect);
            canvas.drawLine(rect.left, baseLine + 20, rect.right, baseLine + 20, paint);
        }
    }
}
