package com.kakaoapps.tamutamu.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by john on 2016-02-12.
 */
public class User implements Parcelable{
    private int userType;
    private int userNo;
    private String userId;
    private String password;
    private String userNick;
    private String userPhone;
    private int area;
    private int subArea;
    private int area2;
    private int subArea2;
    private int area3;
    private int subArea3;
    private String snsId;
    private String userPic;
    private String point;
    private String overview; // added by Adonis
    private ArrayList<String> portfolio;
//    private String isUnreadMsg;


    public User(){}

    public User(Parcel in){
        readFromParcel(in);
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public int getUserNo() {
        return userNo;
    }

    public void setUserNo(int userNo) {
        this.userNo = userNo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserNick() {
        return userNick;
    }

    public void setUserNick(String userNick) {
        this.userNick = userNick;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public int getSubArea() {
        return subArea;
    }

    public void setSubArea(int subArea) {
        this.subArea = subArea;
    }

    public int getArea2() {
        return area2;
    }

    public void setArea2(int area2) {
        this.area2 = area2;
    }

    public int getSubArea2() {
        return subArea2;
    }

    public void setSubArea2(int subArea2) {
        this.subArea2 = subArea2;
    }

    public int getArea3() {
        return area3;
    }

    public void setArea3(int area3) {
        this.area3 = area3;
    }

    public int getSubArea3() {
        return subArea3;
    }

    public void setSubArea3(int subArea3) {
        this.subArea3 = subArea3;
    }

    public String getSnsId() {
        return snsId;
    }

    public void setSnsId(String snsId) {
        this.snsId = snsId;
    }

    public String getUserPic() {
        return userPic;
    }

    public void setUserPic(String userPic) {
        this.userPic = userPic;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

/*    public void setIsUnreadMsg(String isUnreadMsg) { this.isUnreadMsg = isUnreadMsg; }

    public boolean getIsUnreadMsg() {
        if (isUnreadMsg.equals("0"))
            return false;
        else
            return true;
    } */
    // added by Adonis 2017.07.05
    public String getOverview() { return overview; }

    public void setOverview(String overview) { this.overview = overview; }

    ///////////////

    public ArrayList<String> getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(ArrayList<String> portfolio) {this.portfolio  = portfolio;    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(userType);
        dest.writeInt(userNo);
        dest.writeString(userId);
        dest.writeString(password);
        dest.writeString(userNick);
        dest.writeString(userPhone);
        dest.writeInt(area);
        dest.writeInt(subArea);
        dest.writeInt(area2);
        dest.writeInt(subArea2);
        dest.writeInt(area3);
        dest.writeInt(subArea3);
        dest.writeString(snsId);
        dest.writeString(userPic);
        dest.writeString(point);
        dest.writeString(overview); // added by Adonis 2017.07.05
        dest.writeSerializable(portfolio);
    }

    public void readFromParcel(Parcel in){
        userType = in.readInt();
        userNo = in.readInt();
        userId = in.readString();
        password = in.readString();
        userNick = in.readString();
        userPhone = in.readString();
        area = in.readInt();
        subArea = in.readInt();
        area2 = in.readInt();
        subArea2 = in.readInt();
        area3 = in.readInt();
        subArea3 = in.readInt();
        snsId = in.readString();
        userPic = in.readString();
        point = in.readString();
        overview = in.readString();// added by Adonis 2017.07.05
        portfolio = (ArrayList<String>)in.readSerializable();
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

}
