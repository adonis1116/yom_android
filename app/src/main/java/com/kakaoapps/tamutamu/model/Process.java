package com.kakaoapps.tamutamu.model;

/**
 * Created by john on 2016-03-03.
 */
public class Process{
    private int id;
    private int userNo;
    private String userNick;
    private String price;
    private String content;
    private int type;
    private int total;
    private String pic;
    private String upfile;
    private String upreal;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserNo() {
        return userNo;
    }

    public void setUserNo(int userNo) {
        this.userNo = userNo;
    }

    public String getUserNick() {
        return userNick;
    }

    public void setUserNick(String userNick) {
        this.userNick = userNick;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getUpfile(){ return upfile;}
    public void setUpfile(String upfile){ this.upfile = upfile;}
    public String getUpreal(){ return upreal;}
    public void setUpreal(String upreal){ this.upreal = upreal;}
}
