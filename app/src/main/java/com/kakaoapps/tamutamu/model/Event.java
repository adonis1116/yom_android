package com.kakaoapps.tamutamu.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by john on 2016-03-02.
 */
public class Event implements Parcelable {
    private int id;
    private String subject;
    private String content;
    private String image;
    private String closeDate;
    private String link;

    public Event(){

    }

    public Event(Parcel in){
        readFromParcel(in);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String images) {
        this.image = images;
    }

    public String getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(subject);
        dest.writeString(content);
        dest.writeString(image);
        dest.writeString(closeDate);
        dest.writeString(link);
    }

    public void readFromParcel(Parcel in){
        id = in.readInt();
        subject = in.readString();
        content = in.readString();
        image = in.readString();
        closeDate = in.readString();
        link = in.readString();
    }


    @SuppressWarnings("rawtypes")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            // TODO Auto-generated method stub
            return new Event[size];
        }
    };
}
