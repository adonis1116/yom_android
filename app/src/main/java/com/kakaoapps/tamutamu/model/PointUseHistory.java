package com.kakaoapps.tamutamu.model;

/**
 * Created by Panther on 1/15/2017.
 */

public class PointUseHistory {
    public int id; // 본인 id
    public String trader_name; // 거래자닉네임
    public int type; //구매 1, 판매 2
    public int point; // 거래량
    public int status; // 상태, 완료?
    public String regdt; // 거래 날자/시간
    public String content; // 추가정보
}
