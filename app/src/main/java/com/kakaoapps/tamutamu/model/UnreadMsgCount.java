package com.kakaoapps.tamutamu.model;

/**
 * Created by john on 2016-02-15.
 */
public class UnreadMsgCount {
    private int unread_noti_count;
    public void setUnreadNotiCount(int unreadNotiCount) {
        unread_noti_count = unreadNotiCount;
    }
    public int getUnreadNotiCount() { return unread_noti_count; }
}
