package com.kakaoapps.tamutamu.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by john on 2016-02-29.
 */
public class Tag implements Parcelable{
    public int cate;
    public String tag;

    public Tag(){

    }

    public Tag(Parcel in){
        cate = in.readInt();
        tag = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(cate);
        dest.writeString(tag);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public Tag createFromParcel(Parcel in) {
            return new Tag(in);
        }

        @Override
        public Tag[] newArray(int size) {
            return new Tag[size];
        }
    };


}
