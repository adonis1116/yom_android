package com.kakaoapps.tamutamu.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by john on 2016-02-04.
 */
public class Can implements Parcelable{

    private int id;
    private String nickName;
    private String subject;
    private String content;
    private String price;
    private int area;
    private int subArea;
    private ArrayList<Tag> tagList;
    private ArrayList<String> imgList;
    private String userImage;
    private String userId;
    private int userNo;
    private String snsId;
    private int total;
    private boolean wish;
    private String shareLink;
    private String regdt; // added by Adonis 2017.6.30
    private double lat;//added by Adonis 2017.7.4
    private double lng;//added by Adonis 2017.7.4
    private ArrayList<String> fileList; //added by Adonis 2017.07.16

    public Can(){

    }

    public Can(Parcel in){
        readFromParcel(in);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public int getSubArea() {
        return subArea;
    }

    public void setSubArea(int subArea) {
        this.subArea = subArea;
    }

    public ArrayList<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(ArrayList<Tag> tagList) {
        this.tagList = tagList;
    }

    public ArrayList<String> getImgList() {
        return imgList;
    }

    public void setImgList(ArrayList<String> imgList) {
        this.imgList = imgList;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getUserNo() {
        return userNo;
    }

    public void setUserNo(int userNo) {
        this.userNo = userNo;
    }

    public String getSnsId() {
        return snsId;
    }

    public void setSnsId(String snsId) {
        this.snsId = snsId;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public boolean isWish() {
        return wish;
    }

    public void setWish(boolean wish) {
        this.wish = wish;
    }

    public String getShareLink() {
        return shareLink;
    }

    public void setShareLink(String shareLink) {
        this.shareLink = shareLink;
    }

    // Added by Adonis 2017.6.30

    public String getRegdt() { return regdt; };

    public void setRegdt( String regdt) { this.regdt = regdt; }

    // Added by Adonis 2017.07.04

    public double getLat() {return lat;}
    public void setLat(double lat) { this.lat = lat; }

    public double getLng() {return lng;}
    public void setLng(double lng) { this.lng = lng; }
    //

    // addedy by Adonis 2017.07.16
    public ArrayList<String> getFileList() {
        return fileList;
    }

    public void setFileList(ArrayList<String> fileList) {
        this.fileList = fileList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(nickName);
        dest.writeString(subject);
        dest.writeString(content);
        dest.writeString(price);
        dest.writeInt(area);
        dest.writeInt(subArea);
        dest.writeTypedList(tagList);
        dest.writeSerializable(imgList);
        dest.writeString(userImage);
        dest.writeString(userId);
        dest.writeInt(userNo);
        dest.writeString(snsId);
        dest.writeInt(total);
        dest.writeInt(wish ? 1 : 0);
        dest.writeString(shareLink);
        dest.writeString(regdt); // added by Adonis 2017.6.30
        dest.writeDouble(lat);// added by Adonis 2017.07.04
        dest.writeDouble(lng);// added by Adonis 2017.07.04
        dest.writeSerializable(fileList);// added by Adonis 2017.07.16s
    }

    public void readFromParcel(Parcel in){
        id = in.readInt();
        nickName = in.readString();
        subject = in.readString();
        content = in.readString();
        price = in.readString();
        area = in.readInt();
        subArea = in.readInt();
        tagList = new ArrayList<Tag>();
        in.readTypedList(tagList, Tag.CREATOR);
        //tagList = (ArrayList<Tag>)in.readSerializable();
        imgList = (ArrayList<String>)in.readSerializable();
        userImage = in.readString();
        userId = in.readString();
        userNo = in.readInt();
        snsId = in.readString();
        total = in.readInt();
        wish = in.readInt() == 1 ? true : false;
        shareLink = in.readString();
        regdt = in.readString(); // added by Adonis 2017.6.30
        lat = in.readDouble();//added by Adonis 2017.07.04
        lng = in.readDouble();//added by Adonis 2017.07.04
        fileList = (ArrayList<String>)in.readSerializable();// added by Adonis 2017.07.16s
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public Can createFromParcel(Parcel in) {
            return new Can(in);
        }

        @Override
        public Can[] newArray(int size) {
            // TODO Auto-generated method stub
            return new Can[size];
        }
    };
}
