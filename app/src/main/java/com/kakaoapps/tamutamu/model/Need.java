package com.kakaoapps.tamutamu.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by john on 2016-02-04.
 */
public class Need implements Parcelable{
    private int id;
    private String nickName;
    private String subject;
    private String content;
    private String userPic;
    private String term;
    private String price;
    private int area;
    private int subarea;
    private ArrayList<Tag> tagList;
    private String userId;
    private int userNo;
    private String snsId;
    private int total;
    private boolean wish;
    private String shareLink;
    private String regdt; // added by Adonis 2017.6.30
    private double lat;//added by Adonis 2017.7.4
    private double lng;//added by Adonis 2017.7.4

    public Need(){}

    public Need(Parcel in){
        readFromParcel(in);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUserPic() {
        return userPic;
    }

    public void setUserPic(String userPic) {
        this.userPic = userPic;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public int getSubarea() {
        return subarea;
    }

    public void setSubarea(int subarea) {
        this.subarea = subarea;
    }

    public ArrayList<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(ArrayList<Tag> tagList) {
        this.tagList = tagList;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getUserNo() {
        return userNo;
    }

    public void setUserNo(int userNo) {
        this.userNo = userNo;
    }

    public String getSnsId() {
        return snsId;
    }

    public void setSnsId(String snsId) {
        this.snsId = snsId;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public boolean isWish() {
        return wish;
    }

    public void setWish(boolean wish) {
        this.wish = wish;
    }

    public String getShareLink() {
        return shareLink;
    }

    public void setShareLink(String shareLink) {
        this.shareLink = shareLink;
    }

    // Added by Adonis 2017.6.30

    public String getRegdt() { return regdt; };

    public void setRegdt( String regdt) { this.regdt = regdt; }

    // Added by Adonis 2017.07.04

    public double getLat() {return lat;}
    public void setLat(double lat) { this.lat = lat; }

    public double getLng() {return lng;}
    public void setLng(double lng) { this.lng = lng; }
    //
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(nickName);
        dest.writeString(subject);
        dest.writeString(content);
        dest.writeString(userPic);
        dest.writeString(term);
        dest.writeString(price);
        dest.writeInt(area);
        dest.writeInt(subarea);
        dest.writeTypedList(tagList);
        dest.writeString(userId);
        dest.writeInt(userNo);
        dest.writeString(snsId);
        dest.writeInt(total);
        dest.writeInt(wish ? 1 : 0);
        dest.writeString(shareLink);
        dest.writeString(regdt); // added by Adonis 2017.6.30
        dest.writeDouble(lat);// added by Adonis 2017.07.04
        dest.writeDouble(lng);// added by Adonis 2017.07.04
    }

    public void readFromParcel(Parcel in){
        id = in.readInt();
        nickName = in.readString();
        subject = in.readString();
        content = in.readString();
        userPic = in.readString();
        term = in.readString();
        price = in.readString();
        area = in.readInt();
        subarea = in.readInt();
        tagList = new ArrayList<Tag>();
        in.readTypedList(tagList, Tag.CREATOR);
        userId = in.readString();
        userNo = in.readInt();
        snsId = in.readString();
        total = in.readInt();
        wish = in.readInt() == 1 ? true : false;
        shareLink = in.readString();
        regdt = in.readString(); // added by Adonis 2017.6.30
        lat = in.readDouble();//added by Adonis 2017.07.04
        lng = in.readDouble();//added by Adonis 2017.07.04
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public Need createFromParcel(Parcel in) {
            return new Need(in);
        }

        @Override
        public Need[] newArray(int size) {
            // TODO Auto-generated method stub
            return new Need[size];
        }
    };
}
