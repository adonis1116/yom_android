package com.kakaoapps.tamutamu.model;

/**
 * Created by Panther on 1/14/2017.
 */

public class PointRefundHistory {
    public int id;
    public int point; //환급처리된 포인트
    public String regdt; // 환급처리된 날자/시간
    public int status; // 상태
    public String title; // 제목 //환급신청
    public String category; //  처리중인가? 완료인가?

/*    public String bank_name; // 은행명
    public String account_number; // 계좌번호
    public String credit_person; // 예금주*/
}
