package com.kakaoapps.tamutamu.model;

/**
 * Created by developer on 2016-06-30.
 */
public class Memo {
    public String sendId;
    public String receiveId;
    public String partnerImg;
    public String latestMSG;
    public int chatRoom;
    public String regdt;
    public int totalRow;
    public String partnerNick;
    public int partnerNo;
}
