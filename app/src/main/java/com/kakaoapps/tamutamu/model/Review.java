package com.kakaoapps.tamutamu.model;

/**
 * Created by john on 2016-02-11.
 */
public class Review {
    private int id;
    private int writerNo;
    private String writerNick;
    private String writerId;
    private String writerSnsId;
    private String writerPic;
    private String content;
    private int point;
    private String regdt;
    private int total;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWriterNo() {
        return writerNo;
    }

    public void setWriterNo(int writerNo) {
        this.writerNo = writerNo;
    }

    public String getWriterNick() {
        return writerNick;
    }

    public void setWriterNick(String writerNick) {
        this.writerNick = writerNick;
    }

    public String getWriterId() {
        return writerId;
    }

    public void setWriterId(String writerId) {
        this.writerId = writerId;
    }

    public String getWriterSnsId() {
        return writerSnsId;
    }

    public void setWriterSnsId(String writerSnsId) {
        this.writerSnsId = writerSnsId;
    }

    public String getWriterPic() {
        return writerPic;
    }

    public void setWriterPic(String writerPic) {
        this.writerPic = writerPic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getRegdt() {
        return regdt;
    }

    public void setRegdt(String regdt) {
        this.regdt = regdt;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
