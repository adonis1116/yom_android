package com.kakaoapps.tamutamu.model;

/**
 * Created by developer on 2016-06-28.
 */
public class Question {
    public int id;
    public String receiveId;
    public String receivePath;
    public String sendId;
    public String content;
    public String file;
    public String fileSize;
    public int fileType;
    public String fileName;
    public String regdate;
    public int totalCount;
}
