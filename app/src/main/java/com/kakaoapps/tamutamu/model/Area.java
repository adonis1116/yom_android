package com.kakaoapps.tamutamu.model;

import java.util.ArrayList;

/**
 * Created by john on 2016-02-15.
 */
public class Area {
    private int id;
    private String name;
    private ArrayList<SubArea> subareaList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<SubArea> getSubareaList() {
        return subareaList;
    }

    public void setSubareaList(ArrayList<SubArea> subareaList) {
        this.subareaList = subareaList;
    }
}
