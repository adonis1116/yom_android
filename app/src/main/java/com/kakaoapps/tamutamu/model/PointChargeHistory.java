package com.kakaoapps.tamutamu.model;

/**
 * Created by Panther on 1/18/2017.
 */

public class PointChargeHistory {
    public int id;
    public int point; // 충전포인트
    public String regdt; // 환급처리된 날자/시간
    public int status; // 상태
    public String title; // 제목 //구매
    public String category; //  처리중인가? 완료인가?

/*    public String bank_name; // 은행명
    public String account_number; // 계좌번호
    public String credit_person; // 예금주*/
}
