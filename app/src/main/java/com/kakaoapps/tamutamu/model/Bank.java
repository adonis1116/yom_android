package com.kakaoapps.tamutamu.model;

/**
 * Created by john on 2016-03-09.
 */
public class Bank {
    private String name;
    private String number;
    private String owner;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
