package com.kakaoapps.tamutamu.model;

/**
 * Created by developer on 2016-07-06.
 */
public class Like {
    public int id;
    public String subject;
    public String content;
    public String price;
    public String pic;
    public String nick;
    public int totalCnt;
    public String type;
    public int gidx;
    public String regdt;
}
