package com.kakaoapps.tamutamu.manager;

import android.app.Activity;

import java.util.ArrayList;

/**
 * Created by John on 2015-12-15.
 */
public class ActivityManager {
    private final String TAG = getClass().getSimpleName();
    public static ArrayList<Activity> actList = new ArrayList<Activity>();

    public static void allActivityFinish(){
        for(Activity act : actList){
            act.finish();
        }
    }

    public static void activityFinish(String actClassName){
        for(Activity act : actList){
            if(act.getClass().getSimpleName().equals(actClassName) ){
                act.finish();
            }
        }
    }

    public static void allActivityFinish(String actClassName){
        for(Activity act : actList){
            if(!act.getClass().getSimpleName().equals(actClassName) )act.finish();
        }
    }
}
