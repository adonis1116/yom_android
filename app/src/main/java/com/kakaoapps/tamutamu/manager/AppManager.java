package com.kakaoapps.tamutamu.manager;


import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;

public class AppManager {

    public static boolean isNetworkCoonnection(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        boolean isWifiConn = false;
        NetworkInfo mobileNi = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        boolean isMobileConn = false;
//        NetworkInfo ethernetNi = cm.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET);
//        boolean isEthernetConn = false;

        if(wifiNi != null)isWifiConn = wifiNi.isConnected();
        if(mobileNi != null)isMobileConn = mobileNi.isConnected();
//        if(ethernetNi != null)isEthernetConn = ethernetNi.isConnected();
        if(isWifiConn || isMobileConn){
            return true;
        }
        return false;
    }


    public static boolean isServiceRunning(Context context, String serviceName){
        ActivityManager manager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);

        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceName.equals(service.service.getClassName())) {
                return true;
            }
        }

        return false;
    }

    public static String getAppVerName(Context context){

        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);

            return packageInfo.versionName;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getAppVerCode(Context context){

        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);

            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static final String getIMEI(Context context) {
        if (context != null)
            return ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE))
                    .getDeviceId();
        return null;
    }


    public static String getDeviceSerialNumber() {
        try {
            return (String) Build.class.getField("SERIAL").get(null);
        } catch (Exception ignored) {
            return null;
        }
    }
}