package com.kakaoapps.tamutamu.manager;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.model.User;

import java.util.ArrayList;

/**
 * Created by John on 2015-12-09.
 */


public class PreferencesManager {

    private Context mContext;
    private SharedPreferences pref;
    private SharedPreferences.Editor saver;


    public PreferencesManager(Context paramContext) {
        mContext = paramContext;
        pref = paramContext.getSharedPreferences(mContext.getString(R.string.app_name), Activity.MODE_PRIVATE);
        saver = pref.edit();
    }

    public void setIsLogin(boolean value){
        saver.putBoolean("isLogin", value);
        saver.commit();
    }

    public boolean isLogin(){
        return pref.getBoolean("isLogin", false);
    }

    public void setIsPush(boolean value){
        saver.putBoolean("isPush", value);
        saver.commit();
    }

    public boolean isPush(){
        return pref.getBoolean("isPush", true);
    }

    public void setIsMemoPush(boolean value){
        saver.putBoolean("isMemoPush", value);
        saver.commit();
    }

    public boolean isMemoPush(){
        return pref.getBoolean("isMemoPush", true);
    }

    public void setPushToken(String value){
        saver.putString("pushToken", value);
        saver.commit();
    }

    public boolean isSendSMS(){
        return pref.getBoolean("isSendSMS", true);
    }

    public void setIsSendSMS(Boolean value){
        saver.putBoolean("isSendSMS", value);
        saver.commit();
    }

    public String getPushToken(){
        return pref.getString("pushToken", null);
    }

    /**
     * 로그인 유저 정보  저장
     * @param 'User'
     */
    public void setUser(User value){
        saver.putInt("user_type", value.getUserType());
        saver.putInt("user_no", value.getUserNo());
        saver.putString("user_id", value.getUserId());
        saver.putString("user_nick", value.getUserNick());
        saver.putString("user_phone", value.getUserPhone());
        saver.putString("user_sns_id", value.getSnsId());
        saver.putInt("user_area1", value.getArea());
        saver.putInt("user_sub_area1", value.getSubArea());
        saver.putInt("user_area2", value.getArea2());
        saver.putInt("user_sub_area2", value.getSubArea2());
        saver.putInt("user_area3", value.getArea3());
        saver.putInt("user_sub_area3", value.getSubArea3());
        saver.putString("user_pic", value.getUserPic());
        saver.putString("user_point", value.getPoint());
        saver.putString("overview", value.getOverview()); //added by Adonis 2017.07.07
        saver.putString("user_pass", value.getPassword());
        for(int i = 0; i < 5; i++){
            saver.putString("portfolio"+i+1, "");
        }
        for(int i = 0; i < value.getPortfolio().size(); i++){
            saver.putString("portfolio"+i+1, value.getPortfolio().get(i));
        }
        saver.commit();
    }

    /**
     * 로그인 유저 정보
     * @return User
     */
    public User getUser(){
        User user = new User();
        user.setUserType(pref.getInt("user_type", 0));
        user.setUserNo(pref.getInt("user_no", 0));
        user.setUserId(pref.getString("user_id", null));
        user.setUserNick(pref.getString("user_nick", null));
        user.setUserPhone(pref.getString("user_phone", null));
        user.setSnsId(pref.getString("user_sns_id", null));
        user.setArea(pref.getInt("user_area1", 0));
        user.setSubArea(pref.getInt("user_sub_area1", 0));
        user.setArea2(pref.getInt("user_area2", 0));
        user.setSubArea2(pref.getInt("user_sub_area2", 0));
        user.setArea3(pref.getInt("user_area3", 0));
        user.setSubArea3(pref.getInt("user_sub_area3", 0));
        user.setUserPic(pref.getString("user_pic", null));
        user.setPoint(pref.getString("user_point", null));
        user.setOverview(pref.getString("overview", "")); //added by Adonis 2017.07.07
        user.setPassword(pref.getString("user_pass",""));

        ArrayList<String> portfolio = new ArrayList<>();
        for(int i = 0; i < 5; i++){
            String portItem = pref.getString("portfolio" + i+1, "");
            if (portItem != "") portfolio.add(portItem);
        }
        user.setPortfolio(portfolio);
        return user;
    }


    public void setKakaoId(String value){
        saver.putString("kakao_id", value);
        saver.commit();
    }

    public String getKakaoId(){
        return  pref.getString("kakao_id", null);
    }

    public void setUserPoint(String value){
        saver.putString("user_point", value);
        saver.commit();
    }
}