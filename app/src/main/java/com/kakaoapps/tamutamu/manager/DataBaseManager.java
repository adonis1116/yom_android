package com.kakaoapps.tamutamu.manager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kakaoapps.tamutamu.model.Area;
import com.kakaoapps.tamutamu.model.Event;
import com.kakaoapps.tamutamu.model.SubArea;
import com.kakaoapps.tamutamu.model.User;
import com.kakaoapps.tamutamu.utils.DebugUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by john on 2016-02-03.
 */
public class DataBaseManager extends SQLiteOpenHelper {
    private final String TAG = getClass().getSimpleName();

    private Context mContext;

    private final static String DATABASE_NAME = "tamutamu.db";
    private final static int DATABASE_VERSION = 1;

    private final String AREA_TABLE_NAME = "area";
    private final String SUBAREA_TABLE_NAME = "subarea";
    private final String EVENT_TABLE_NAME = "event";

    private final String CREATE_AREA_TABLE = "CREATE TABLE " + AREA_TABLE_NAME + " ("
            + " id INTEGER PRIMARY KEY,"
            + " name TEXT"
            + ")";

    private final String CREATE_SUBAREA_TABLE = "CREATE TABLE " + SUBAREA_TABLE_NAME + " ("
            + " id INTEGER PRIMARY KEY,"
            + " area_id INTEGER,"
            + " name TEXT,"
            + " lat TEXT, "
            + " lng TEXT"
            + ")";

    private final String CREATE_EVENT_TABLE = "CREATE TABLE " + EVENT_TABLE_NAME + " ("
            + " id INTEGER PRIMARY KEY,"
            + " close_date TEXT"
            + ")";



    public DataBaseManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        DebugUtils.setLog(TAG, "=========================== Create Table ===========================");
        db.execSQL(CREATE_AREA_TABLE);
        db.execSQL(CREATE_SUBAREA_TABLE);
        db.execSQL(CREATE_EVENT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        DebugUtils.setLog(TAG, "=========================== Drop Table ===========================");
        db.execSQL("DROP TABLE IF EXISTS " + AREA_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + SUBAREA_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + EVENT_TABLE_NAME);
    }

    private String getSelectSql (String table, HashMap<String, Object> map){
        String sql = "SELECT * FROM " + table;

        if(map != null && map.size() > 0){
            sql+=" WHERE ";
            Iterator<String> it = map.keySet().iterator();
            for(int i=0; i<map.size(); i++){
                String key = it.next();
                Object value = map.get(key);
                sql += key + "='" + value + "' ";
                if(i != map.size()-1){
                    sql += "AND ";
                }
            }
        }
        DebugUtils.setLog(TAG, "sql : " + sql);

        return sql;
    }


    public Area getArea(HashMap<String, Object> map){
        Area result = null;
        String sql = getSelectSql(AREA_TABLE_NAME, map);

        Cursor cursor = null;
        try{
            cursor = getReadableDatabase().rawQuery(sql, null);
            if(cursor != null && cursor.getCount() > 0){
                result = new Area();
                if (cursor.moveToNext()) {
                    result.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    result.setName(cursor.getString(cursor.getColumnIndex("name")));
                }
            }
        }catch(Exception e){
            e.getStackTrace();
        }finally{
            if(cursor != null && !cursor.isClosed())cursor.close();
        }

        return result;
    }

    public SubArea getSubArea(HashMap<String, Object> map){
        SubArea result = null;
        String sql = getSelectSql(SUBAREA_TABLE_NAME, map);

        Cursor cursor = null;
        try{
            cursor = getReadableDatabase().rawQuery(sql, null);
            if(cursor != null && cursor.getCount() > 0){
                result = new SubArea();
                if (cursor.moveToNext()) {
                    result.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    result.setAreaId(cursor.getInt(cursor.getColumnIndex("area_id")));
                    result.setName(cursor.getString(cursor.getColumnIndex("name")));
                    result.setLat(cursor.getString(cursor.getColumnIndex("lat")));
                    result.setLng(cursor.getString(cursor.getColumnIndex("lng")));
                }
            }
        }catch(Exception e){
            e.getStackTrace();
        }finally{
            if(cursor != null && !cursor.isClosed())cursor.close();
        }

        return result;
    }


    public ArrayList<Area> getAreaList(HashMap<String, Object> map){
        ArrayList<Area> result = null;
        String sql = getSelectSql(AREA_TABLE_NAME, map);

        Cursor cursor = null;
        try{
            cursor = getReadableDatabase().rawQuery(sql, null);
            if(cursor != null && cursor.getCount() > 0){
                result = new ArrayList<Area>();
                while (cursor.moveToNext()) {
                    Area item = new Area();
                    item.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    item.setName(cursor.getString(cursor.getColumnIndex("name")));

                    result.add(item);
                }
            }
        }catch(Exception e){
            e.getStackTrace();
        }finally{
            if(cursor != null && !cursor.isClosed())cursor.close();
        }

        return result;
    }

    public ArrayList<SubArea> getSubAreaList(HashMap<String, Object> map){
        ArrayList<SubArea> result = null;
        String sql = getSelectSql(SUBAREA_TABLE_NAME, map);

        Cursor cursor = null;
        try{
            cursor = getReadableDatabase().rawQuery(sql, null);
            if(cursor != null && cursor.getCount() > 0){
                result = new ArrayList<SubArea>();
                while (cursor.moveToNext()) {
                    SubArea item = new SubArea();
                    item.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    item.setAreaId(cursor.getInt(cursor.getColumnIndex("area_id")));
                    item.setName(cursor.getString(cursor.getColumnIndex("name")));
                    item.setLat(cursor.getString(cursor.getColumnIndex("lat")));
                    item.setLng(cursor.getString(cursor.getColumnIndex("lng")));
                    result.add(item);
                }
            }
        }catch(Exception e){
            e.getStackTrace();
        }finally{
            if(cursor != null && !cursor.isClosed())cursor.close();
        }

        return result;
    }

    public void insertAllArea(ArrayList<Area> list){
        getReadableDatabase().execSQL("DELETE FROM " + AREA_TABLE_NAME);
        getReadableDatabase().execSQL("DELETE FROM " + SUBAREA_TABLE_NAME);

        for(Area item : list){
            insertArea(item);
            for(SubArea subItem : item.getSubareaList()){
                insertSubArea(subItem);
            }
        }

    }

    public void insertArea(Area value){
        ContentValues values = new ContentValues();
        values.put("id", value.getId());
        values.put("name", value.getName());
        getWritableDatabase().insert(AREA_TABLE_NAME, null, values);
        if(getWritableDatabase() != null && getWritableDatabase().isOpen())getWritableDatabase().close();
    }

    public void insertSubArea(SubArea value){
        ContentValues values = new ContentValues();
        values.put("id", value.getId());
        values.put("area_id", value.getAreaId());
        values.put("name" , value.getName());
        values.put("lat", value.getLat());
        values.put("lng", value.getLng());
        getWritableDatabase().insert(SUBAREA_TABLE_NAME, null, values);
        if(getWritableDatabase() != null && getWritableDatabase().isOpen())getWritableDatabase().close();
    }



    public Event getEvent(HashMap<String, Object> map){
        Event result = null;
        String sql = getSelectSql(EVENT_TABLE_NAME, map);
        Cursor cursor = null;
        try{
            cursor = getReadableDatabase().rawQuery(sql, null);
            if(cursor != null && cursor.getCount() > 0){
                result = new Event();
                if (cursor.moveToNext()) {
                    DebugUtils.setLog(TAG, "id : " + cursor.getInt(cursor.getColumnIndex("id")));
                    result.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    result.setCloseDate(cursor.getString(cursor.getColumnIndex("close_date")));
                }
            }
        }catch(Exception e){
            e.getStackTrace();
        }finally{
            if(cursor != null && !cursor.isClosed())cursor.close();
        }
        return result;
    }

    public void insertEvent(Event value){
        DebugUtils.setLog(TAG, "value : " + value.getId());
        ContentValues values = new ContentValues();
        values.put("id", value.getId());
        values.put("close_date" , value.getCloseDate());
        getWritableDatabase().insert(EVENT_TABLE_NAME, null, values);
        if(getWritableDatabase() != null && getWritableDatabase().isOpen())getWritableDatabase().close();
    }

    public void updateEvent(Event value){
        String sql = "UPDATE " + EVENT_TABLE_NAME + " SET close_date='" + value.getCloseDate() + "' WHERE id=" + value.getId();
        DebugUtils.setLog(TAG, sql);
        getWritableDatabase().execSQL(sql);
        if(getWritableDatabase() != null && getWritableDatabase().isOpen())getWritableDatabase().close();
    }



}
