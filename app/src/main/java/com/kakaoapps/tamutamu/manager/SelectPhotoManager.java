package com.kakaoapps.tamutamu.manager;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.widget.ImageView;

import com.kakaoapps.tamutamu.BuildConfig;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.activity.ModifyProfileActivity;
import com.kakaoapps.tamutamu.utils.DebugUtils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;

public class SelectPhotoManager {
    private final String TAG = getClass().getSimpleName();

    public static final int PICK_FROM_CAMERA = 0;
    public static final int PICK_FROM_ALBUM = 1;
    public static final int CROP_FROM_CAMERA = 2;
    public static final int MY_CAMERA_REQUEST_CODE = 100;
    public String from_where = "";
    private Context mContext;


    public static Uri mImageCaptureUri;
    private ImageView iv;

    public interface SelectPhotoListner{
        void selectPhoto(Bitmap bitmap);
    }

    private SelectPhotoListner mListner;

    public SelectPhotoManager(Context context) {
        mContext = context;
    }

    public SelectPhotoManager(Context context, ImageView iv, SelectPhotoListner listner) {
        mContext = context;
        this.iv = iv;
        mListner = listner;
    }



    // 앨범 호출
    public void doTakeAlbumAction() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
        ((Activity) mContext).startActivityForResult(intent, PICK_FROM_ALBUM);
    }

    public void doTakePhotoAction() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String url = "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
        mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), url));
        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
        intent.putExtra("return-data", true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int permissionCheck = ContextCompat.checkSelfPermission(mContext,
                    android.Manifest.permission.CAMERA);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                //showing dialog to select image
                ((Activity) mContext).startActivityForResult(intent, PICK_FROM_CAMERA);
            } else {
                ActivityCompat.requestPermissions((Activity)mContext,
                        new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
            }
            return;
        } else {
            ((Activity) mContext).startActivityForResult(intent, PICK_FROM_CAMERA);
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) return;

        switch (requestCode) {
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();
                if (!resultUri.equals("") || resultUri != null) {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap photo = BitmapFactory.decodeFile(resultUri.getPath(), options);
                    if (iv != null) iv.setImageBitmap(photo);
                    if (mListner != null) mListner.selectPhoto(photo);
                }
                break;
            case CROP_FROM_CAMERA: // changed by RGJ 2017.02.04
                DebugUtils.setLog(TAG, "CROP_FROM_CAMERA");
                // 크롭이 된 이후의 이미지를 넘겨 받습니다.
                // 이미지뷰에 이미지를 보여준다거나 부가적인 작업 이후에
                // 임시 파일을 삭제합니다.
                final Bundle extras = data.getExtras();

                if (extras != null) {
                    if (extras.containsKey("data")){
                        Bitmap photo = extras.getParcelable("data");
                        /*DebugUtils.setLog(TAG, "iv : " + iv);*/
                        if(iv != null)iv.setImageBitmap(photo);
                        if(mListner != null)mListner.selectPhoto(photo);
                        /*DebugUtils.setLog(TAG, "path : " + mImageCaptureUri.getPath());*/
                            // 임시 파일 삭제
                        /*File f = new File(mImageCaptureUri.getPath());
                        if (f.exists()) {
                            f.delete();
                        }*/
                    }
                }else{
                    if ( data.getData() != null ){
                        mImageCaptureUri = data.getData();
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                        Bitmap photo = BitmapFactory.decodeFile(mImageCaptureUri.getPath(), options);
                        /*DebugUtils.setLog(TAG, "iv : " + iv);*/
                        if(iv != null)iv.setImageBitmap(photo);
                        if(mListner != null)mListner.selectPhoto(photo);
                        /*DebugUtils.setLog(TAG, "path : " + mImageCaptureUri.getPath());*/
                            // 임시 파일 삭제
                        /*File f = new File(mImageCaptureUri.getPath());
                        if (f.exists()) {
                            f.delete();
                        }*/
                    }
                }
                break;

            case PICK_FROM_ALBUM:
                DebugUtils.setLog(TAG, "PICK_FROM_ALBUM");
                // 이후의 처리가 카메라와 같으므로 일단  break없이 진행합니다.
                // 실제 코드에서는 좀더 합리적인 방법을 선택하시기 바랍니다.

                mImageCaptureUri = data.getData();
            case PICK_FROM_CAMERA:
                if (Build.VERSION.SDK_INT >= 24) {
                    int width = 0;
                    int height = 0;
                    switch (from_where){
                        case "can":
                        case "question":
                            width = 320;
                            height = 200;
                            break;
                        case "signup":
                        case "profile":
                            width = 300;
                            height = 300;
                            break;
                    }
                    CropImage.activity(mImageCaptureUri)
                            .setGuidelines(CropImageView.Guidelines.OFF)
                            .setActivityTitle("선택해주세요")
                            .setFixAspectRatio(true)
                            .setRequestedSize(width, height)
                            .start((Activity) mContext);
                }
                else{
                    DebugUtils.setLog(TAG, "PICK_FROM_CAMERA");
                    // 이미지를 가져온 이후의 리사이즈할 이미지 크기를 결정합니다.
                    // 이후에 이미지 크롭 어플리케이션을 호출하게 됩니다.
                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(mImageCaptureUri, "image/*");
                    switch (from_where){
                        case "can":
                        case "question":
                            intent.putExtra("aspectX", 16);
                            intent.putExtra("aspectY", 10);
                            intent.putExtra("outputX", 320);
                            intent.putExtra("outputY", 200);
                            break;
                        case "signup":
                        case "profile":
                            intent.putExtra("aspectX", 1);
                            intent.putExtra("aspectY", 1);
                            intent.putExtra("outputX", 300);
                            intent.putExtra("outputY", 300);
                            break;
                    }
                    intent.putExtra("scale", false);
                    intent.putExtra("return-data", true);
                    ((Activity)mContext).startActivityForResult(intent, CROP_FROM_CAMERA);
                }
                break;

        }
    }
}