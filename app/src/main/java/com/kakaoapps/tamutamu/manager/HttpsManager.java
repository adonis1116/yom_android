package com.kakaoapps.tamutamu.manager;

import android.content.Context;
import android.graphics.Point;

import com.kakaoapps.tamutamu.Const;
import com.kakaoapps.tamutamu.R;
import com.kakaoapps.tamutamu.UrlInfo;
import com.kakaoapps.tamutamu.dialog.DialogView;
import com.kakaoapps.tamutamu.model.*;
import com.kakaoapps.tamutamu.model.Process;
import com.kakaoapps.tamutamu.utils.DebugUtils;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by john on 2016-02-12.
 */
public class HttpsManager extends AsyncHttpClient {
    private final String TAG = getClass().getSimpleName();

    private final int TAG_MAX = 5;
    private final int IMG_MAX = 5;

    private Context mContext;

    public interface HttpListner {
        void onSuccess(Object result);
        void onFailure(String errorStr);
    }

    public HttpsManager(Context context) {
        mContext = context;

    }


    private String getUrl(String url, RequestParams params){
        DebugUtils.setLog(TAG, url + "?" + params.toString());
        return url + "?" + params.toString();
    }

    public void login(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.LOGIN;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    DebugUtils.setLog(TAG, "" + response.getString("msg"));
                    if (response.getInt("status") == 0) {
                        User user = new User();
                        user.setUserType(response.getInt("user_type"));
                        user.setUserNo(response.getInt("user_no"));
                        user.setUserId(response.getString("user_id"));
                        user.setUserNick(response.getString("user_nick"));
                        user.setUserPhone(response.getString("user_phone"));
                        user.setArea(response.getInt("user_area"));
                        user.setSubArea(response.getInt("user_sub_area"));
                        user.setArea2(response.getInt("user_area2"));
                        user.setSubArea2(response.getInt("user_sub_area2"));
                        user.setArea3(response.getInt("user_area3"));
                        user.setSubArea3(response.getInt("user_sub_area3"));
                        user.setSnsId(response.getString("user_sns_id"));
                        user.setUserPic(response.getString("user_img"));
                        user.setPoint(response.getString("mypoint"));
                        user.setOverview(response.getString("overview"));//added by Adonis 2017.07.07
                        ArrayList<String> portfolio = new ArrayList<String>();
                        for(int i = 1; i <= 5; i++){
                            if (!response.getString("user_img"+i).isEmpty()) portfolio.add(response.getString("user_img"+i));
                        }
                        user.setPortfolio(portfolio);
                        listner.onSuccess(user);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    public void getUnreadMsgCount(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.UNREAD_MSG_COUNT;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    UnreadMsgCount umc = new UnreadMsgCount();
                    umc.setUnreadNotiCount(response.getInt("unread_msg_count"));
                    listner.onSuccess(umc);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    public void test(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.LOGIN;
        RequestHandle handle = get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    DebugUtils.setLog(TAG, "" + response.getString("msg"));
                    if (response.getInt("status") == 0) {
                        User user = new User();
                        user.setUserType(response.getInt("user_type"));
                        user.setUserNo(response.getInt("user_no"));
                        user.setUserId(response.getString("user_id"));
                        user.setUserNick(response.getString("user_nick"));
                        user.setUserPhone(response.getString("user_phone"));
                        user.setArea(response.getInt("user_area"));
                        user.setSubArea(response.getInt("user_sub_area"));
                        user.setArea2(response.getInt("user_area2"));
                        user.setSubArea2(response.getInt("user_sub_area2"));
                        user.setArea3(response.getInt("user_area3"));
                        user.setSubArea3(response.getInt("user_sub_area3"));
                        user.setSnsId(response.getString("user_sns_id"));
                        user.setUserPic(response.getString("user_img"));
                        DebugUtils.setLog(TAG, "pic : " + response.getString("user_img"));
                        listner.onSuccess(user);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        });

    }

    public void getCanList(RequestParams params, final HttpListner listner) {

        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.CAN_LIST;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                DebugUtils.setLog(TAG, response.toString());
                try {
                    if (response.getInt("status") == 0) {

                        ArrayList<Can> list = new ArrayList<Can>();
                        JSONArray jsonArrayNearUsers = new JSONArray();
                        if(response.getString("list") != null && !response.getString("list").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                if ( obj.get("user_id").equals(null) || obj.get("user_no").equals(null) ) continue;
                                Can item = new Can();
                                item.setId(obj.getInt("idx"));

                                ArrayList<String> imgList = new ArrayList<String>();
                                for(int j=0; j<IMG_MAX; j++){
                                    String img = obj.getString("img" + (j + 1));
                                    if(img != null && !img.trim().equals("null") && img.length() > 0)imgList.add(img);
                                }
                                item.setImgList(imgList);

                                // added by Adonis 2017.07.16
                                ArrayList<String> fileList = new ArrayList<String>();
                                for(int j=0; j<IMG_MAX; j++){
                                    String strFile = obj.getString("file" + (j + 1));
                                    if(strFile != null && !strFile.trim().equals("null") && strFile.length() > 0)
                                        fileList.add(strFile);
                                }
                                item.setFileList(fileList);

                                item.setUserImage(obj.getString("user_img"));
                                item.setNickName(obj.getString("user_nick"));
                                item.setSubject(obj.getString("subject"));
                                item.setContent(obj.getString("content"));
                                item.setPrice(obj.getString("price"));
                                item.setArea(obj.getInt("area_code"));
                                item.setSubArea(obj.getInt("subarea_code"));
                                item.setUserId(obj.getString("user_id"));
                                item.setUserNo(obj.getInt("user_no"));
                                item.setSnsId(obj.getString("user_kakao_id"));
                                item.setTotal(obj.getInt("total_rows"));
                                item.setWish(obj.getInt("wish") == 1 ? true : false);
                                item.setShareLink(obj.getString("share_link"));

                                item.setLat( Double.parseDouble(obj.getString("lat"))); // added by Adonis 2017.07.04
                                item.setLng( Double.parseDouble(obj.getString("lng"))); // added by Adonis 2017.07.04
                                item.setRegdt(obj.getString("regdt")); // added by Adonis 2017.6.30

                                ArrayList<Tag> tagList = new ArrayList<Tag>();
                                for(int j=0; j<TAG_MAX; j++){
                                    Tag tag = new Tag();
                                    tag.cate = obj.getInt("cate" + (j + 1));
                                    tag.tag = obj.getString("tag" + (j + 1));
                                    if(tag != null && !tag.tag.trim().equals("null") && tag.tag.length() > 0)tagList.add(tag);
                                }
                                item.setTagList(tagList);

                                list.add(item);
                            }

                        }
                        if(response.getString("user_list") != null && !response.getString("user_list").equals("null")) {
                            jsonArrayNearUsers = new JSONArray(response.getString("user_list"));
                        }

                        HashMap<String, Object> map = new HashMap<String, Object>();
                        map.put("list", list);
                        map.put("near_users_array", jsonArrayNearUsers);
                        listner.onSuccess(map);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void getCanDetail(RequestParams params, final HttpListner listner) {

        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.ICAN_DETAIL;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                DebugUtils.setLog(TAG, response.toString());
                try {
                    if (response.getInt("status") == 0) {

                        ArrayList<Can> list = new ArrayList<Can>();
                        if(response.getString("list") != null && !response.getString("list").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                Can item = new Can();
                                item.setId(obj.getInt("idx"));

                                ArrayList<String> imgList = new ArrayList<String>();
                                for(int j=0; j<IMG_MAX; j++){
                                    String img = obj.getString("img" + (j + 1));
                                    if(img != null && !img.trim().equals("null") && img.length() > 0)imgList.add(img);
                                }
                                item.setImgList(imgList);

                                // added by Adonis 2017.07.16
                                ArrayList<String> fileList = new ArrayList<String>();
                                for(int j=0; j<IMG_MAX; j++){
                                    String strFile = obj.getString("file" + (j + 1));
                                    if(strFile != null && !strFile.trim().equals("null") && strFile.length() > 0)fileList.add(strFile);
                                }
                                item.setFileList(fileList);

                                item.setUserImage(obj.getString("user_img"));
                                item.setNickName(obj.getString("user_nick"));
                                item.setSubject(obj.getString("subject"));
                                item.setContent(obj.getString("content"));
                                item.setPrice(obj.getString("price"));
                                item.setArea(obj.getInt("area_code"));
                                item.setSubArea(obj.getInt("subarea_code"));
                                item.setUserId(obj.getString("user_id"));
                                item.setUserNo(obj.getInt("user_no"));
                                item.setSnsId(obj.getString("user_kakao_id"));
                                item.setWish(obj.getInt("wish") == 1 ? true : false);
                                item.setShareLink(obj.getString("share_link"));
                                item.setRegdt(obj.getString("regdt"));
//                                item.setLat( Double.parseDouble(obj.getString("lat"))); // added by Adonis 2017.07.04
//                                item.setLng( Double.parseDouble(obj.getString("lng"))); // added by Adonis 2017.07.04

                                ArrayList<Tag> tagList = new ArrayList<Tag>();
                                for(int j=0; j<TAG_MAX; j++){
                                    Tag tag = new Tag();
                                    tag.cate = obj.getInt("cate" + (j + 1));
                                    tag.tag = obj.getString("tag" + (j + 1));
                                    if(tag != null && !tag.tag.trim().equals("null") && tag.tag.length() > 0)tagList.add(tag);
                                }
                                item.setTagList(tagList);

                                list.add(item);
                            }
                        }

                        listner.onSuccess(list);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void getNeedList(RequestParams params, final HttpListner listner) {

        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.NEED_LIST;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                DebugUtils.setLog(TAG, response.toString());
                try {
                    if (response.getInt("status") == 0) {

                        ArrayList<Need> list = new ArrayList<Need>();
                        JSONArray jsonArrayNearUsers = new JSONArray();
                        if(response.getString("list") != null && !response.getString("list").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                if ( obj.get("user_id").equals(null) || obj.get("user_no").equals(null) )
                                    continue;
                                Need item = new Need();
                                item.setId(obj.getInt("idx"));
                                item.setUserPic(obj.getString("user_img"));
                                item.setNickName(obj.getString("user_nick"));
                                item.setSubject(obj.getString("subject"));
                                item.setContent(obj.getString("content"));
                                item.setPrice(obj.getString("price"));
                                item.setArea(obj.getInt("area_code"));
                                item.setSubarea(obj.getInt("subarea_code"));
                                item.setTerm(obj.getString("hope_date"));

                                item.setUserId(obj.getString("user_id"));
                                item.setUserNo(obj.getInt("user_no"));
                                item.setSnsId(obj.getString("user_kakao_id"));
                                item.setTotal(obj.getInt("total_rows"));
                                item.setWish(obj.getInt("wish") == 1 ? true : false);
                                item.setShareLink(obj.getString("share_link"));
                                item.setRegdt(obj.getString("regdt")); // added by Adonis 2017.6.30

                                item.setLat( Double.parseDouble(obj.getString("lat"))); // added by Adonis 2017.07.04
                                item.setLng( Double.parseDouble(obj.getString("lng"))); // added by Adonis 2017.07.04
                                ArrayList<Tag> tagList = new ArrayList<Tag>();
                                for(int j=0; j<TAG_MAX; j++){
                                    Tag tag = new Tag();
                                    tag.cate = obj.getInt("cate" + (j + 1));
                                    tag.tag = obj.getString("tag" + (j + 1));
                                    if(tag != null && !tag.tag.trim().equals("null") && tag.tag.length() > 0)tagList.add(tag);
                                }
                                item.setTagList(tagList);

                                list.add(item);
                            }
                        }

                        if(response.getString("user_list") != null && !response.getString("user_list").equals("null")) {
                            jsonArrayNearUsers = new JSONArray(response.getString("user_list"));
                        }

                        HashMap<String, Object> map = new HashMap<String, Object>();
                        map.put("list", list);
                        map.put("near_users_array", jsonArrayNearUsers);
                        listner.onSuccess(map);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    public void getNeedDetail(RequestParams params, final HttpListner listner) {

        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.INEED_DETAIL;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                DebugUtils.setLog(TAG, response.toString());
                try {
                    if (response.getInt("status") == 0) {

                        ArrayList<Need> list = new ArrayList<Need>();
                        if(response.getString("list") != null && !response.getString("list").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                Need item = new Need();
                                item.setId(obj.getInt("idx"));
                                item.setUserPic(obj.getString("user_img"));
                                item.setNickName(obj.getString("user_nick"));
                                item.setSubject(obj.getString("subject"));
                                item.setContent(obj.getString("content"));
                                item.setPrice(obj.getString("price"));
                                item.setArea(obj.getInt("area_code"));
                                item.setSubarea(obj.getInt("subarea_code"));
                                item.setTerm(obj.getString("hope_date"));
                                item.setUserId(obj.getString("user_id"));
                                item.setUserNo(obj.getInt("user_no"));
                                item.setSnsId(obj.getString("user_kakao_id"));
//                                item.setWish(obj.getInt("wish") == 1 ? true : false);
                                item.setShareLink(obj.getString("share_link"));
                                item.setLat( Double.parseDouble(obj.getString("lat"))); // added by Adonis 2017.07.04
                                item.setLng( Double.parseDouble(obj.getString("lng"))); // added by Adonis 2017.07.04
                                item.setRegdt(obj.getString("regdt"));
                                ArrayList<Tag> tagList = new ArrayList<Tag>();
                                for(int j=0; j<TAG_MAX; j++){
                                    Tag tag = new Tag();
                                    tag.cate = obj.getInt("cate" + (j + 1));
                                    tag.tag = obj.getString("tag" + (j + 1));
                                    if(tag != null && !tag.tag.trim().equals("null") && tag.tag.length() > 0)tagList.add(tag);
                                }
                                item.setTagList(tagList);

                                list.add(item);
                            }
                        }

                        listner.onSuccess(list);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }



    public void getArea(final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.AREA_LIST;

        get(mContext, url, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {

                        ArrayList<Area> list = new ArrayList<Area>();
                        JSONArray jsonArray = new JSONArray(response.getString("list"));
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject(i);
                            Area item = new Area();
                            item.setId(obj.getInt("area_no"));
                            item.setName(obj.getString("area_name"));

                            ArrayList<SubArea> subareaList = new ArrayList<SubArea>();
                            JSONArray subJsonArray = new JSONArray(obj.getString("subarea_list"));
                            for (int j = 0; j < subJsonArray.length(); j++) {
                                JSONObject subObj = subJsonArray.getJSONObject(j);
                                SubArea subItem = new SubArea();
                                subItem.setId(subObj.getInt("subarea_no"));
                                subItem.setName(subObj.getString("subarea_name"));
                                subItem.setAreaId(obj.getInt("area_no"));
                                subItem.setLat(subObj.getString("lat"));
                                subItem.setLng(subObj.getString("lng"));
                                subareaList.add(subItem);
                            }
                            item.setSubareaList(subareaList);
                            list.add(item);
                        }
                        listner.onSuccess(list);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    public void signup(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.SIGN_UP;
        DebugUtils.setLog(TAG, url + params.toString());
        post(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        User user = new User();
                        user.setUserType(response.getInt("user_type"));
                        user.setUserNo(response.getInt("user_no"));
                        user.setUserId(response.getString("user_id"));
                        user.setUserNick(response.getString("user_nick"));
                        user.setUserPhone(response.getString("user_phone"));
                        user.setArea(response.getInt("user_area"));
                        user.setSubArea(response.getInt("user_sub_area"));
                        user.setArea2(response.getInt("user_area2"));
                        user.setSubArea2(response.getInt("user_sub_area2"));
                        user.setArea3(response.getInt("user_area3"));
                        user.setSubArea3(response.getInt("user_sub_area3"));
                        user.setSnsId(response.getString("user_sns_id"));
                        user.setUserPic(response.getString("user_img"));
                        user.setOverview("");//added by Adonis 2017.07.07

                        ArrayList<String> portfolio = new ArrayList<String>();
                        for(int i = 1; i <= 5; i++){
                            if (response.has("user_img" +i)){
                                if (!response.getString("user_img"+i).isEmpty()) portfolio.add(response.getString("user_img"+i));
                            }
                        }
                        user.setPortfolio(portfolio);

                        listner.onSuccess(user);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void emailCheck(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.EMAIL_CHECK;

        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        String msg = response.getString("msg");
                        listner.onSuccess(msg);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    public void nickCheck(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.NICK_CHECK;

        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        String msg = response.getString("msg");
                        listner.onSuccess(msg);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void getOtherProfile(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.GET_PROFILE;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                DebugUtils.setLog(TAG, "" + response.toString());
                try {
                    DebugUtils.setLog(TAG, "" + response.getString("msg"));
                    if (response.getInt("status") == 0) {
                        JSONObject userObj = new JSONObject(response.getString("myprofile"));
                        User user = new User();
                        user.setUserNo(userObj.getInt("user_no"));
                        user.setUserId(userObj.getString("user_id"));
                        user.setUserNick(userObj.getString("user_nick"));
                        user.setSnsId(userObj.getString("user_kakao_id"));
                        user.setUserPic(userObj.getString("user_img"));
                        user.setArea(userObj.getInt("user_area"));
                        user.setSubArea(userObj.getInt("user_sub_area"));
                        user.setArea2(userObj.getInt("user_area2"));
                        user.setSubArea2(userObj.getInt("user_sub_area2"));
                        user.setArea3(userObj.getInt("user_area3"));
                        user.setSubArea3(userObj.getInt("user_sub_area3"));
                        user.setOverview(userObj.getString("user_overview"));// added by Adonis 2017.07.05

                        // added by Adonis 2017.07.05
                        ArrayList<String> portfolio = new ArrayList<String>();
                        for(int i = 1; i <= 5; i++){
                            if (!userObj.getString("user_img" + i).isEmpty() && !userObj.getString("user_img" + i).equals(""))
                                portfolio.add(userObj.getString("user_img" + i));
                        }
                        //////////////////////////
                        ArrayList<Review> reviews = new ArrayList<Review>();
                        if(response.getString("review") != null && !response.getString("review").equals("null")){
                            JSONArray subJsonArray = new JSONArray(response.getString("review"));
                            for(int i=0; i<subJsonArray.length(); i++){
                                JSONObject subObj = subJsonArray.getJSONObject(i);
                                Review review = new Review();
                                review.setId(subObj.getInt("idx"));
                                review.setWriterNo(subObj.getInt("writer_no"));
                                review.setWriterNick(subObj.getString("writer_nick"));
                                review.setWriterId(subObj.getString("writer_id"));
                                review.setWriterSnsId(subObj.getString("writer_kakao_id"));
                                review.setWriterPic(subObj.getString("writer_img"));
                                review.setContent(subObj.getString("comment"));
                                review.setPoint(subObj.getInt("point"));
                                review.setRegdt(subObj.getString("regdt"));
                                review.setTotal(subObj.getInt("total_rows"));
                                reviews.add(review);
                            }
                        }
                        HashMap<String, Object> map = new HashMap<String, Object>();
                        map.put("user", user);
                        map.put("reviews", reviews);
                        map.put("portfolio", portfolio);
                        listner.onSuccess(map);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void getReplyList(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.REPLY_LIST;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    DebugUtils.setLog(TAG, "" + response.getString("msg"));
                    if (response.getInt("status") == 0) {

                        ArrayList<Reply> replyList = new ArrayList<Reply>();
                        DebugUtils.setLog(TAG, "" + response.getString("comment"));
                        if(response.getString("comment") != null && !response.getString("comment").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("comment"));

                            for(int i=0; i<jsonArray.length(); i++){
                                JSONObject obj = jsonArray.getJSONObject(i);
                                Reply reply = new Reply();
                                reply.setId(obj.getInt("idx"));
                                reply.setUserNo(obj.getInt("user_no"));
                                reply.setUserNick(obj.getString("user_nick"));
                                reply.setUserPic(obj.getString("user_img"));
                                reply.setContent(obj.getString("comment"));
                                reply.setRegdt(obj.getString("regdt"));
                                reply.setTotal(obj.getInt("total_rows"));
                                replyList.add(reply);
                            }
                        }

                        listner.onSuccess(replyList);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    public void writeReply(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.REPLY_WRITE;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    DebugUtils.setLog(TAG, "" + response.getString("msg"));
                    if (response.getInt("status") == 0) {

                        listner.onSuccess(null);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void writeReview(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.REVIEW_WRITE;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    DebugUtils.setLog(TAG, "" + response.getString("msg"));
                    if (response.getInt("status") == 0) {
                        listner.onSuccess(null);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    public void modifyProfile(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.PROFILE_MODIFY;

        post(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                DebugUtils.setLog(TAG, response.toString());
                try {
                    if (response.getInt("status") == 0) {
                        User user = new User();
                        user.setUserType(response.getInt("user_type"));
                        user.setUserNo(response.getInt("user_no"));
                        user.setUserId(response.getString("user_id"));
                        user.setUserNick(response.getString("user_nick"));
                        user.setUserPhone(response.getString("user_phone"));
                        user.setArea(response.getInt("user_area"));
                        user.setSubArea(response.getInt("user_sub_area"));
                        user.setArea2(response.getInt("user_area2"));
                        user.setSubArea2(response.getInt("user_sub_area2"));
                        user.setArea3(response.getInt("user_area3"));
                        user.setSubArea3(response.getInt("user_sub_area3"));
                        user.setSnsId(response.getString("user_sns_id"));
                        user.setUserPic(response.getString("user_img"));
                        user.setOverview(response.getString("user_overview"));//added by Adonis 2017.07.07
                        user.setPoint(response.getString("emoney"));//added by Adonis 2017.07.07
                        ArrayList<String> portfolio = new ArrayList<String>();
                        for(int i = 1; i <= 5; i++){
                            if (!response.getString("user_img"+i).isEmpty()) portfolio.add(response.getString("user_img"+i));
                        }
                        user.setPortfolio(portfolio);
                        listner.onSuccess(user);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    public void modifyProfilePortfolio(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.PROFILE_MODIFY_PORTFOLIO;

        post(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                DebugUtils.setLog(TAG, response.toString());
                try {
                    if (response.getInt("status") == 0) {
                        listner.onSuccess(response.getString("msg"));
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    // added by Adonis 2017.07.06
    public void deleteProfile(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.DELETE_PROFILE;

        post(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                DebugUtils.setLog(TAG, response.toString());
                try {
                    if (response.getInt("status") == 0) {
                        listner.onSuccess("ok");
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    public void writeCan(RequestParams params, final boolean isModify, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.CAN_WRITE;
        getUrl(url, params);
        post(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                DebugUtils.setLog(TAG, "onSuccess" + response.toString());
                try {
                    DebugUtils.setLog(TAG, "" + response.getString("msg"));
                    if (response.getInt("status") == 0) {


                        ArrayList<String> pathList = new ArrayList<String>();
                        if (isModify) {
                            for (int i = 0; i < IMG_MAX; i++) {
                                String path = response.getString("img" + (i + 1));
                                if (path != null && path.trim().length() > 0 && !path.equals("null"))
                                    pathList.add(path);
                            }
                        }
                        listner.onSuccess(pathList);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                DebugUtils.setLog(TAG, "FAIL" + errorResponse.toString());
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    public void writeNeed(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.NEED_WRITE;
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    DebugUtils.setLog(TAG, "" + response.getString("msg"));
                    if (response.getInt("status") == 0) {

                        listner.onSuccess(null);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void getNoticeList(RequestParams params, final HttpListner listner) {

        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.NOTICE_LIST;

        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                DebugUtils.setLog(TAG, response.toString());
                try {
                    if (response.getInt("status") == 0) {

                        ArrayList<Notice> list = new ArrayList<Notice>();
                        if(response.getString("list") != null && !response.getString("list").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                Notice item = new Notice();
                                item.setSubject(obj.getString("subject"));
                                item.setContent(obj.getString("content"));
                                item.setRegdt(obj.getString("regdt"));
                                item.setTotal(obj.getInt("total_rows"));
                                list.add(item);
                            }
                        }
                        listner.onSuccess(list);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void getFaqList(RequestParams params, final HttpListner listner) {

        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.FAQ_LIST;

        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                DebugUtils.setLog(TAG, response.toString());
                try {
                    if (response.getInt("status") == 0) {

                        ArrayList<Notice> list = new ArrayList<Notice>();
                        if(response.getString("list") != null && !response.getString("list").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                Notice item = new Notice();
                                item.setSubject(obj.getString("subject"));
                                item.setContent(obj.getString("content"));
                                item.setRegdt(obj.getString("regdt"));
                                item.setTotal(obj.getInt("total_rows"));
                                list.add(item);
                            }
                        }
                        listner.onSuccess(list);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    // added by Adonis 2017.07.06
    public void getQnaList(RequestParams params, final HttpListner listner) {

        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.REGIST_LIST;

        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                DebugUtils.setLog(TAG, response.toString());
                try {
                    if (response.getInt("status") == 0) {

                        ArrayList<Notice> list = new ArrayList<Notice>();
                        if(response.getString("list") != null && !response.getString("list").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                Notice item = new Notice();
                                item.setSubject(obj.getString("subject"));
                                item.setContent(obj.getString("content"));
                                item.setRegdt(obj.getString("regdt"));
                                item.setTotal(obj.getInt("total_rows"));
                                list.add(item);
                            }
                        }
                        listner.onSuccess(list);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }
    /////////////////////////////

    public void registFaq(RequestParams params, final HttpListner listner) {

        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.REGIST_FAQ;

        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                DebugUtils.setLog(TAG, response.toString());
                try {
                    if (response.getInt("status") == 0) {
                        listner.onSuccess(null);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    public void productDelete(RequestParams params, final HttpListner listner) {

        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.PRODUCT_DELETE;
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                DebugUtils.setLog(TAG, response.toString());
                try {
                    if (response.getInt("status") == 0) {
                        listner.onSuccess("");

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void findId(RequestParams params, final HttpListner listner) {

        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.FIND_ID;
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        listner.onSuccess(response.getString("user_id"));

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void findPw(RequestParams params, final HttpListner listner) {

        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.FIND_PW;
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        listner.onSuccess(response.getString("new_password"));

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void report(RequestParams params, final HttpListner listner) {

        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.REPORT;
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        listner.onSuccess("");

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    public void getProvision(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.GET_PROVISION;
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        listner.onSuccess(response.getString("text_info"));

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void getEvent(final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.GET_EVENT;
        get(mContext, url, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {

                        ArrayList<Event> list = new ArrayList<Event>();
                        if(response.getString("list") != null && !response.getString("list").equals("null")) {
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for(int i=0; i<jsonArray.length(); i++){
                                JSONObject obj = jsonArray.getJSONObject(i);
                                Event event = new Event();
                                event.setId(obj.getInt("id"));
                                event.setImage(obj.getString("bgimg"));
                                event.setLink(obj.getString("url"));
                                list.add(event);
                            }
                        }
                        listner.onSuccess(list);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void deleteReply(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.REPLY_DELETE;
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        listner.onSuccess("");

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void canDetail(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.CAN_DETAIL;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        Process process = new Process();
                        process.setType(response.getInt("type"));
                        if(response.getInt("type") > 0)process.setId(response.getInt("idx"));
                        process.setPrice(response.getString("price"));
                        process.setUserNo(response.getInt("user_no"));
                        listner.onSuccess(process);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    public void canAction(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.CAN_ACTION;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        listner.onSuccess(null);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    public void canRegist(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.CAN_REGIST;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        listner.onSuccess(null);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void canRegistList(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.CAN_REGIST_LIST;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        ArrayList<Process> list = new ArrayList<Process>();
                        if(response.getString("list") != null && !response.getString("list").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Process item = new Process();
                                JSONObject obj = jsonArray.getJSONObject(i);
                                item.setId(obj.getInt("idx"));
                                item.setUserNo(obj.getInt("user_no"));
                                item.setUserNick(obj.getString("user_nick"));
                                item.setPrice(obj.getString("price"));
                                item.setContent(obj.getString("content"));
                                item.setType(obj.getInt("type"));
                                item.setTotal(obj.getInt("total_rows"));
                                item.setPic(obj.getString("user_img"));
                                item.setUpfile(obj.getString("upfile"));
                                item.setUpreal(obj.getString("upreal"));
                                list.add(item);
                            }

                        }
                        listner.onSuccess(list);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    // Can 작업상황 모두 얻기
    public void canMyRegistList(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.CAN_MY_REGIST_LIST;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        ArrayList<Process> list = new ArrayList<Process>();
                        if(response.getString("list") != null && !response.getString("list").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Process item = new Process();
                                JSONObject obj = jsonArray.getJSONObject(i);
                                item.setId(obj.getInt("idx"));
                                item.setUserNo(obj.getInt("user_no"));
                                item.setUserNick(obj.getString("user_nick"));
                                item.setPrice(obj.getString("price"));
                                item.setContent(obj.getString("content"));
                                item.setType(obj.getInt("type"));
                                item.setPic(obj.getString("user_img"));
                                item.setUpfile(obj.getString("upfile"));
                                item.setUpreal(obj.getString("upreal"));
                                list.add(item);
                            }

                        }
                        listner.onSuccess(list);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }



    public void needDetail(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.NEED_DETAIL;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        Process process = new Process();
                        process.setType(response.getInt("type"));
                        if(response.getInt("type") > 0)process.setId(response.getInt("idx"));
                        process.setPrice(response.getString("price"));
                        listner.onSuccess(process);

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    public void needAction(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.NEED_ACTION;
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        listner.onSuccess(null);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    public void needRegist(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.NEED_REGIST;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        listner.onSuccess(null);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void needRegistList(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.NEED_REGIST_LIST;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        ArrayList<Process> list = new ArrayList<Process>();
                        if(response.getString("list") != null && !response.getString("list").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Process item = new Process();
                                JSONObject obj = jsonArray.getJSONObject(i);
                                item.setId(obj.getInt("idx"));
                                item.setUserNo(obj.getInt("user_no"));
                                item.setUserNick(obj.getString("user_nick"));
                                item.setPrice(obj.getString("price"));
                                item.setContent(obj.getString("content"));
                                item.setType(obj.getInt("type"));
                                item.setPic(obj.getString("user_img"));
                                item.setUpfile(obj.getString("upfile"));
                                item.setUpreal(obj.getString("upreal"));
                                list.add(item);
                            }

                        }
                        listner.onSuccess(list);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    // I need 내작업상황모두 얻기
    public void needMyRegistList(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.NEED_MY_REGIST_LIST;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        ArrayList<Process> list = new ArrayList<Process>();
                        if(response.getString("list") != null && !response.getString("list").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Process item = new Process();
                                JSONObject obj = jsonArray.getJSONObject(i);
                                item.setId(obj.getInt("idx"));
                                item.setUserNo(obj.getInt("user_no"));
                                item.setUserNick(obj.getString("user_nick"));
                                item.setPrice(obj.getString("price"));
                                item.setContent(obj.getString("content"));
                                item.setType(obj.getInt("type"));
                                item.setPic(obj.getString("user_img"));
                                item.setUpfile(obj.getString("upfile"));
                                item.setUpreal(obj.getString("upreal"));
                                list.add(item);
                            }

                        }
                        listner.onSuccess(list);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

// changed by RGJ 2017.01.31
    public void getBankInfo(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.GET_BANK;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        Bank item = new Bank();
                        if(response.getString("banknm") != "null")
                            item.setName(response.getString("banknm"));
                        if(response.getString("banknum") != "null")
                            item.setNumber(response.getString("banknum"));
                        if(response.getString("bankown") != "null")
                            item.setOwner(response.getString("bankown"));
                        listner.onSuccess(item);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    // added by RGJ 2017.01.31
    public void setBankInfo(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.SET_BANK;
        getUrl(url, params);
        get(mContext, url, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        Bank item = new Bank();
                        item.setName(response.getString("banknm"));
                        item.setNumber(response.getString("banknum"));
                        item.setOwner(response.getString("bankown"));
                        listner.onSuccess(item);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void actBank(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.ACT_BANK;
        getUrl(url, params);
        post(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        listner.onSuccess(response.getString("msg"));
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }
    public void terminateTransaction(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.
                TERMINATE_TRANSACTION;
        getUrl(url, params);
        post(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        listner.onSuccess(response.getString("msg"));
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }
    // added by RGJ 2017.01.18
    // 포인트환급신청
    public void actBankRefund(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.ACT_BANK_REFUND;
        getUrl(url, params);
        post(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        listner.onSuccess(response.getString("msg"));
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }
    // added end RGJ



    public void registSingo(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.REGIST_SINGO;
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        listner.onSuccess(null);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void registWish(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.REGIST_WISH;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        listner.onSuccess(null);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }



    public void registMemo(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.REGIST_MEMO;
        getUrl(url, params);
        post(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 1) {
                        listner.onSuccess(null);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    public void getQuestList(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.GET_MEMO;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        ArrayList<Question> list = new ArrayList<Question>();
                        if(response.getString("list") != null && !response.getString("list").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for (int i = jsonArray.length()-1; i > -1 ; i--) {
                                Question item = new Question();
                                JSONObject obj = jsonArray.getJSONObject(i);
                                item.id = obj.getInt("idx");
                                item.receiveId = obj.getString("recv_id");
                                item.receivePath = obj.getString("recv_img");
                                item.sendId = obj.getString("send_id");
                                item.content = obj.getString("content");
                                item.file = obj.getString("upfile");
                                item.regdate = obj.getString("regdt");
                                item.totalCount = obj.getInt("total_rows");
                                item.fileSize = obj.getString("filesize");
                                item.fileType = obj.getInt("type");
                                item.fileName = obj.getString("ori_filename");
                                list.add(item);
                            }
                        }
                        listner.onSuccess(list);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }



    public void getPointMall(final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.GET_POINT_MALL;
        get(mContext, url, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        ArrayList<Mall> list = new ArrayList<Mall>();
                        if(response.getString("list") != null && !response.getString("list").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for (int i = 0; i <jsonArray.length() ; i++) {
                                Mall item = new Mall();
                                JSONObject obj = jsonArray.getJSONObject(i);
                                item.idx = obj.getInt("idx");
                                item.cash = obj.getInt("cash");
                                item.point = obj.getInt("point");
                                item.title = obj.getString("title");
                                item.img = obj.getString("img");
                                list.add(item);
                            }
                        }
                        listner.onSuccess(list);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void getPointHistory(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.GET_POINT_HISTORY;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        ArrayList<PointHistory> list = new ArrayList<PointHistory>();
                        if(response.getString("list") != null && !response.getString("list").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for (int i = 0; i <jsonArray.length() ; i++) {
                                PointHistory item = new PointHistory();
                                JSONObject obj = jsonArray.getJSONObject(i);
                                item.id = obj.getInt("idx");
                                /*item.account = obj.getInt("account");
                                item.goods = obj.getString("goods");
                                item.payway = obj.getString("payway");*/


                                item.category = obj.getString("category");
                                item.title = obj.getString("title");
                                item.point = obj.getString("point");
                                item.regdt = obj.getString("regdt");
                                list.add(item);
                            }
                        }
                        listner.onSuccess(list);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void getChatRoomList(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.GET_CHATROOM_LIST;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        ArrayList<Memo> list = new ArrayList<Memo>();
                        if(response.getString("list") != null && !response.getString("list").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for (int i = 0; i <jsonArray.length() ; i++) {
                                Memo item = new Memo();
                                JSONObject obj = jsonArray.getJSONObject(i);
                                item.sendId = obj.getString("send_userid");
                                item.receiveId = obj.getString("recv_userid");
                                item.partnerImg = obj.getString("recv_img");
                                if(obj.getString("chatroom") != "null") //changed by RGJ 2014.01.15
                                    item.chatRoom = obj.getInt("chatroom");
                                item.regdt = obj.getString("regdt");
                                if(obj.getString("total_rows") != "null") //changed by RGJ 2014.01.15
                                    item.totalRow = obj.getInt("total_rows");
                                item.partnerNick = obj.getString("recv_nick");
                                if(obj.getString("recv_user_no") != "null") //changed by RGJ 2014.01.15
                                    item.partnerNo = obj.getInt("recv_user_no");
                                if(obj.getString("latest_msg") != "null" || obj.getString("latest_msg") != null ) //Added by Adonis 2017.07.18
                                    item.latestMSG = obj.getString("latest_msg");
                                list.add(item);
                            }
                        }
                        listner.onSuccess(list);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }
    public void getBuyerRankList(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.BUYER_RANK;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        ArrayList<Rank> list = new ArrayList<Rank>();
                        if(response.getString("list") != null && !response.getString("list").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for (int i = 0; i <jsonArray.length() ; i++) {
                                Rank item = new Rank();
                                JSONObject obj = jsonArray.getJSONObject(i);
                                item.rank = obj.getInt("rank");
                                item.myRank = obj.getString("myrank");
                                item.nick = obj.getString("nick");
                                item.userId = obj.getString("userid");
                                item.point = obj.getString("point");
                                list.add(item);
                            }
                        }
                        listner.onSuccess(list);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    public void getSellerRankList(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.SELLER_RANK;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        ArrayList<Rank> list = new ArrayList<Rank>();
                        if(response.getString("list") != null && !response.getString("list").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for (int i = 0; i <jsonArray.length() ; i++) {
                                Rank item = new Rank();
                                JSONObject obj = jsonArray.getJSONObject(i);
                                item.rank = obj.getInt("rank");
                                item.myRank = obj.getString("myrank");
                                item.nick = obj.getString("nick");
                                item.userId = obj.getString("userid");
                                item.point = obj.getString("point");
                                list.add(item);
                            }
                        }
                        listner.onSuccess(list);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void getLikeList(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.GET_LIKE_LIST;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        ArrayList<Like> list = new ArrayList<Like>();
                        if(response.getString("list") != null && !response.getString("list").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for (int i = 0; i <jsonArray.length() ; i++) {
                                Like item = new Like();
                                JSONObject obj = jsonArray.getJSONObject(i);
                                item.id = obj.getInt("idx");
                                item.subject = obj.getString("subject");
                                item.content = obj.getString("content");
                                item.price = obj.getString("price");
                                item.pic = obj.getString("user_img");
                                item.nick = obj.getString("user_nick");
                                item.totalCnt = obj.getInt("total_rows");
                                item.type = obj.getString("type");
                                item.gidx = obj.getInt("gidx");
                                item.regdt = obj.getString("regdt");//added by Adonis 2017.07.02
                                list.add(item);
                            }
                        }
                        listner.onSuccess(list);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void getSMSCode(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.GET_CODE;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        listner.onSuccess(response.getString("sms_code"));

                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }


    public void getMyPoint(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.GET_POINT;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        new PreferencesManager(mContext).setUserPoint(response.getString("mypoint"));
                        listner.onSuccess(response.getString("mypoint")); // added by RGJ 2017.01.15
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });
    }


    public void getAlarm(RequestParams params, final HttpListner listner){
        DebugUtils.setLog(TAG, "getAlarm called!!!");
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.GET_ALARM;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    DebugUtils.setLog(TAG, "status : " + response.getInt("status"));
                    if (response.getInt("status") == 0) {

                        new PreferencesManager(mContext).setIsPush(response.getString("chk_notice").equals("Y") ? true : false);
                        new PreferencesManager(mContext).setIsMemoPush(response.getString("chk_msg").equals("Y") ? true : false);
                        new PreferencesManager(mContext).setIsSendSMS(response.getString("sms_notice").equals("Y") ? true : false);
                        listner.onSuccess(null);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });
    }

    public void setAlarm(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.SET_ALARM;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        listner.onSuccess(null);
                    }else{
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    public void download(String downloadUrl, final String fileName, final HttpListner listner){
        get(downloadUrl, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {

                File dir = new File(Const.SAVE_FILE_DIR);
                if (!dir.exists()){
                    if(!dir.mkdir()) {
                        listner.onFailure("파일조작오류가 발생했습니다."); // added by RGJ 2017.02.04
                    }
                }
                File file = new File(Const.SAVE_FILE_DIR + fileName);
                try {
                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(bytes);
                    fos.close();
                    listner.onSuccess(fileName);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    listner.onFailure("파일조작오류가 발생했습니다."); // added by RGJ 2017.02.04
                } catch (IOException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    // added by RGJ 2017.01.16
    // 포인트사용이력얻기
    public void getPointUseHistory(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.GET_POINT_USE_HISTORY;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        ArrayList<PointUseHistory> list = new ArrayList<PointUseHistory>();
                        if(response.getString("list") != null && !response.getString("list").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for (int i = 0; i <jsonArray.length() ; i++) {
                                PointUseHistory item = new PointUseHistory();
                                JSONObject obj = jsonArray.getJSONObject(i);
                                // item.id = obj.getInt("id"); //id 얻기
                                item.regdt = obj.getString("regdt"); // 등록날자
                                if(obj.getString("point") != "null")
                                    item.point = obj.getInt("point"); // 거래 포인트량
                                else
                                    item.point = 0; // 거래 포인트량
                                if(obj.getString("type") != "null") // 거래유형 구매1, 판매2
                                    item.type = obj.getInt("type");
                                item.status = obj.getInt("status"); //상태: 완료
                                item.trader_name = obj.getString("trader_name"); // 거래자 닉네임
                                item.content = obj.getString("content"); // 추가정보

                                list.add(item);
                            }
                        }
                        listner.onSuccess(list);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    // added by RGJ 2017.01.16
    // 포인트환급이력얻기
    public void getPointRefundHistory(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.GET_POINT_REFUND_HISTORY;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        ArrayList<PointRefundHistory> list = new ArrayList<PointRefundHistory>();
                        if(response.getString("list") != null && !response.getString("list").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for (int i = 0; i <jsonArray.length() ; i++) {
                                PointRefundHistory item = new PointRefundHistory();
                                JSONObject obj = jsonArray.getJSONObject(i);
                                // item.id = obj.getInt("id"); //id 얻기
                                item.regdt = obj.getString("regdt"); // 등록날자
                                if(obj.getString("point") != "null")
                                    item.point = obj.getInt("point"); // 거래 포인트량
                                else
                                    item.point = 0; // 거래 포인트량
                                if(obj.getString("status") != "null") // 상태 환급처리중, 환급완료 구매1, 판매2
                                    item.status = obj.getInt("status");
                                if(item.status == 1)
                                    item.category = "처리중"; // 상태: 처리중
                                else
                                    item.category = "환급완료";//상태: 완료
                                item.title = "환급신청"; //
                                list.add(item);
                            }
                        }
                        listner.onSuccess(list);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    // added by RGJ 2017.01.16
    // 포인트충전이력얻기
    public void getPointChargeHistory(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.GET_POINT_CHARGE_HISTORY;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.getInt("status") == 0) {
                        ArrayList<PointChargeHistory> list = new ArrayList<PointChargeHistory>();
                        if(response.getString("list") != null && !response.getString("list").equals("null")){
                            JSONArray jsonArray = new JSONArray(response.getString("list"));
                            for (int i = 0; i <jsonArray.length() ; i++) {
                                PointChargeHistory item = new PointChargeHistory();
                                JSONObject obj = jsonArray.getJSONObject(i);
                                // item.id = obj.getInt("id"); //id 얻기
                                item.regdt = obj.getString("regdt"); // 등록날자
                                if(obj.getString("point") != "null")
                                    item.point = obj.getInt("point"); // 거래 포인트량
                                else
                                    item.point = 0; // 거래 포인트량
                                if(obj.getString("status") != "null")
                                    item.status = obj.getInt("status"); // 거래 상태
                                else
                                    item.status = 0; // 상태

                                item.title = " 구매"; // 구매
                                list.add(item);
                            }
                        }
                        listner.onSuccess(list);
                    } else {
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

    // added by RGJ 2017.01.16
    // 리용자 작업 정보얻기
    public void getUserWorkInfo(RequestParams params, final HttpListner listner){
        String url = UrlInfo.URL_HEADER + UrlInfo.API_HEADER + UrlInfo.CAN_USER_WORK_INFO;
        getUrl(url, params);
        get(mContext, url, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try{
                    if (response.getInt("status") == 0) {
                        double[] list = new double[2];
                        list[0] = response.getDouble("work_cnt");
                        list[1] = response.getDouble("avg_price");
                        listner.onSuccess(list);
                    }else{
                        listner.onFailure(response.getString("msg"));
                    }
                } catch (JSONException e){
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listner.onFailure(mContext.getString(R.string.network_error));
            }
        });
    }

}
